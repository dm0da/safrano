# frozen_string_literal: true

require_relative 'lib/safrano/version'

D = 'Safrano is an OData server library '
f = ['lib/safrano.rb']
f += Dir.glob('lib/*.rb') + Dir.glob('lib/odata/*.rb') + Dir.glob('lib/odata/filter/*.rb')
f += Dir.glob('lib/safrano/*.rb')
f += Dir.glob('lib/sequel/plugins/*.rb')
f += Dir.glob('lib/odata/edm/*.rb')
f += Dir.glob('lib/odata/request/*.rb')
f += Dir.glob('lib/core_ext/*/*.rb')
f += Dir.glob('lib/core_ext/*/*/*.rb')
f += Dir.glob('lib/core_ext/*.rb')

Gem::Specification.new do |s|
  s.name        = 'safrano'
  s.version     = Safrano::VERSION
  s.summary     = 'Safrano is an OData server library '
  s.description = D
  s.authors     = ['oz']
  s.email       = 'dev@aithscel.eu'
  s.files       = f
  s.required_ruby_version = '>= 2.7.0'
  s.add_dependency 'rack', '>= 2.0', '< 4.0'
  s.add_dependency 'rack-cors', '>= 1.1', '< 3.0'
  s.add_dependency 'new_rfc_2047', '~> 1.0'
  s.add_dependency 'sequel', '~> 5.15'
  s.add_dependency 'tzinfo', '~> 2.0'
  s.add_dependency 'tzinfo-data', '~> 1.0'
  s.add_dependency 'uuidtools', '~> 2.2'
  s.add_dependency 'rexml', '~> 3.0'
  s.add_development_dependency 'rack-test', '~> 2.0'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.homepage = 'https://safrano.aithscel.eu'
  s.license = 'MIT'
  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/dm0da/safrano/issues',
    'changelog_uri' => 'https://gitlab.com/dm0da/safrano/blob/master/CHANGELOG',
    'source_code_uri' => 'https://gitlab.com/dm0da/safrano/tree/master',
    'documentation_uri' => 'https://gitlab.com/dm0da/safrano/-/blob/master/README.md'
  }
end
