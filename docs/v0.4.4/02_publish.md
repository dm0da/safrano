## Safrano Documentation v0.4.4 

< [previous](01_Sequel_Model.md#safrano-documentation) | [up](README.md) | [next](03_rack_config.md#safrano-documentation) >

### 2. publish models and relations as an Safrano OData app

In this step you define a Safrano OData web app and publish the previously defined
models and associations as part of the app.
All OData specific options and settings belong here.

The `Safrano OData app` is just a class that should derive from `Safrano::ServerApp`.

Safrano provides a small publishing DSL that you can use within the app class.
The DSL has a top-down structure. `publish_service` is the entry point for defining an OData service in your app class. From within the `publish_service` you can use
* `title`, `name`, `prefix`, `namespace` to set  the service title, name, prefix and namespace
* `server_url` (added in safrano-0.4.3) to specify the server-part of ressource metadata uris. This setting is purely passive, only used to generate the metadata uri's. The default value is  `http://localhost:9494`. Before safrano-0.4.3, this was pulled dynamically from the http-request
* `enable_batch` to enable the batch functionality
* `publish_model` to publish Models. As a mandatory argument you need to provide the model class name . Per default the model will be published as an OData Entity Set named like the class name. You can provide a different entity set name as an option . Within a `publish_model` block, you can then publish navigation attributes with:
  + `add_nav_prop_single` to publish a one_to_one or a many_to_one association. As a mandatory argument you need to provide the association name. The OData navigation attribute will be named like the association name. 
  + `add_nav_prop_collection` to publish a one_to_many or a many_to_many association. As a mandatory argument you need to provide the association name. The OData navigation attribute will be named like the association name. 
  
* `publish_media_model` is a specialised `publish_model` to publish a model as a media entity set.  See [Publishing media entity models and associations](02_publish_media.md#safrano-documentation) for details. 

A typical use case would look like that:

 ```ruby
### Sequel Models (Step 1)
class Publisher < Sequel::Model(:publishers_table)
end
class TrackModel < Sequel::Model(:tracks_table)
end
class Album < Sequel::Model(:albums_table)
  many_to_one :publisher, class: :Publisher, key: ...
  one_to_many :tracks, class: :TrackModel, key: ...
end

### Safrano server (Step 2) 
class YourODataApp < Safrano::ServerApp
  publish_service do

    title  'Title of your service'
    name  'ServiceName'
    namespace  'ODataServiceNamespace'

    # to publish your service at hostname:port/Service.svc
    path_prefix '/Service.svc'
    
    # used to build the metadata ressource uris
    server_url 'http://example.org:9494'
    
    # to enable the $batch functionality
    enable_batch

    # publish a model with default naming convention
    publish_model Publisher

    # publish a model under a different entity set name 
    publish_model TrackModel, :Track

    # publish a model with associations 
    publish_model Album do
    
      # for navigation attributes having target cardinality 0 or 1, 
      # eg for  /Album(1)/publisher 
      add_nav_prop_single :publisher

      # for navigation attributes having target cardinality N  
      # eg for  /Album(1)/tracks
      add_nav_prop_collection :tracks

    end

  end
end
```
 
#### Sail DB Example  from the testsuite 
```ruby
class ODataSailApp < Safrano::ServerApp
  publish_service do

    title  'Sail OData API'
    name  'SailService'
    namespace  'ODataSail'
    path_prefix '/Sail.svc'
    enable_batch

    publish_model RaceType
    publish_model Race do

      # This allows to ask for  /Race(1)/rtype with type RaceType
      add_nav_prop_single :rtype

      # This allows to ask for  /Race(1)/Edition
      add_nav_prop_collection :Edition

    end

    # This allows to ask for  /Edition(...)/Race
    publish_model Edition do
      add_nav_prop_single :Race
    end
  end
end
```