## Safrano Documentation v0.8.0+  

< [previous](02_publish.md#safrano-documentation) | [Up](README.md) | >

### 3. Configure your Safrano OData Rack app 

Safrano is a [Rack](https://github.com/rack/rack) application, and as such you can benefit from the already existing Rack middlewares and plugins.

Safrano does **not** come out of the box with auto-magical security protections.

**Please consider using Rack::Cors and/or Rack::Protection for security reasons.**

### Safrano app configuration and CORS setup

Rack apps are usually configured with an Rack::Builder block.

For configuring your Safrano app, please use Rack::Safrano::Builder instead of Rack::Builder. 
Rack::Safrano::Builder adds rack-cors middleware with a simple default configuration .
Safrano's default rack-cors configuration basically implements this:
  - all *localhost* origins are allowed anything (read/write)
  - all other origins are allowed GET (and HEAD and OPTIONS) only, ie read-only
  
*Note:* Because the batch functionality (endpoint /$batch) uses exclusively POST, this means that
batch functionality will only work with localhost origins with the default setup. If you want
to use batch with non localhost origins, you need to specify an explicit Rack::Cors block 
( allowing POST /$batch)   
  
You can overide safrano's  rack-cors default setings by just providing explicitely a
"use Rack::Cors" block 

*Examples* :
  - This will use Safrano's built-in default cors handling 
```ruby
final_odata_app = Rack::Safrano::Builder.new do
  use Rack::ODataCommonLogger
  run YourSafranoODataApp.new
end
run final_odata_app
```

- This will use your own cors handling, overriding Safrano's default one
```ruby
final_odata_app = Rack::Safrano::Builder.new do
  use Rack::Cors do
    # allow *changing* methods for a specific origin 
    allow do
      origins 'https://some.origin.net'
      resource '*', headers: :any, methods: [:get, :post, :put, :patch, :head, :options]
    end    
    
    # allow GET from everywhere
    allow do
      origins '*'
      resource '*', headers: :any, methods: [:get,  :head, :options]
    end
  end
  use Rack::ODataCommonLogger
  run YourSafranoODataApp.new
end
run final_odata_app
```

### Safrano specific Rack middlewares

Currently there is only one: `Rack::ODataCommonLogger` is like 
Rack::CommonLogger but additionally logs sub-requests of $batch requests.
Example output:

```
- - - [26/Apr/2020:10:59:20 +0200] "GET /cultivar/$count " 200 2 0.0086
- - - [26/Apr/2020:10:59:20 +0200] "GET /cultivar?$skip=0&$top=120&$expand=breeder " 200 10794 0.0793
127.0.0.1 - - [26/Apr/2020:10:59:20 +0200] "POST /odata/$batch HTTP/1.1" 202 11330 0.1509
```

The two first lines are the sub-requests of a $batch request which is logged
in the third line.

### Example configuration

```ruby
###########
# config.ru
#

# model.rb contains the Sequel models and Safrano OData app class definition 
require './model.rb'


final_odata_app = Rack::Safrano::Builder.new do


# CORS handling with rack-cors
#  use Rack::Cors do
#    allow do
#      Please put something meaningfull here after having read the CORS
#      and security related informations
#    end
#  end

# use Rack::CommonLogger
# or
# use Rack::ODataCommonLogger

# provide additional error informations
#  use Rack::ShowExceptions

  run YourSafranoODataApp.new

end

# run with the  default first available handler 
run final_odata_app 
