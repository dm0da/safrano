## Safrano Documentation v0.6.0 

<  | [up](README.md) | [next](02_publish.md#safrano-documentation) >

### 1. setup Sequel Models and Model relationships
This step is purely ruby-sequel coding. Just define the models and
relationships that you want to publish as and OData service. Please
have a look at the [excellent Sequel documentation](http://sequel.jeremyevans.net/documentation.html)
for detail about this step. You will not need to know the full Sequel 
functionality, but only the following topics:
* [Connecting to a database](http://sequel.jeremyevans.net/rdoc/files/doc/opening_databases_rdoc.html#label-Connecting+to+a+database)
* [Define Sequel Models](http://sequel.jeremyevans.net/rdoc/files/doc/object_model_rdoc.html#label-Sequel-3A-3AModel)
* [Model Associations](http://sequel.jeremyevans.net/rdoc/files/doc/association_basics_rdoc.html#label-Association+Basics)

#### Example

```ruby
require 'safrano'

DB = Sequel.connect(YOUR_DB_CONNECTION_DETAILS)

class Edition < Sequel::Model(:Edition)
end

class Race < Sequel::Model(:Race)
  many_to_one :rtype, class: :RaceType, key: :type
  one_to_many :Edition, class: :Edition, key: :race_id
end

class RaceType < Sequel::Model(:RaceType)
end

class Edition
  # A Edition is for a given Race; there are many Editions for a
  # given Race...--> many_to_one
  many_to_one :Race, key: :race_id
end
```
