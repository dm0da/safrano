
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata


module TM_RK_DateTime
  # datetime eq
  def test_datetime_eq
    @filtstr = "arrival eq datetime'2018-11-12T04:21:47'"

    # need brute force check
    expected = DateTime.parse '2018-11-12T04:21:47'
    gcollfilt  {|rk| rk.arrival == expected }

  end

  # datetime offset eq. Note: we assume UTC on DB/Sequel level
  def test_datetime_offset_eq
    # currently only works in sqlite because in Postgresql the
    # arrival column is TIMESTAMP ( without TZ ) which makes it ignore offsets in querie
    # changing the arrival column to TIMESTAMPTZ  ( with TZ ) should make it work in theory,
    # but then we have the datetime hours/minutes etc function tests that are failing; because these
    # 
    return unless Sequel::Model.db.adapter_scheme == :sqlite
    
    #@filtstr = "arrival eq datetime'2018-11-12T04:21:47'"
    @filtstr = "arrival eq datetimeoffset'2018-11-12T06:21:47+02:00'"
    # need brute force check
    # expected = DateTime.parse '2018-11-12T06:21:47+02:00'
    expected = DateTime.parse '2018-11-12T04:21:47Z'

    gcollfilt  {|rk| rk.arrival == expected }
    
  end  
  
  def test_datetime_lt
    @filtstr = "arrival lt datetime'2005-02-02T22:49:00'"
    # need brute force check
    threshld = DateTime.parse '2005-02-02T22:49:00'
    gcollfilt  {|rk| rk.arrival < threshld  }
    
    @filtstr = "arrival lt datetime'2005-02-02T22:49:00' AND rank ge 3"
    # need brute force check
    threshld = DateTime.parse '2005-02-02T22:49:00'
    gcollfilt  {|rk| rk.arrival < threshld && rk.rank >= 3}
  end
  
  # TODO
  #def test_type_error
  #  get "Ranking?$filter=rank eq datetime'2005-02-02T22:49:00'"
  #  assert last_response.bad_request?
  #end
  
  # datetime funcs
  def test_datetime_year
    @filtstr = "year(arrival) ge 2017"

    # need brute force check
    gcollfilt  {|rk| rk.arrival.year >= 2017}
  end

  def test_datetime_month
    @filtstr = "month(arrival) eq 11"

    # need brute force check
    gcollfilt {|rk| rk.arrival.month == 11 }
  end
  
  def test_datetime_day
    @filtstr = "day(arrival) lt 11"

    # need brute force check
    gcollfilt {|rk| rk.arrival.day < 11 }
  end
  
  def test_datetime_hour
    @filtstr = "hour(arrival) gt 11"

    # need brute force check
    gcollfilt  {|rk| rk.arrival.hour > 11 }
  end
  
  def test_datetime_minute
    @filtstr = "minute(arrival) ge 30"

    # need brute force check
    gcollfilt  {|rk| rk.arrival.min >= 30 }
  end
  
  def test_datetime_second
    @filtstr = "second(arrival) lt 30"

    # need brute force check
    gcollfilt  {|rk| rk.arrival.sec < 30 }
  end

end

module TM_Basics
 
  def test_basics_equal_filter
    @filtstr = "periodicity eq 1"
    gcollfilt Race.where( :periodicity => 1 ).all
  end
  
  # Check filtering on related attributes
  def test_basics_related_attr_filter
    @filtstr = 'PeriodicityType/period eq 4'
    gcollfilt{|r| r.PeriodicityType.period == 4}
  end
  
  def test_basics_equal_filter_parenthesis
    @filtstr = "(periodicity eq 1)"
    gcollfilt Race.where( :periodicity => 1 ).all
    
  end
  
  def test_basics_not_ne_filter_parenthesis
    @filtstr = "not (periodicity ne 1)"
    gcollfilt Race.where( :periodicity => 1 ).all
    
  end
  
  def test_basics_startswith_filter
# Route du Rhum
    @filtstr = "startswith(name,'Rout')"
    gcollfilt{|r| r.name =~ /\ARout/ }

# neg test
    @filtstr = "startswith(name,'fgfdgdfgdf')"
    gcollfilt []
  end  
  
  def test_basics_concat_fn_fval
    @filtstr = "concat(name,'xyz') eq 'The Race'"
    gcollfilt []
    
    
    @filtstr = "concat(name,'xyz') eq 'The Racexyz'"
    gcollfilt Race.where(name: 'The Race').all
    
  end
  
  
  def test_basics_concat_nested_eql
    @filtstr = "concat(name,concat(',',name)) eq 'The Race,The Race'"
    gcollfilt Race.where(name: 'The Race').all
  end

  def test_basics_concat_fval_fn
    @filtstr = "concat('xyz',name) eq 'The Race'"
    gcollfilt []

    @filtstr = "concat('xyz',name) eq 'xyzThe Race'"
    gcollfilt  Race.where(name: 'The Race').all

  end

  def test_basics_substringof_sig1_filter
# Route du Rhum
    @filtstr = "substringof('Rhum',name)"
    gcollfilt{|r| r.name.include?('Rhum')}

# Vendée Globe
    @filtstr = "substringof('é',name)"
    gcollfilt{|r| r.name.include?('é')}

# negative test
    @filtstr = "substringof('dfsfsfsdfsfsxx', name)"
    gcollfilt  []

# __The Race__
    #@filtstr = "substringof(name,'__The Race__')"
    #
    #

    #assert(y.size >= 1)
    #assert_kind_of(Race, y.first)
    #y.each { |r| assert('__The Race__'.include?(r.name)) }

  end
  
  

end



module TM_New

  def test_equal_filter
    @filtstr = "periodicity eq '1'"
    gcollfilt  Race.where( :periodicity => 1 ).all
    

    @filtstr = "periodicity EQ '1'"
    gcollfilt  Race.where( :periodicity => 1 ).all
    
    @filtstr = "name EQ 'Route du Rhum'"
    gcollfilt  Race.where( :name => 'Route du Rhum' ).all

    @filtstr = "name Ne 'Route du Rhum'"
    gcollfilt{|r| r.name != 'Route du Rhum'}
    
  end

  def test_equal_filter_quoted_arg
    @filtstr = "name Ne 'Route ''du'' Rhum'"
    gcollfilt{|r| r.name != "Route 'du' Rhum"}
  end

  def test_le_filter
    @filtstr = 'id Le 10' 
    gcollfilt{|r| r.id <= 10}
  end

  def test_lt_filter
    @filtstr = 'id Lt 7' 
    gcollfilt{|r| r.id < 7}
  end

  def test_arithm_add
    # id <= 4
    @filtstr = "id add 1 le 5"
    gcollfilt  Race.where{ Sequel[:id] <= 4 }.all
  end

  def test_arithm_sub
    # id <= 7
    @filtstr = "id sub 2 le 5"
    gcollfilt  Race.where{ Sequel[:id] <= 7 }.all
  end

  def test_arithm_mul
    # id <= 5
    @filtstr = "id mul 2 le 11"
    gcollfilt  Race.where{ Sequel[:id] <= 5 }.all
    
  end

  def test_arithm_div
    # id <= 21
    @filtstr = "id div 2 le 10"
    gcollfilt  Race.where{ Sequel[:id] <= 21}.all
    
  end

  def test_arithm_mod
    # id mod 3
    @filtstr = "id mod 3 eq 0"
    gcollfilt   Race.where( Sequel::SQL::NumericExpression.new(:%, Sequel[:id],3) => 0).all
   
#    y.each { |r| assert_equal 0, (r.id % 3) }
  end

  def test_equal_filter_protected_args
    @filtstr = "name NE 'id EQ 1'"
    gcollfilt   Race.all
  end

  def test_substring_protected_args

    @filtstr = "startswith(name,'id EQ 1')"
    gcollfilt  []
    
  end

  # test that protected quote in strings (double single quote) work
  def test_substring_protected_args_quotes

    @filtstr = "startswith(name,' xyz''xx')"
    gcollfilt  []
  
  end



  def test_substringof_sig1_filter
# Route du Rhum
    @filtstr = "substringof('Rhum',name)"
    
    gcollfilt{|r| r.name.include?('Rhum') }

# Vendée Globe
    @filtstr = "substringof('é',name)"
  
    gcollfilt{|r| r.name.include?('é') }

# negative test
    @filtstr = "substringof('dfsfsfsdfsfsxx', name)"
    
    gcollfilt  []
  end

  def test_substringof_sig2_filter
# Route du Rhum
    @filtstr = "substringof(name, 'this race is called Route du Rhum ?')"
    gcollfilt{|r| 'this race is called Route du Rhum ?'.include? r.name }

# Neg Tst
    @filtstr = "substringof(name, 'this race is called broute du foing ?')"
    gcollfilt  []

  end

  def test_substringof_sig1_filter_protected_quo
    # record #13 is Test-Race 'record' with quotes
    @filtstr = "substringof('''record'' with',name)"
    
   gcollfilt{|r| r.name.include?("'record' with") }

  end

  def test_startswith_filter_navattr
    @filtstr = "startswith(PeriodicityType/description,'every')"
  
    gcollfilt{|r| r.PeriodicityType.description.to_s =~ /\Aevery/  }
  end

  def test_endswith_filter
# Route du Rhum
    @filtstr = "endswith(name,'hum')"
    
    gcollfilt{|r| r.name =~ /hum\z/ }

# neg test
    @filtstr = "endswith(name,'fgfdgdfgdf')"
    gcollfilt  []
  end

  def test_special_char
    @filtstr = "substringof('’h', name)"
    gcollfilt  Race.where(name: 'Armel Le Cléac’hs race for testing').all
    
  end
   
end



module TM_new_complex

  def test_concat_fn_fval
    @filtstr = "concat(name,'xyz') eq 'The Race'"
    gcollfilt  []

    @filtstr = "concat(name,'xyz') eq 'The Racexyz'"
    gcollfilt  Race.where(name: 'The Race').all

  end

  def test_concat_fval_fn
    @filtstr = "concat('xyz',name) eq 'The Race'"
    gcollfilt  []

    @filtstr = "concat('xyz',name) eq 'xyzThe Race'"
    gcollfilt  Race.where(name: 'The Race').all

  end

  def test_concat_fn_fn
    @filtstr = "concat(name, periodicity) eq 'The Race99'"
    gcollfilt  [Race[7]]
  end
  
  def test_not_end_with
    @filtstr = "not endswith(name,'fgfdgdfgdf')"
    gcollfilt  Race.all
    
  end

  def test_not_eq
    @filtstr = "not name eq 'Route du Rhum'"
    gcollfilt  {|r| r.name != 'Route du Rhum' }
  
  end
  
  def test_equal_filter_cmplx_2
    # "Route du Rhum" has id=3...
    
    @filtstr = "(name EQ 'Route du Rhum') AND (id EQ 3)"
    gcollfilt  {|r| r.name == 'Route du Rhum' && r.id == 3 }
    
    
    @filtstr = "(name EQ 'Route du Rhum') AND (id EQ 2)"
    gcollfilt  {|r| r.name == 'Route du Rhum' && r.id == 2 }

    @filtstr = "(name EQ 'Route du Rhum') OR (id EQ 2)"
    gcollfilt  {|r| r.name == 'Route du Rhum' || r.id == 2 }
 
  end
  
  def test_and_or_without_parentheses_1
  # "Route du Rhum" has id=3...
    
    @filtstr = "name EQ 'Route du Rhum' OR id EQ 2"
    gcollfilt  {|r| r.name == 'Route du Rhum' || r.id == 2 }
 
  end
  
  def test_and_or_without_parentheses_2
    
    @filtstr = "id Eq 3 or id Eq 6"
    gcollfilt  [Race[3], Race[6]]

  end

  def test_and_or_without_parentheses_3
    
    @filtstr = "id Le 3 or id Ge 6"
    gcollfilt  {|r| ((r.id <= 3) || (r.id >= 6)) }

    
  end
end
module TM_Edition_null

  def test_is_null
    @filtstr = "organizer eq Null"
    gcollfilt  {|r| r.organizer.nil? }
  end
  
  def test_is_not_null
    @filtstr = "organizer ne NULL "
    gcollfilt  {|r| (not r.organizer.nil?) }
  end
  
end

class CollectionNullFilterTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional url parameter (eg. orderby )
    @adpar = nil
    @collpath = 'Edition'
    @etype = 'Edition'
    @ecollbase = Edition.all
  end
    
  include TM_Edition_null
  
end
# same tests should pass if orderby year is requested additionally
class CollectionNullFilterTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional $orderby url parameter 
    @adpar = 'year'
    @collpath = 'Edition'
    @etype = 'Edition'
    @ecollbase = Edition.all
  end
    
  include TM_Edition_null
  
end
class CollectionFilterTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional url parameter (eg. orderby )
    @adpar = nil
    @collpath = 'Race'
    @etype = 'Race'
    @ecollbase = Race.all
  end
    
  include TM_Basics
  include TM_New
  include TM_new_complex
  
end

# same tests should pass if orderby name is requested additionally
class CollectionFilterOrderbyTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional $orderby url parameter 
    @adpar = 'name'
    @collpath = 'Race'
    @etype = 'Race'
    @ecollbase = Race.all
    
  end
  include TM_Basics
  include TM_New
  include TM_new_complex
end

# same tests should pass if multiple orderbys are requested additionally
class CollectionFilterOrderbyMoreTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional  orderby  url parameter 
    @adpar = 'name desc,PeriodicityType/period,crew_type asc'
    @collpath = 'Race'
    @etype = 'Race'
    @ecollbase =  Race.all

  end
  include TM_Basics
  include TM_New
  include TM_new_complex
end

#### Datetime funcs with Ranking entity / arrival   #######

class CollectionDateTimeFilterTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional url parameter (eg. orderby )
    @adpar = nil
    @collpath = 'Ranking'
    @etype = 'Ranking'
    @ecollbase = Ranking.all
    
  end
  
  include TM_RK_DateTime
 
end

# same tests should pass if orderby crew is requested additionally
class CollectionDateTimeFilterOrderbyTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional $orderby url parameter 
    @adpar = 'crew'
    @collpath = 'Ranking'
    @etype = 'Ranking'
    @ecollbase = Ranking.all
  end
  include TM_RK_DateTime
end

# same tests should pass if orderby Race.name is requested additionally
class CollectionDateTimeFilterOrderby2Test < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional $orderby url parameter 
    @adpar = 'Race/name'
    @collpath = 'Ranking'
    @etype = 'Ranking'
    @ecollbase = Ranking.all
  end
  include TM_RK_DateTime
end

# same tests should pass if multiple orderbys are requested additionally
class CollectionDateTimeFilterOrderbyMoreTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    
    # additional  orderby  url parameter 
    @adpar = 'Race/name desc,arrival asc'
    @collpath = 'Ranking'
    @etype = 'Ranking'
    @ecollbase = Ranking.all
  end
  include TM_RK_DateTime
end

