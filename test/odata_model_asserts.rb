require_relative './client.rb'

module TM_Model_Compare_Helper

  # missnamed. this check actually values and metadata
  def assert_sequel_values_equal(expected, resulth, uri: nil)
    #type
    exptype = ns(expected.model.to_s)
    assert_equal(exptype, resulth[:__metadata][:type])

    # values
    assert_sequel_values_only_equal(expected, resulth)
    
    # metadata uri
    assert_equal "http://example.org/#{uri}", resulth[:__metadata][:uri] if uri
  end
  
  # This check only values and not metadata
  def assert_sequel_values_only_equal(expected, resulth)

    # values
    expected.each{|k,v| assert_equal v, resulth[k] }
    
  end

  # just a variant for complex types with string key instead of 
  # symbols; because currently our client parsing does not
  # symbolize keys for Complex Types
  def assert_sequel_ct_values_equal(expected, resulth, uri: nil)
    #type
    exptype = ns(expected.model.to_s)
    assert_equal(exptype, resulth['__metadata']['type'], 'Wrong Type')

    # values
    expected.each{|k,v| assert_equal v, resulth[k.to_s] }
    
    # metadata uri
    assert_equal "http://example.org/#{uri}", resulth['__metadata']['uri'] if uri
  end

  def assert_sequel_media_values_equal(expmedia, resulth, ressource_version='1', &proc)
    #type
    exptype = ns(expmedia.model.to_s)
    assert_equal(exptype, resulth[:__metadata][:type])

    expmedia.each{|k,v| 
      case k
        when :content_type
           # media related values --> metadata
        assert_equal v,  resulth[:__metadata][k]
        when :media_src # obsolete here, will be removed from table/model
      else
    # non-media values
        assert_equal v, resulth[k] 
      end
      }
    # media src
     assert_equal "#{uribase(last_request)}/#{proc.call(expmedia)}/$value?version=#{ressource_version}",
                  resulth[:__metadata][:media_src]
    # edit media
    assert_equal "#{uribase(last_request)}/#{proc.call(expmedia)}/$value",
                  resulth[:__metadata][:edit_media]
  end

  # helper method to transform expected collection and returned json collection
  # into comparable (values and type) set objects
  def make_comparable_coll_sets(expcoll, rescollh)
    exptype = ns(expcoll.first.model.to_s)
    exptab = expcoll.map{|exp|   {type: exptype,   values: exp.values }   }
    fields = expcoll.first.model.db_schema.keys

    restab = rescollh.map{|resh|
        pruned = {}
        fields.each{|f| pruned[f] = resh[f] }
        
      {type: resh[:__metadata][:type],
      values: pruned}
    }
    [exptab.to_set, restab.to_set]
  end
  
  # helper method to transform expected collection and returned json collection
  # into comparable (values only) set objects
  def make_comparable_valsonly_coll_sets(expcoll, rescollh)

    exptab = expcoll.map{|exp|   exp.values    }
    fields = expcoll.first.model.db_schema.keys

    restab = rescollh.map{|resh|
        pruned = {}
        fields.each{|f| pruned[f] = resh[f] }
        
       pruned 
    }
    
    [exptab.to_set, restab.to_set]
  end  
  
  # compare collections using Set (unordered) and compare type and values
  def assert_sequel_coll_values_equal(expcoll, rescollh)
    # we compare Set's of {:type=>... 
    #                      :values=>{} } hashes
    if expcoll.empty?
      assert_empty rescollh
    else
      expset, result_set = make_comparable_coll_sets(expcoll, rescollh)
      assert_equal expset, result_set
    end
  end
  
  # compare collections using Set (unordered) and compare  values only
  def assert_sequel_coll_valuesonly_equal(expcoll, rescollh)
    # we compare Set's of values hashes
    if expcoll.empty?
      assert_empty rescollh
    else
      expset, result_set = make_comparable_valsonly_coll_sets(expcoll, rescollh)
      assert_equal expset, result_set
    end
  end

  # same as assert_sequel_coll_values_equal but does check that
  # result set is *included* or equal to the provided expected set
  # thus we are able to test limited result sets without knowing anything
  # about ordering 
  def assert_sequel_coll_set_included(expcoll, rescollh)
    # we compare Set's of {:type=>... 
    #                      :values=>{} } hashes
    if expcoll.empty?
      assert_empty rescollh
    else
      expset, result_set = make_comparable_coll_sets(expcoll, rescollh)
      assert result_set.subset?(expset)
    end
  
  end
  # compare collections content regardless of ordering (use Set)
  def assert_sequel_coll_set_equal(expcoll, rescoll)
    if expcoll.empty?
      assert_empty rescoll
    else
      # remove metadata and nav attribs from rescoll in order to be able
      # to compare just the direct values
      fields = expcoll.first.model.db_schema.keys
      prunedres = rescoll.map{ |res|
        pruned = {}
        fields.each{|f| pruned[f] = res[f] }
        pruned
      }
      assert_equal expcoll.map{|o| o.values}.to_set, prunedres.to_set
    end
  end

  # compare collections content regardless of ordering (use Set) with selected field
  def assert_sequel_coll_sel_set_equal(expcoll, rescoll, sel)
    if expcoll.empty?
      assert_empty rescoll
    else
      # 0 cleanup not relevant __metadata
      rescoll.each{|res| res.delete(:__metadata)}
      # 1. check that we have the expected data
      # remove nav attribs from rescoll in order to be able
      # to compare just the direct values of selected fields
      klass = expcoll.first.class
      selprops = klass.columns.map(&:to_s) & sel
      
      prunedres = rescoll.map{ |res|
        pruned = {}
        selprops.each{|f| pruned[f] = res[f] }
        pruned
      }
      prunedexp = expcoll.map{ |exp|
        pruned = {}
        selprops.each{|f| pruned[f] = exp.values[f] }
        pruned
      }
      assert_equal prunedexp.to_set, prunedres.to_set
      
      
      # 2. check that we dont have additional unwanted data
      sel_set = sel.to_set
      rescoll.each{|res|  
        fields = res.keys.to_set
        assert_equal sel_set, fields, "Exported fields are not ok : #{fields.to_a.join(' ')}"
      }
    end
  end


  def  assert_sequel_coll_ord_desc_by(y, &proc)
    y[0...-1].each_with_index{|elmt, idx|
      assert((proc.call(elmt) <=> proc.call(y[idx+1])) >= 0,
      "idx #{idx} #{proc.call(elmt).inspect} is not larger as #{proc.call(y[idx+1]).inspect}")  #descending
    }
  end

  def  assert_sequel_coll_ord_asc_by(y, &proc)
    y[0...-1].each_with_index{|elmt, idx|
      assert((proc.call(elmt) <=> proc.call(y[idx+1])) <= 0,
      "idx #{idx} #{proc.call(elmt).inspect} is not smaller as #{proc.call(y[idx+1]).inspect}")  #ascending
    }
  end
end

module TM_ODataModelTestCase
  def setup
    @jresd=nil
    @jresd_coll=nil
  end
  
  def uribase(last_request)
    e = last_request.env
    %Q(#{e['rack.url_scheme']}://#{e['HTTP_HOST']})
  end

  # Reuseable get (some collection) with filter/order and asserts expected result
  # assumes the following input is set : 
  
  # @collpath   : collection ressource path, eg: Race or Customer(1)/invoices
  # @etype  : the expected collection type(class name)
  # @filtstr : the value of $filter parameter
  # @adpar: optionally the value of $orderby parameter (or nil)
  # @toppar optionally the value of $top parameter (or nil)
  # @ecollbase: the expected unfiltered collection corresponding to @collpath
  #  method proc &filt: a filtering proc when applied to @ecollbase yields the final 
  #        expected result
  # OR alternatively to @ecollbase + filt
  #  method argument ecollfinal the final expected filtered collection
  
  # Usage; 
  #  gcollfilt Invoice.where{ val > 3 }.all  OR 
  #  gcollfilt {|i| i.val > 3  } 
  def gcollfilt( ecollfinal = nil, &filt)
    getcoll
    expected_tab = if block_given?
      @ecollbase.select{|row| filt.call(row) } 
    else
      ecollfinal
    end
    # expected result can be limited by @toppar if provided
    if @toppar
      exp_count =  [expected_tab.count, @toppar.to_i].min 
      assert_last_resp_coll_parseable_ok(@etype , exp_count)
      # we check that the limited result is *contained* in the expected 
      # *unlimited* collection set 
      assert_sequel_coll_set_included(expected_tab, @jresd_coll)
    else
      assert_last_resp_coll_parseable_ok(@etype , expected_tab.count)
      assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    end
  end
  
  def parse_error_response
    @jrawd = JSON.parse last_response.body
    @jmessage = @jrawd['odata.error']['message']
  end
  
  # check that it's a server error and that the err.msg contains the expected keywords
  def assert_server_error(*keywords)
    assert last_response.server_error?
    parse_error_response
    assert @jmessage, "Returned message is nil"
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.downcase.include?(kw.downcase),
             "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end

    # check that it's a 422 unprocessable error and that the err.msg contains 
    #the expected keywords
  def assert_422_unprocessable_error(*keywords)
    assert_equal 422, last_response.status, "last response status is #{last_response.status}"
    parse_error_response
    assert @jmessage, "Returned message is nil"
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.downcase.include?(kw.downcase),
             "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end
  
    # check that it's a not found error and that the err.msg contains 
    #the expected keywords
  def assert_not_found_error(*keywords)
    assert last_response.not_found?, "last response status is #{last_response.status}"
    parse_error_response
    assert @jmessage, "Returned message is nil"
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.downcase.include?(kw.downcase),
             "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end
  
    # check that it's a 501 request and that the err.msg contains the expected keywords
  def assert_not_implemented(*keywords)
    assert_equal 501, last_response.status
    parse_error_response
    assert @jmessage, "Returned message is nil"
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.downcase.include?(kw.downcase), "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end
  # check that it's a bad request and that the err.msg contains the expected keywords
  def assert_bad_request(*keywords)
    assert last_response.bad_request?, "last response status is #{last_response.status}"
    parse_error_response
    assert @jmessage, 'Returned message is nil'
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.include?(kw), "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end
  
  def gcollfilt_not_implemented(funcname)
    gcollfilt_bad_request(funcname, 'not implemented')
  end
  
  def gcollfilt_bad_request(*keywords)
    getcoll
    assert_bad_request(*keywords)
  end
  
  # helper func for gcollfilt and gcollfilt_not_implemented
  def getcoll
    partab = []
    fpar = @filtstr ? "$filter=#{URI.encode_www_form_component @filtstr}" : nil
    partab << fpar if fpar
    apar =  @adpar ? "$orderby=#{URI.encode_www_form_component @adpar}" : nil
    partab << apar if apar
    tpar =  @toppar ? "$top=#{URI.encode_www_form_component @toppar}" : nil
    partab << tpar if tpar
    # we expect at least one entry in partab
    get "#{@collpath}?#{partab.join('&')}"
  end
  
  # reusable check on last response + json-parse
  # it + check

  def assert_last_resp_entity_parseable_ok(type_name=nil)
    assert last_response.ok?

    assert_last_resp_entity_parseable(type_name=nil)

  end
  
  def assert_last_resp_entity_parseable(type_name)
    begin
      @jresd = Safrano::XJSON.parse_one last_response.body
    rescue
      require 'pry'
      binding.pry
    end
    assert_equal(ns(type_name), @jresd[:__metadata][:type], 
                 "Type #{@jresd[:__metadata][:type]} in reponse body is wrong ") if type_name

  end

 # reusable check on last response + json-parse
  # it + check

  def assert_last_resp_complextype_parseable_ok(type_name)
    assert last_response.ok?

    @jresd = JSON.parse last_response.body
    @jresd = @jresd['d']
 
    assert_equal(ns(type_name), @jresd['__metadata']['type'])

  end
  
  #namespace type. Method odata_namespace needs to be provided by test-class
  def ns(type_name)
    "#{odata_namespace}.#{type_name}"
  end
  
  def assert_last_resp_ct_coll_parseable_ok(type_name)
    assert last_response.ok?

    @jresd = JSON.parse last_response.body
    @jresd_coll = @jresd['d']['results']

    assert_equal(ns(type_name), @jresd_coll.first['__metadata']['type'])

  end
 # reusable check on last response + json-parse
  # it + check

  def assert_last_resp_primitive_parseable_ok(type_name)
    assert last_response.ok?, "Last response status is no ok but #{last_response.status}"

    @jresd = JSON.parse last_response.body
    @jresd = @jresd['d']
    
    assert_equal(type_name, @jresd['__metadata']['type'])

  end

  # same as previous but keep values unchanged (not casted)
  def assert_last_resp_enty_ok_no_cast(type_name=nil)
    assert last_response.ok?, "Last response status is no ok but #{last_response.status}"

    assert_last_resp_enty_no_cast(type_name=nil)

  end
    
  def assert_last_resp_enty_no_cast(type_name=nil)


    @jrawd = Safrano::XJSON.parse_one_nocast last_response.body

    # check type name on request only
    assert_equal(ns(type_name), @jrawd[:__metadata][:type]) if type_name

  end
  # same as previous but expect an empty entity
  def assert_last_resp_empty_entity_ok(type_name=nil)
    assert last_response.ok?

    @jrawd = JSON.parse last_response.body

    # check emptyness
    assert_equal({}, @jrawd['d'])

  end
  def assert_last_resp_empty_coll_parseable_ok( version = 2)
    assert last_response.ok?
    assert_last_resp_coll_parseable(nil, 0, version )
  end


  def assert_last_resp_coll_parseable(type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      @jresd = Safrano::XJSON.v1_parse_coll last_response.body
      @jresd_coll = @jresd
    else
      @jresd = Safrano::XJSON.parse_coll last_response.body
      @jresd_coll = @jresd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jresd_coll.size, 'Size of result-collection is incorrect')

    # check the returned type
    if exp_size > 0
      if type_name  # make check optional
        assert_equal(ns(type_name), @jresd_coll.first[:__metadata][:type])
        assert_equal(ns(type_name), @jresd_coll.last[:__metadata][:type])
      end
    end
  end

  def assert_last_resp_coll_parseable_nocast(type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      @jrawd = Safrano::XJSON.v1_parse_coll_nocast last_response.body
      @jrawd_coll = @rawd
    else
      @jrawd = Safrano::XJSON.parse_coll_nocast last_response.body
      @jrawd_coll = @jrawd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jrawd_coll.size)

    # check the returned type
    if exp_size > 0
      assert_equal(ns(type_name), @jrawd_coll.first[:__metadata][:type])
      assert_equal(ns(type_name), @jrawd_coll.last[:__metadata][:type])
    end
  end

  def assert_last_resp_links_parseable(type_name, exp_size, version=2)
    @resh = nil

    if (version == 1)
      @jresd = Safrano::XJSON.v1_parse_coll last_response.body
      @jresd_links = @jresd
    else
      @jresd = Safrano::XJSON.parse_coll last_response.body
      @jresd_links = @jresd['results']
    end


    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jresd_links.size)

  end

  def assert_last_resp_onelink_parseable
    @jresd_links = Safrano::XJSON.parse_one last_response.body
    assert @jresd_links.has_key? :uri
  end

  # this is implicitely v2
  def assert_last_resp_coll_parseable_ok(type_name, exp_size, version=2)
    unless  last_response.ok?
      require 'pry'
      binding.pry
    end
    assert last_response.ok?, "Last response status is not ok but #{last_response.status}"
    assert_last_resp_coll_parseable(type_name, exp_size, version)
  end
  
  # this is implicitely v2
  def assert_last_resp_coll_parseable_nocast_ok(type_name, exp_size, version=2)
    assert last_response.ok?, "Last response status is not ok but #{last_response.status}"
    assert_last_resp_coll_parseable_nocast(type_name, exp_size, version)
  end
  # list of $links
  def assert_last_resp_links_parseable_ok(type_name, exp_size, version=2)
    assert last_response.ok?, "Last response status is not ok but #{last_response.status}"
    assert_last_resp_links_parseable(type_name, exp_size, version)
  end

  # single $links  for x_to_one navigation
  def assert_last_resp_onelink_parseable_ok
    assert last_response.ok?, "Last response status is not ok but #{last_response.status}"
    assert_last_resp_onelink_parseable
  end
  
# def assert_last_resp_coll_parseable_created(type_name, exp_size)
  def assert_last_resp_created_one(type_name, &proc)
    unless  last_response.created?
      require 'pry'
      binding.pry
    end
    assert last_response.created?, "Last Response Status-Code is not *201 created* but #{last_response.status}"
    
    assert_last_resp_entity_parseable(type_name) 
    
    if block_given?
      @idnew = proc.call(@jresd)
      # uri of created ressource
      
      newloc = "#{uribase(last_request)}/#{type_name}(#{@idnew})"
      assert_equal newloc, @jresd[:__metadata][:uri]
      assert last_response.headers.has_key?('Location'), "POST response is missing Location header"
      assert_equal newloc, last_response.headers['Location']
    end
  end
  
  def assert_last_resp_nocast_created_one(type_name)
    assert last_response.created?
    assert_last_resp_enty_no_cast(type_name)
  end

  def assert_last_resp_coll_parseable_success(type_name, exp_size)
    assert last_response.successful?
    assert_last_resp_coll_parseable(type_name, exp_size)
  end



  # $expand checks
  def assert_deferred_attrib(jresd, attr)
    assert (enty_uri = jresd[:__metadata][:uri])
    assert_kind_of Hash, jresd[attr]
    assert_kind_of Hash, jresd[attr][:__deferred]
    assert_equal "#{enty_uri}/#{attr}",
                  jresd[attr][:__deferred][:uri]
  end

  def assert_not_expanded_entity(expected, jresd, attr)

    assert_sequel_values_equal(expected, jresd)
    assert_deferred_attrib(jresd, attr)

  end

  def assert_metadata_uri(entity, jresd, &proc)
      assert_equal "#{uribase(last_request)}/#{proc.call(entity)}",
                  jresd[:__metadata][:uri]
  end

  def assert_expanded_attrib(expanded, jresd_attr, proc)
    assert_kind_of Hash, jresd_attr
    assert_kind_of Hash, jresd_attr[:__metadata]

    assert_metadata_uri(expanded, jresd_attr, &proc)
                  
    assert_sequel_values_equal(expanded, jresd_attr)
  end

  def assert_expanded_attrib_mult(expcoll, jresd_attr_coll, proc)
    expcoll.each_with_index{|exp, idx|
      assert (jresd_attr = jresd_attr_coll[idx])
      assert_expanded_attrib(exp, jresd_attr, proc)
    }

  end

  # need to be called with a block that returns the Odata address (uri) of
  # the expected navigated attribute.
  def assert_expanded_entity(expected, jresd, attr, &proc)

    assert_sequel_values_equal(expected, jresd)

    expanded = expected.send(attr)
    if ( expanded.nil? )  # empty attrib
      assert_equal({}, jresd[attr])
    else
      assert_expanded_attrib(expanded, jresd[attr], proc)
    end
  end

  # need to be called with a block that returns the Odata address (uri) of
  # the expected navigated attribute.
  def assert_expanded_entity_mult(expected, jresd, attr, &proc)

    assert_sequel_values_equal(expected, jresd)

    expanded = expected.send(attr)
    assert_kind_of Array, jresd[attr][:results]  # Warning, v2 only

    assert_expanded_attrib_mult(expanded, jresd[attr][:results], proc)  # Warning, v2 only
  end

end

module TM_CORSTestCase
  CORS_H_ACAM = 'access-control-allow-methods'
  CORS_H_ACAO = 'access-control-allow-origin'
  
  def assert_cors_ok_allow(methods: , origin: )
    assert last_response.ok?
    
    
    #check CORS allow headers
    assert last_response.headers.has_key?(CORS_H_ACAM), "Missing CORS allow-methods header"
    assert last_response.headers.has_key?(CORS_H_ACAO) , "Missing CORS allow-origin header"
    
    # check access-control-allow-methods
    cors_acam = last_response.headers[CORS_H_ACAM]
    
    methods.each{|msym|
      meth = msym.to_s.upcase
      assert cors_acam.include?(meth), "CORS Allow methods does not include #{meth}"
    }
    
    # check access-control-allow-origin
    
    cors_acao = last_response.headers[CORS_H_ACAO]
    assert_include [origin, '*'], cors_acao
    
  end
  
  def assert_cors_not_allow
    assert last_response.ok?
    
    cors_acam = last_response.headers.has_key?(CORS_H_ACAM)
    cors_acao = last_response.headers.has_key?(CORS_H_ACAO)
    #at least one of the cors headers should be missing, or both
    # then it's considerd disallowed
    assert ( (cors_acao == false ) || ( cors_acam == false ) ) 
  end
  
  def assert_cors_ok_not_allow(methods: , origin: )
    assert last_response.ok?
        
    #check CORS allow headers
    assert last_response.headers.has_key?(CORS_H_ACAM), "Missing CORS allow-methods header"
    assert last_response.headers.has_key?(CORS_H_ACAO) , "Missing CORS allow-origin header"
    
    # check access-control-allow-methods
    cors_acam = last_response.headers[CORS_H_ACAM]
    
    #we check that the passed methods are NOT allowed
    methods.each{|msym|
      meth = msym.to_s.upcase
      assert !cors_acam.include?(meth), "CORS Allow methods is allowed #{meth} but should not"
    }
    
    # check access-control-allow-origin
    
    cors_acao = last_response.headers[CORS_H_ACAO]
    assert_include [origin, '*'], cors_acao
    
  end
end
module Safrano
  module Test
    module Methods
      include ::Rack::Test::Methods
      include TM_Model_Compare_Helper
      include TM_ODataModelTestCase
      include TM_CORSTestCase
    end
    module Unit
      class TestCase < ::Test::Unit::TestCase
        def self.startup
          Sequel.datetime_class = DateTime
          Sequel.default_timezone = :utc 
        end
        def self.shutdown
          Sequel.datetime_class = DateTime
          Sequel.default_timezone = :utc 
        end  
        #redefine this to add middlewares before run app(eg. cors)
        def safrano_builder_mwuses_before
          ->(builder){ builder.use ::Rack::Lint }
        end
        
        #redefine this to add middlewares after run app
        def safrano_builder_mwuses_after
          ->(builder){ builder.use ::Rack::Lint }
        end
        
        # normally dont redefine this
        def app
          tc = self
          ::Rack::Safrano::Builder.new do
            tc.safrano_builder_mwuses_before.call(self)
            run tc.safrano_app
            tc.safrano_builder_mwuses_after.call(self)
          end
        end              
      end
    end
    module Rack
      module Unit
        class TestCase < ::Safrano::Test::Unit::TestCase
          include ::Safrano::Test::Methods
        end
      end
    end
  end

end
