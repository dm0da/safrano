
require 'test/unit'
require_relative './db_helper.rb'


class Class
  def descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end
end

ENV['RACK_ENV'] = 'none'
TMPDIR = Dir.mktmpdir
at_exit{ FileUtils.remove_entry TMPDIR }


class TCWithoutDB < Test::Unit::TestCase

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

end
