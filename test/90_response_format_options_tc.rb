#!/usr/bin/env ruby

require_relative './test_chinook_db.rb' # Chinook-DB testdata

class ResponseFormatNoDeffer_TC < ChinookDBTest

  def safrano_app
    super.response_format_options :skip_deferred
  end
   
  def test_get_entity
    
    get '/Album(1)'
    
    assert_last_resp_entity_parseable_ok('Album')
    assert_sequel_values_equal(Album[1], @jresd,
                               uri: "Album(1)")

    assert_nil @jresd['artist']  # deferred
    assert_nil @jresd['tracks']  # deferred
    

  end
  def test_get_coll
    get '/Album'
    
    assert_last_resp_coll_parseable_ok('Album', Album.count)
    expected = Album.all.to_a
    assert_sequel_coll_valuesonly_equal(expected, @jresd_coll)


    assert_nil @jresd_coll.first['artist']  # deferred
    assert_nil @jresd_coll.first['tracks']  # deferred
  end
end

class ResponseFormatNoMeta_TC < ChinookDBTest

  def safrano_app
    super.response_format_options :skip_metadata
  end
  
  def test_get_entity
    get '/Album(1)'
    
    assert_last_resp_entity_parseable_ok()
    assert_sequel_values_only_equal(Album[1], @jresd)
  
    assert_not_nil @jresd[:artist]  # deferred
    assert_not_nil @jresd[:tracks]  # deferred
    assert_nil @jresd[:__metadata]
    assert_nil @jresd['__metadata']       
  end
  def test_get_coll
    get '/Album'
    
    assert_last_resp_coll_parseable_ok(nil, Album.count)
    expected = Album.all.to_a

    assert_sequel_coll_valuesonly_equal(expected, @jresd_coll)

    assert_not_nil @jresd_coll.first[:artist]  # deferred
    assert_not_nil @jresd_coll.first[:tracks]  # deferred
    assert_nil @jresd_coll.first[:__metadata]
    assert_nil @jresd_coll.first['__metadata']       
  end
  
end

class ResponseFormatNoMetaNoDeferr_TC < ChinookDBTest

  def safrano_app
    super.response_format_options :skip_metadata, :skip_deferred
  end
  
  def test_get_entity
    get '/Album(347)'
    
    assert_last_resp_entity_parseable_ok()
    assert_sequel_values_only_equal(Album[347], @jresd)

    assert_nil @jresd[:artist]  # deferred
    assert_nil @jresd[:tracks]  # deferred
    assert_nil @jresd[:__metadata]
    assert_nil @jresd['__metadata']      
   
  end
  def test_get_coll
    get '/Album'
    
    assert_last_resp_coll_parseable_ok(nil, Album.count)
    expected = Album.all.to_a

    assert_sequel_coll_valuesonly_equal(expected, @jresd_coll)

    assert_nil @jresd_coll.first[:artist]  # deferred
    assert_nil @jresd_coll.first[:tracks]  # deferred
    assert_nil @jresd_coll.first[:__metadata]
    assert_nil @jresd_coll.first['__metadata']   
    
  end
end
