#!/usr/bin/env ruby



# add support for MERGE...
module Rack
  module Test

    module Methods

      # Issue a MERGE request for the given URI.
      #
      # Example:
      #   xmerge "Customers(8)"

      def xmerge(uri, params = {}, env = {}, &block)
        env.merge!(:method => "MERGE", :params => params)
        request(uri, env, &block)
      end
    end
  end
end
