#!/usr/bin/env ruby

require 'test/unit'

require_relative '../lib/safrano.rb'

class TC_Safrano < TCWithoutDB
  include Safrano

  ## test taht version is accessible
  def test_version
    assert_nothing_raised do
      puts "Safrano Version #{Safrano::VERSION}"
    end
  end
  
  def assert_transition_match(tr, pstart, pdone, premain: nil)
    tres = tr.result(pstart)
    assert tres.match
    assert_equal premain, tres.path_remain
    assert_equal(pdone, tres.path_done)  
  end

  def assert_transition_nomatch(tr, pstart)
    assert_nil tr.result(pstart)
  end
  

  ## TransitionEnd = Transition.new('\A(\/?)\z', trans:'transition_end')
  def test_transition_end
    assert_transition_match(TransitionEnd, '', '')

    assert_transition_match(TransitionEnd, '/', '/')

    assert_transition_nomatch(TransitionEnd, 'xfdf' )
    
    assert_transition_nomatch(TransitionEnd, '/x' )

  end

  ##     TransitionMetadata = Transition.new('\A(\/\$metadata)(.*)', trans:'transition_metadata')
  def test_transition_metadata
    assert_transition_match(TransitionMetadata, '/$metadata', '/$metadata', premain: '')

    assert_transition_match(TransitionMetadata, '/$metadata/', '/$metadata', premain: '/')

    assert_transition_nomatch(TransitionMetadata, '/$metatata' )
    
    assert_transition_nomatch(TransitionMetadata, '$metatata' )  
  
  end
end
