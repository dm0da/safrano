
require 'test/unit'
require 'pp'
require 'set'
require_relative '../lib/safrano.rb'

require_relative 'test_botanics_db.rb'
require_relative '000_api_botanics_model.rb'

# only for testing
module API_Testing
  refine Safrano::Media::Static do
    attr_reader :root
  end
end
using API_Testing


class ODataBotanicsAPI_TCMedia_App2 < Safrano::ServerApp
end
class ODataBotanicsAPI_TCMedia_App3 < Safrano::ServerApp
end
class ODataBotanicsAPI_TCMedia_Err_App < Safrano::ServerApp
end

class API_Media_TC < BotanicsDBTest

  MediaRoot = File.absolute_path('xyz/media', File.dirname(__FILE__))
  MediaRootAlt = File.absolute_path('abc/medium', File.dirname(__FILE__))
  
  # cleanup directories potentially created in testcases
  def delete_media_dirs
    Dir.delete('Breeder/tmp') if File.exist? 'Breeder/tmp'
    Dir.delete('Breeder') if File.exist? 'Breeder'
    Dir.delete('Photo/tmp') if File.exist? 'Photo/tmp'
    Dir.delete('Photo') if File.exist? 'Photo'
    Dir.delete('NotAMedia') if File.exist? 'NotAMedia'
    %w(xyz/media/Photo xyz/media xyz).each do |dn|
      tmp = File.join(dn, 'tmp')
      Dir.delete(tmp) if File.exist?(tmp)
      Dir.delete(dn) if File.exist?(dn)
      tdn = File.join('test', dn)
      tmp = File.join(tdn, 'tmp')
      Dir.delete(tmp) if File.exist?(tmp)
      Dir.delete(tdn) if File.exist?(tdn)
    end
    
    %w(abc/medium/Photo abc/medium abc).each do |dn|
      Dir.delete(dn) if File.exist?(dn)
      tdn = File.join('test', dn)
      Dir.delete(tdn) if File.exist?(tdn)
    end
    
  end
  
  def setup
    delete_media_dirs
  end
  
  def teardown
    delete_media_dirs
  end
  
 
  
  def test_publish_media_entity_default_mediadir


    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo 
        
      end
      ODataBotanicsAPI_TCMedia_App.new
    end
    
    # check the default media handler is set
    md = Photo.media_handler
    assert_kind_of Safrano::Media::Static, md
    
    # default root media dir is the current wd
    assert_equal Pathname(Dir.pwd), md.root
    
    # check the media dir was created
    assert Dir.exist?(File.join(Dir.pwd, 'Photo'))
  end

  def test_publish_media_entity_static_handler

    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App2.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo do 
          use Safrano::Media::Static, :root => MediaRoot
        end

        
      end    
      ODataBotanicsAPI_TCMedia_App2.new  
    end
    # check the requested static media handler is set    
    md = Photo.media_handler
    assert_kind_of Safrano::Media::Static, md
    assert_equal Pathname(MediaRoot) , md.root
    
    # check the media dir was created
    assert Dir.exist?(MediaRoot)
    assert Dir.exist?(File.join(MediaRoot, 'Photo'))
  
  
    # check that the default dir is not created additionally when 
    # requesting a non-default one
    assert MediaRoot != Dir.pwd
    assert_false Dir.exist?(File.join(Dir.pwd, 'Photo'))
  end

  # change media handler (mostly for testing)
  def test_publish_change_media_entity_handler

    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App3.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo do 
          use Safrano::Media::Static, :root => MediaRoot 
        end

      end   
      ODataBotanicsAPI_TCMedia_App3.new   
    end
    # check the requested static media handler is set    
    md = Photo.media_handler
    assert_kind_of Safrano::Media::Static, md
    assert_equal Pathname(MediaRoot) , md.root
    
    Photo.use Safrano::Media::StaticTree, :root => MediaRootAlt
    md = Photo.media_handler
    assert_kind_of Safrano::Media::StaticTree, md
    assert_equal Pathname(MediaRootAlt), md.root
    # check the media dir was created
    assert Dir.exist?(MediaRootAlt)
    assert Dir.exist?(File.join(MediaRootAlt, 'Photo'))
  
  end

  def test_publish_media_entity_err
    assert_raise Safrano::API::MediaModelError do
      ODataBotanicsAPI_TCMedia_Err_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model  NotAMedia
        
      end
      ODataBotanicsAPI_TCMedia_Err_App.new
    end
    
  end


end
