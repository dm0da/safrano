
require_relative './test_botanics_db.rb' # Botanics-DB testdata

module EntityMedia_TM

#  PhotoFlatDir = 'botanics/media/flat/Photo'.freeze
#  PhotoTreeDir = 'botanics/media/tree/Photo'.freeze
  PhotoFlatDir = File.join(TMPDIR, 'botanics/media/flat/Photo').freeze
  PhotoTreeDir = File.join(TMPDIR, 'botanics/media/tree/Photo').freeze
  
  # a few helper methods
  def assert_photomedia_entity(enty,id,mimetype,ressource_version='1')
    assert_equal mimetype, enty[:__metadata][:content_type]
    assert_equal "#{uribase(last_request)}/Photo(#{id})/$value?version=#{ressource_version}", 
                 enty[:__metadata][:media_src]
    assert_equal "#{uribase(last_request)}/Photo(#{id})/$value", 
                  enty[:__metadata][:edit_media]
  end

  def assert_new_photomedia(idlist_old)
    assert_new_photomedia_entity(idlist_old)
    #TODO test quirks mode array
  end
 
  # POST response returns a non-array object
  def assert_new_photomedia_entity(idlist_old)
    idnew = @jresd[:id]
      
    # check the returned id is a new one
    assert_false idlist_old.include?(idnew), "wrong new id #{idnew}"
      
    newme = @jresd.first
        
    yield(idnew, newme) if block_given?
    
  end 
  
  # POST response returns an array of 1
  def assert_new_photomedia_array(idlist_old)
    idnew = @jresd_coll.first[:id]
      
    # check the returned id is a new one
    assert_false idlist_old.include?(idnew), "wrong new id #{idnew}"
      
    newme = @jresd_coll.first
        
    yield(idnew, newme) if block_given?
    
  end
  
  def assert_notnew_photomedia(idlist_old)
  
    upserted = @jresd
      
    # check the returned id is NOT a new one
    assert idlist_old.include?(@jresd.id), "wrong id #{@jresd.id}"
      
        
    yield(upserted) if block_given?
    
  end  
  
  def mock_upload_media(fname, mimetype='image/jpeg')
    data = Dir.chdir(TMPDIR) do
      IO.binread("botanics/uploadfiles/#{fname}")
    end
    yield data, fname, mimetype
  end
  
  def assert_photomedia_uploaded(data,id,fname,ressource_version='1')
  # check that the file was "uploaded" to the media dir
    Dir.chdir(File.dirname(__FILE__)) do
      if Photo.media_handler.class ==  Safrano::Media::Static
          
            mdir = "#{PhotoFlatDir}/#{id}"
            mfile = "#{mdir}/#{ressource_version}"
            assert File.exist?( mfile )
          
            assert_equal data, IO.binread(mfile)
            
            # we dont keep history of all versions;
            # only the current one ! 
            assert_equal 1, Dir.glob("#{mdir}/*").size
          
      elsif Photo.media_handler.class == Safrano::Media::StaticTree
        
          idpath = Safrano::Media::StaticTree.path_builder([id])
          mdir = "#{PhotoTreeDir}/#{idpath}"
          mfile = "#{mdir}/#{ressource_version}"
          assert File.exist?( mfile ), "File not exists: #{mfile}"
          
          assert_equal data, IO.binread(mfile)
      end
    end
  end  

  
  def assert_no_photomedia_uploaded(id)
  # check that no file is present in the media dir
  # (but we dont care if the directory itself is here, and we dont care about subdirectories)
    
    Dir.chdir(File.dirname(__FILE__)) do
      if Photo.media_handler.class ==  Safrano::Media::Static
      
          mdir = "#{PhotoFlatDir}/#{id}"
          assert_equal 0, Dir.glob("#{mdir}/*").size
          
      elsif Photo.media_handler.class ==  Safrano::Media::StaticTree
          idpath = Safrano::Media::StaticTree.path_builder([id])
          mdir = "#{PhotoTreeDir}/#{idpath}"
          Dir.each_child(mdir){|c| assert File.directory?(c)}
      end
    end
  end  
  
  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end


  # simple get media entity
  def test_get_media_entity
    get '/Photo(1)'
    assert_last_resp_entity_parseable_ok('Photo')
    assert_sequel_media_values_equal(Photo[1], @jresd){|expm| "Photo(#{expm.id})"}
    
    # check that content_type is hidden on entity 
    # attribute level (only visisble as metadata)
    assert_nil @jresd[:content_type]
    
  end
  
  # get related media entities
  def test_get_related_media_entities
    get '/Cultivar(5)/photo'
    assert last_response.ok?
  end
  
  # simple get media entity $value
  def test_get_media_entity_value

    mock_upload_media('Topaz_Apfel.jpg') do |data,fname,mimetype|

      get '/Photo(4)/$value'

      assert last_response.ok?
      assert_equal mimetype, last_response.content_type 
      assert_equal data.bytesize, last_response.content_length 

      assert_equal data, last_response.body.to_s
    end
  end
  
  # test delete media entity
  def test_delete_media_entity
  
    # 1. get the media entity and check content is not empty
    get '/Photo(7)'
    assert_last_resp_entity_parseable_ok('Photo')
    assert_sequel_media_values_equal(Photo[7], @jresd){|expm| "Photo(#{expm.id})"}
    
    # 2. get the media ressource and check content is not empty
    mock_upload_media('Tree-Identification-1.jpg') do |data,fname,mimetype|

      get '/Photo(7)/$value'

      assert last_response.ok?
      assert_equal mimetype, last_response.content_type 
      assert_equal data.bytesize, last_response.content_length 

      assert_equal data, last_response.body.to_s
    end
    
    # 3. delete it
    delete  '/Photo(7)'
    
    # 4. check it's been deleted
    assert Photo.where(id: 7).all.empty?
    
    assert_no_photomedia_uploaded(7)
  end
  
  # simple create (upload); ie POST 
  def test_create_media_entity
    mock_upload_media('Rubin_apple.jpg') do |data,fname,mimetype|
      idlist_old = Photo.to_a.map{|r| r.id}
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
      
      post '/Photo', data

      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

        # check that the file was "uploaded" to the media dir
        assert_photomedia_uploaded(data,idnew,fname)
      end
    end
  end

  # simple create (upload); ie POST with Slug RFC2047 encoded
  def test_create_media_entity_slug_2047
    filename = 'Köstlische_aus_Charneux_Deutsche_Pomologie_-_Birnen_-_034.jpg'
    mock_upload_media(filename) do |data,fname,mimetype|
      idlist_old = Photo.to_a.map{|r| r.id}
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', Safrano::RFC2047.encode(fname)
      
      post '/Photo', data

      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

        # check that the file was "uploaded" to the media dir
        assert_photomedia_uploaded(data,idnew,fname)
        
          # 3. According to the model, the slug is mapped to Photo.name ... check
        assert_equal filename, Photo[idnew].name
        assert_equal filename, newme[:name]
      end
    end
  end


#  simple replace media-ressource
# 
# [before 0.4.1] 1/TopazeCat.png  -->  1/pommes-topaz.jpg
# 1/1 (TopazeCat.png)  -->  1/2 (pommes-topaz.jpg)
  def test_replace_media_ressource
    mock_upload_media('pommes-topaz.jpg') do |data,fname,mimetype|
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
      
      put '/Photo(1)/$value', data
  
      assert last_response.no_content?
  
      # check that the file was replaced in the media dir
      # starting from 0.4.1 replace means increaded ressource_version

      assert_photomedia_uploaded(data,1,fname, '2')
      
      # check that the mimetype was updated on DB (png-->jpg)
      assert_equal mimetype, Photo[1].content_type
      
    end
  end
  
#  simple replace navigated media-ressource
#  Breeder(4)/photo   Porträt_Ernest_Baltet.jpg  
#    -->  Porträt_Charles_Appolinaire_Baltet_Pomologische_Monatshefte_1867.jpg
# Note: Breeder(4)/photo is Photo(5) 
  def test_replace_navigated_media_ressource
    newf = 'Porträt_Charles_Appolinaire_Baltet_Pomologische_Monatshefte_1867.jpg'
    mock_upload_media(newf) do |data,fname,mimetype|

      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      # Rack::Lint only ASCII in http headers --> encode
      header 'Slug', Safrano::RFC2047.encode(fname)
     
      get '/Breeder(4)/photo'
      assert_last_resp_entity_parseable_ok('Photo')
      assert_photomedia_entity(@jresd,5,mimetype)
  
      # PUT on not nil navigated attribute $value
      put '/Breeder(4)/photo/$value', data
      
      assert last_response.no_content?
      assert last_response.successful?
      
      # starting from 0.4.1 replace means increaded ressource_version
      assert_photomedia_uploaded(data,5,fname, '2')
        
    
      ## check that the  media entity is still linked to 
      ## Breeder 4  in the parent Model entity
      Breeder[4].reload
      assert_equal 5, Breeder[4].photo_id
     
    end
  end
  
  # delete existing media-entity by nav
  # expected result:
  # 1. the navigation link is removed (ie. FK nulled)
  # 2. the media entity+MR is deleted
  
  # Note: we delete /Breeder(6)/photo which is Photo(8)
  def test_delete_nav_media_entity  
   # 1. get the media entity and check content is not empty
    get '/Breeder(6)/photo'
    assert_last_resp_entity_parseable_ok('Photo')
    assert_sequel_media_values_equal(Photo[8], @jresd){|expm| "Photo(#{expm.id})"}
    
    # 2. get the media ressource and check content is not empty
    mock_upload_media('james-grieve.jpg') do |data,fname,mimetype|

      get '/Breeder(6)/photo/$value'

      assert last_response.ok?
      assert_equal mimetype, last_response.content_type 
      assert_equal data.bytesize, last_response.content_length 

      assert_equal data, last_response.body.to_s
    end
    
    # 3. delete it
    delete  '/Breeder(6)/photo'
    
    # 4. check it's been deleted
    Breeder[6].reload

    assert_nil Breeder[6].photo_id
    assert Photo.where(id: 8).all.empty?
    
    assert_no_photomedia_uploaded(8)  
    
    # 5. profit
  end
  
  # get existing media-entity by nav
  def test_get_nav_media_entity
    get '/Breeder(1)/photo'
    assert_last_resp_entity_parseable_ok('Photo')
    assert_photomedia_entity(@jresd,Breeder[1].photo_id,'image/png')
  end

  # get non-existing media-entity nav
  def test_get_empty_nav_media_entity
    get '/Breeder(2)/photo'
    assert_last_resp_empty_entity_ok
  end

  # create (upload); ie POST from a navigation coll. attribute
  def test_create_media_entity_from_navcoll
    idlist_old = Photo.to_a.map{|r| r.id}
    mock_upload_media('Rubin_apple.jpg') do |data,fname,mimetype|
  
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
      
      # POST on navigated collection
      post '/Cultivar(1)/photo', data
      
      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

      
        # check that the file was "uploaded" to the media dir
        assert_photomedia_uploaded(data,idnew,fname)
    
        # check that the created media entity is *automatically* linked to 
        # Cultivar(1)
        # 1. in the response "photo" payload
        assert_equal 1, newme[:cultivar_id]
        
        # 2. in the Model entity
        assert_equal 1, Photo[idnew].cultivar_id
      end
    end
  end

  # create (upload); ie POST from a navigation coll. attribute
  # with a RFC2047 encoded slug
  def test_create_media_entity_from_navcoll_rfc2047
    idlist_old = Photo.to_a.map{|r| r.id}
    filename = 'Köstlische_aus_Charneux_Deutsche_Pomologie_-_Birnen_-_034.jpg'
    mock_upload_media(filename) do |data,fname,mimetype|
  
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', Safrano::RFC2047.encode(fname)
      
      # POST on navigated collection
      post '/Cultivar(23)/photo', data
      
      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

      
        # check that the file was "uploaded" to the media dir
        assert_photomedia_uploaded(data,idnew,fname)
    
        # check that the created media entity is *automatically* linked to 
        # Cultivar(23)
        # 1. in the response "photo" payload
        assert_equal 23, newme[:cultivar_id]
        
        # 2. in the Model entity
        assert_equal 23, Photo[idnew].cultivar_id
        
        # 3. According to the model, the slug is mapped to Photo.name ... check
        assert_equal filename, Photo[idnew].name
      end
    end
  end

  
    # MASS create (upload); ie POST from a navigation coll. attribute
  def test_MASS_create_media_entity_from_navcoll
    
    mock_upload_media('220px-Topas_halbiert.jpg') do |data,fname,mimetype|
  
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
      
      20.times do  # why not...
      
        idlist_old = Photo.to_a.map{|r| r.id}
             
        
        # POST on navigated collection
        post '/Cultivar(5)/photo', data
        
        assert_last_resp_created_one('Photo')
        assert_new_photomedia(idlist_old) do |idnew, newme|
  
          assert_photomedia_entity(newme,idnew,mimetype)
  
        
          # check that the file was "uploaded" to the media dir
          assert_photomedia_uploaded(data,idnew,fname)
      
          # check that the created media entity is *automatically* linked to 
          # Cultivar(5)
          # 1. in the response "photo" payload
          assert_equal 5, newme[:cultivar_id]
          
          # 2. in the Model entity
          assert_equal 5, Photo[idnew].cultivar_id
        end # 20 times
      end
    end
  end
  
  # create (upload); ie POST to a NULL navigation single attribute
  def test_create_media_entity_from_nil_navsingle
    idlist_old = Photo.to_a.map{|r| r.id}
    mock_upload_media('thomas-laxton.jpg') do |data,fname,mimetype|
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
  
      get '/Breeder(3)/photo'
      assert_last_resp_empty_entity_ok
  
      # POST on nil navigated attribute
      post '/Breeder(3)/photo', data
      
      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

        assert_photomedia_uploaded(data,idnew,fname)
        
    
        # check that the created media entity is *automatically* linked to 
        # Breeder 3
     
        # in the parent Model entity
        assert_equal idnew, Breeder[3].photo_id
      end
      
    end
  end

  
  # POST media entity like for a create but it already exists
  #    --> error 
  def test_rePOST_media_entity
  
    mock_upload_media('thomas-laxton.jpg') do |data,fname,mimetype|
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
  
      get '/Photo(1)'
      assert_last_resp_entity_parseable_ok('Photo')
      assert_sequel_media_values_equal(Photo[1], @jresd){|expm| "Photo(#{expm.id})"}
 
      post '/Photo(1)', data 
      assert_422_unprocessable_error('ressource', 'already exists')
      
    end

  end

  #  create (upload) with POST to a NOT-NULL navigation single attribute
  # Error "already exist" 
  def test_create_media_entity_from_notnil_navsingle
    idlist_old = Photo.to_a.map{|r| r.id}
    mock_upload_media('thomas-laxton.jpg') do |data,fname,mimetype|
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
  
      get '/Breeder(4)/photo'
      assert_last_resp_entity_parseable_ok('Photo')
  
      # POST on NOT-nil navigated attribute
      post '/Breeder(4)/photo', data

      assert_422_unprocessable_error('ressource', 'already exists')
         
    end
  end
    
  # upsert;  PUT to a NULL navigation single attribute $value
  # Warning: PUT to a media entity URL would try to update the media entity data 
  #          (not the linked media ressource itself)
  # To upsert the linked media ressource itself, 
  # we need to use PUT .../$value
  def test_upsert_media_ressource_from_notnil_navsingle
    idlist_old = Photo.to_a.map{|r| r.id}
    mock_upload_media('Salins-les-Bains-La-ligne-de-Mouchard-e-Salins.jpeg') do |data,fname,mimetype|
    
      header 'Accept', 'application/json'
      header 'Content-Type', mimetype
      header 'Slug', fname
  
      get '/Breeder(5)/photo'
      assert_last_resp_empty_entity_ok
  
      # PUT on nil navigated attribute --> we expect same result as 
      # POST /Breeder(5)/photo
      # ie the media entity is created, linked and file uploaded
      put '/Breeder(5)/photo/$value', data
      
      assert_last_resp_created_one('Photo')
      assert_new_photomedia(idlist_old) do |idnew, newme|

        assert_photomedia_entity(newme,idnew,mimetype)

        assert_photomedia_uploaded(data,idnew,fname)
        
    
        # check that the created media entity is *automatically* linked to 
        # Breeder 5
     
        # in the parent Model entity
        assert_equal idnew, Breeder[5].photo_id
      end
      
      
    end
  end


end

class EntityMedia_TC < BotanicsRackTest

  def EntityMedia_TC.startup
  
    super
    
    case Sequel::Model.db.opts[:adapter]
      when :sqlite
        refresh_sqlite_testdb('botanics')
      else
        refresh_postgresql_testdb('botanics')
      end
    create_dummy_tables('botanics')
    create_dummy_data('botanics')
    
    # change the Media handler
    Photo.use Safrano::Media::Static, :root => MediaFlatRoot
  
  end

  
  include EntityMedia_TM

end


class EntityMedia2_TC < BotanicsRackTest

  def EntityMedia2_TC.startup
  
    super
    
    case Sequel::Model.db.opts[:adapter]
      when :sqlite
        refresh_sqlite_testdb('botanics')
      else
        refresh_postgresql_testdb('botanics')
    end
    create_dummy_tables('botanics')
    create_dummy_data('botanics')
    
    # change the Media handler
    Photo.use Safrano::Media::StaticTree, :root => MediaTreeRoot
  
  end
  
  include EntityMedia_TM

end