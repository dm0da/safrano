#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata


# with Enabled  $batch
class BatchChangeSetContentIdTC < SailDBTest

  def setup
    header 'MinDataServiceVersion', '2.0'
    header 'DataServiceVersion', '2.0'
    header 'MaxDataServiceVersion', '2.0'
  end
  def safrano_app
    super.enable_batch 
  end

  def test_changeset_content_id

    # MVP testcase...
    inpstr = File.read('test/multipart/odata_changeset_content_id_body.txt')

    # check the data before
    nbrace = Race.count

    header 'Accept', 'application/json'
    header 'Content-Type', 'multipart/mixed; boundary=batch_48f8-3aea-2f04'
    post '/$batch', inpstr

    # the batch was processed
    assert_equal(202, last_response.status)

    # we should have two more Race entities
    assert_equal nbrace+2, Race.count

    # check that we get back a multipart response

    md = %r(multipart/mixed;\s*boundary=(\S+)).match(last_response.content_type)

    assert md
    @boundary = md[1]

    # parse the returned multipart response and check the content

    mpresp = MIME::Media::Parser.new
    assert_nothing_raised do
      mpresp.hook_multipart('multipart/mixed', @boundary)
      @mime = mpresp.parse_string(last_response.body)
    end

    @changeset_response = @mime.content.first.content
    @cset0 = @changeset_response.first.content
    @cset1 = @changeset_response[1].content
    @cset2 = @changeset_response[2].content
    @cset3 = @changeset_response[3].content
    @cset4 = @changeset_response[4].content

    # POST --> 201 accepted
    assert_equal '201', @cset0.status
    assert_equal 'application/json;charset=utf-8', @cset0.hd['content-type']

    # POST --> 201 accepted
    assert_equal '201', @cset1.status
    assert_equal 'application/json;charset=utf-8', @cset1.hd['content-type']

    # GET --> 200 json
    assert_equal '200', @cset2.status
    assert_equal 'application/json;charset=utf-8', @cset2.hd['content-type']

    # GET --> 200 json
    assert_equal '200', @cset3.status
    assert_equal 'application/json;charset=utf-8', @cset3.hd['content-type']

    # GET $value --> 200 text
    assert_equal '200', @cset4.status
    assert_equal 'text/plain;charset=utf-8', @cset4.hd['content-type']


    # check that GET $2 (cset2) returns same json payload as
    # the content-id 2 POST (cset1)
    # (considering that POST returns an array of 1 element)
    assert_nothing_raised do
      @cset2_json = JSON.parse(@cset2.content)
    end
    assert_nothing_raised do
      @cset1_json = JSON.parse(@cset1.content)
    end
    assert_equal @cset1_json['d'], @cset2_json['d']

    # check that GET $2/rtype/description (cset3) is correct
    # (should be same as for Race[12] )

    assert_nothing_raised do
      @cset3_json = JSON.parse(@cset3.content)
    end
    assert_equal Race[12].rtype.description, @cset3_json['d']['description']

    # check that GET $1/id/$value (cset4) is correct
    # (should be same id as the one returned in cset0 with content-id 1)

    assert_nothing_raised do
      @cset0_json = JSON.parse(@cset0.content)
    end
    assert_equal @cset0_json['d']['id'].to_s, @cset4.content
  end

end



