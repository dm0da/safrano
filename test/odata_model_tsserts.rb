require_relative './client.rb'
## Definitions: tsserts are an attempt to provide more threadsafe
##              rack-test asserts.
##              Especially, and most important, they dont rely
##              on the @last_response attribute,
##              but the last_response object returned from the rack-tests methods (get, puts )
##              is used, and passed anlong the assers/checks methods
##              --> like the assers helper but in a bit more functional way



module TM_ODataModelTestCase

  # check that it's a bad request and that the err.msg contains the expected keywords
  def tssert_bad_request(last_r, *keywords)
    assert last_r.bad_request?, "last response status is #{last_r.status}"
    j_message = t_parse_error_response(last_r)
    
    assert j_message, 'Returned message is nil'
    assert j_message.kind_of?(String), "Returned message is not a String but a #{j_message.class.to_s}"
    keywords.each{|kw|
      assert j_message.include?(kw), "Keyword '#{kw}' not in message : //#{j_message}//"
    }
  end
  
  def t_parse_error_response(last_r)
    j_rawd = JSON.parse last_response.body
    j_message = j_rawd['odata.error']['message']
  end  
  # reusable check on last response + json-parse
  # it + check

  def tssert_last_resp_entity_parseable_ok(last_r, type_name)
    assert last_r.ok?

    tssert_last_resp_entity_parseable(last_r, type_name)

  end
  
  def tssert_last_resp_entity_parseable(last_r,type_name)
    begin
      t_jresd = Safrano::XJSON.parse_one last_r.body
    rescue
      require 'pry'
      binding.pry
    end
    assert_equal(ns(type_name), t_jresd[:__metadata][:type], "Type #{t_jresd[:__metadata][:type]} in reponse body is wrong ")
    t_jresd
  end

 # reusable check on last response + json-parse
  # it + check

  def tssert_last_resp_complextype_parseable_ok(last_r, type_name)
    assert last_r.ok?

    t_jresd = JSON.parse last_r.body
    t_jresd = t_jresd['d']
 
    assert_equal(ns(type_name), t_jresd['__metadata']['type'])
    t_jresd
  end
  
  
  def tssert_last_resp_ct_coll_parseable_ok(last_r, type_name)
    assert last_r.ok?

    t_jresd = JSON.parse last_r.body
    t_jresd_coll = t_jresd['d']['results']

    assert_equal(ns(type_name), t_jresd_coll.first['__metadata']['type'])
    t_jresd_coll
  end
 # reusable check on last response + json-parse
  # it + check

  def tssert_last_resp_primitive_parseable_ok(last_r, type_name)
    assert last_r.ok?, "Last response status is no ok but #{last_r.status}"

    t_jresd = JSON.parse last_r.body
    t_jresd = t_jresd['d']
    
    assert_equal(type_name, t_jresd['__metadata']['type'])
    t_jresd
  end

  # same as previous but keep values unchanged (not casted)
  def tssert_last_resp_enty_ok_no_cast(last_r, type_name=nil)
    assert last_r.ok?, "Last response status is no ok but #{last_r.status}"

    tssert_last_resp_enty_no_cast(last_r, type_name=nil)

  end
    
  def tssert_last_resp_enty_no_cast(last_r, type_name=nil)


    t_jrawd = Safrano::XJSON.parse_one_nocast last_r.body

    # check type name on request only
    assert_equal(ns(type_name), t_jrawd[:__metadata][:type]) if type_name
    t_jrawd
  end
  
  # same as previous but expect an empty entity
  def tssert_last_resp_empty_entity_ok(last_r, type_name=nil)
    assert last_r.ok?

    t_jrawd = JSON.parse last_r.body

    # check emptyness
    assert_equal({}, t_jrawd['d'])
    t_jrawd
  end
  
  def tssert_last_resp_empty_coll_parseable_ok(last_r,  version = 2)
    tssert_last_resp_coll_parseable(last_r, nil, 0, version )
  end


  def tssert_last_resp_coll_parseable(last_r, type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      t_jresd = Safrano::XJSON.v1_parse_coll last_r.body
      t_jresd_coll = t_jresd
    else
      t_jresd = Safrano::XJSON.parse_coll last_r.body
      t_jresd_coll = t_jresd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, t_jresd_coll.size, 'Size of result-collection is incorrect')

    # check the returned type
    if exp_size > 0
      assert_equal(ns(type_name), t_jresd_coll.first[:__metadata][:type])
      assert_equal(ns(type_name), t_jresd_coll.last[:__metadata][:type])
    end
    t_jresd_coll
  end

  def tssert_last_resp_coll_parseable_nocast(last_r, type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      t_jrawd = Safrano::XJSON.v1_parse_coll_nocast last_r.body
      t_jrawd_coll = t_rawd
    else
      t_jrawd = Safrano::XJSON.parse_coll_nocast last_r.body
      t_jrawd_coll = t_jrawd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, t_jrawd_coll.size)

    # check the returned type
    if exp_size > 0
      assert_equal(ns(type_name), t_jrawd_coll.first[:__metadata][:type])
      assert_equal(ns(type_name), t_jrawd_coll.last[:__metadata][:type])
    end
    t_jrawd_coll
  end

  def tssert_last_resp_links_parseable(last_r, type_name, exp_size, version=2)
    @resh = nil

    if (version == 1)
      t_jresd = Safrano::XJSON.v1_parse_coll last_r.body
      t_jresd_links = t_jresd
    else
      t_jresd = Safrano::XJSON.parse_coll last_r.body
      t_jresd_links = t_jresd['results']
    end


    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, t_jresd_links.size)
    t_jresd_links
  end

  def tssert_last_resp_onelink_parseable(last_r)
    t_jresd_links = Safrano::XJSON.parse_one last_r.body
    assert t_jresd_links.has_key? :uri
  end

  # this is implicitely v2
  def tssert_last_resp_coll_parseable_ok(last_r, type_name, exp_size, version=2)
    unless  last_r.ok?
      require 'pry'
      binding.pry
    end
    assert last_r.ok?, "Last response status is not ok but #{last_r.status}"
    tssert_last_resp_coll_parseable(last_r, type_name, exp_size, version)
    
  end
  
  # this is implicitely v2
  def tssert_last_resp_coll_parseable_nocast_ok(last_r, type_name, exp_size, version=2)
    assert last_r.ok?, "Last response status is not ok but #{last_r.status}"
    tssert_last_resp_coll_parseable_nocast(last_r, type_name, exp_size, version)
  end
  
  # list of $links
  def tssert_last_resp_links_parseable_ok(last_r, type_name, exp_size, version=2)
    assert last_r.ok?, "Last response status is not ok but #{last_r.status}"
    tssert_last_resp_links_parseable(type_name, exp_size, version)
  end

  # single $links  for x_to_one navigation
  def tssert_last_resp_onelink_parseable_ok(last_r)
    assert last_r.ok?, "Last response status is not ok but #{last_r.status}"
    tssert_last_resp_onelink_parseable(last_r)
  end
  
  
  def tssert_last_resp_nocast_created_one(last_r, type_name)
    assert last_r.created?
    tssert_last_resp_enty_no_cast(last_r, type_name)
  end

  def tssert_last_resp_coll_parseable_success(last_r, type_name, exp_size)
    assert last_r.successful?
    tssert_last_resp_coll_parseable(last_r,type_name, exp_size)
  end


 

end
