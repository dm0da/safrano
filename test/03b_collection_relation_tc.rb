
require_relative 'test_helper.rb'
require_relative './test_chinook_db.rb' # Chinook-DB testdata

module MathFloorCeilFuncImplementedTest 

  def test_floor_filter
    @filtstr =  'floor(Total) eq 2'
    gcollfilt {|inv| inv.Total.floor ==  2 }

    @filtstr =  'floor(Total) eq 1'
    gcollfilt {|inv| inv.Total.floor ==  1 }


  end
  
  def test_floor_ceil
    @filtstr =  'ceiling(Total) eq 2'
    gcollfilt {|inv| inv.Total.ceil ==  2 }

    @filtstr =  'ceiling(Total) eq 1'
    gcollfilt {|inv| inv.Total.ceil ==  1 }

  end
  
end

module MathFloorCeilFuncNotImplementedTest 

  def test_floor_filter
    @filtstr =  'floor(Total) eq 2'
    gcollfilt_not_implemented 'floor'
  end
  
  def test_floor_ceil
    @filtstr =  'ceiling(Total) eq 2'
    gcollfilt_not_implemented 'ceiling'
  end
  
end

class ChinookCollectionRelationTest < ChinookDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

    # Check ordering on related attributes
  def test_related_attr_order
    get '/Customer?$orderby=salesrep/BirthDate'
    expected_tab = Customer.to_a.sort_by{ |cu| cu.salesrep.BirthDate}

    assert_last_resp_coll_parseable_ok('Customer', expected_tab.count)

# test ordering by salesrep/BirthDate'
    exp_order = expected_tab.map{|cu| cu.salesrep.BirthDate }

    resbd = @jresd_coll.map{|js| Employee[js[:SupportRepId]].BirthDate }

    y = resbd.dup.sort!

    assert_equal y, resbd
  end

  def test_nav_one_to_many
    get '/Customer(1)/invoices'
    expected_tab = Customer[1].invoices
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)

    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
  end

  def test_nav_one_to_many_filter
    get '/Customer(1)/invoices?$filter=Total eq 8.91'
    expected_tab = Customer[1].invoices_dataset.where(:Total => 8.91)

    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    
    # should also work with Edm.Decimal [0-9]+.[0-9]+M|m
    get '/Customer(1)/invoices?$filter=Total eq 8.91m'
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    
    # should also work with Float & Double ?
    get '/Customer(1)/invoices?$filter=Total eq 8.91d'
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    
    get '/Customer(1)/invoices?$filter=Total eq 8.91f'
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)    
    
    get '/Customer(1)/invoices?$filter=Total eq 891e-2f'
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    
    get '/Customer(1)/invoices?$filter=Total eq 891e-2d'
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)    
  end

  # This is queriyng things like 
  # get all Customers having invoices with total amout greater that x
  
  # Note that the are no such similar queries in OData V2
  # but is on the other side it does not explicitely says it's wrong
  # the OData V2 specs does not mention anything like that
  #  
  def test_having_filter_over_navcoll
    get '/Customer?$filter=invoices/Total ge 15.00'
    expected_set = Set.new
    Invoice.where{ Sequel[Invoice.table_name][:Total] >= 15 }.each{|i| expected_set.add(i.customer) }
    assert_last_resp_coll_parseable_ok('Customer', expected_set.count)
    assert_sequel_coll_values_equal(expected_set, @jresd_coll)
  end


  # same kind of query but  many_to_many based 
  # Tracks many_to_many Playlists
  def test_having_filter_over_many2_many_navcoll
    # all Plalist having Tracks from Album 4
    # Brute Force
    expected_set = Playlist.all.select{|pl| pl.tracks.find{|tr| tr.AlbumId == 4 } }

    # all Plalist having Tracks from Album 4
    get 'Playlist?$filter=tracks/AlbumId eq 4'
    

    assert_last_resp_coll_parseable_ok('Playlist', expected_set.count)
    assert_sequel_coll_values_equal(expected_set, @jresd_coll)
    
    #******************************************
    # all Plalist having Tracks from Album 227
    # Brute Force
    expected_set = Playlist.all.select{|pl| pl.tracks.find{|tr| tr.AlbumId == 227 } }

    # all Plalist having Tracks from Album 227
    get 'Playlist?$filter=tracks/AlbumId eq 227'
    

    assert_last_resp_coll_parseable_ok('Playlist', expected_set.count)
    assert_sequel_coll_values_equal(expected_set, @jresd_coll)
    
  end
  
  # the items of Invoice(411) whose Artist-Name (of album of track) is L.K.
  def test_nav_one_to_many_filter_deep_path
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'"

    expected_tab = [InvoiceItem[2236], InvoiceItem[2237]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
  end

  # the items of Invoice(411) whose Artist-Name (of album of track) is L.K.
  # oderderd by Track-Name
  def test_nav_one_to_many_filter_deep_path_order
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'&$orderby=track/Name"

    expected_tab = [InvoiceItem[2236], InvoiceItem[2237]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)

  end

  # same as prev. but order descending
  def test_nav_one_to_many_filter_deep_path_order_desc
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'&$orderby=track/Name desc"

    expected_tab = [InvoiceItem[2237], InvoiceItem[2236]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)

    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)

  end
  
  def test_expand_bad_request
    
    # positive test
    get '/Invoice?$expand=invoice_items/track&$top=2'
    assert_last_resp_coll_parseable_ok('Invoice',  2 )
    
    # bad requ.
    get '/Invoice?$expand=invoice_items/treck'
    assert_bad_request '$expand', 'path', 'invoice_items/treck', 'invalid'

    # bad requ.
    get '/Invoice?$expand=invoice_items/tracking'
    assert_bad_request '$expand', 'path', 'invoice_items/tracking', 'invalid'


  end
  ## expand nav attribut of target cardinality 1
  #def test_expand_to_one_attr
    #get '/Race(1)?$expand=rtype'
    #assert_last_resp_entity_parseable_ok('Race')
    #assert_expanded_entity(Race[1], @jresd, :rtype){|expanded| "RaceType(#{expanded.id})"}
  #end
  # minimaly test iso formating of date
  # theres probably some work remaining when it comes to the TZ
  
  # test  output of Time fields
  # both are valid 
#  EXPT = [Time.new(2010,03,11,0,0,0,0).iso8601,    # --> 2010-03-11T00:00:00+00:00
#          Time.utc(2010,03,11,0,0,0).iso8601  ]    # --> 2010-03-11T00:00:00Z
  EXPT = [Time.new(2010,03,11,0,0,0,0).to_edm_json,   #--> /Date(1268265600000)           
          Time.utc(2010,03,11,0,0,0).to_edm_json  ]    # --> /Date(1268265600000)
  def test_collection_date_type
    get "/Invoice?$filter=InvoiceId eq 98"
    
    assert_last_resp_coll_parseable_nocast_ok('Invoice', 1)

    assert EXPT.include?(@jrawd_coll.first[:InvoiceDate])

  end
  
  def test_rel_collection_get_date_type
    get "/Customer(1)/invoices?$filter=InvoiceId eq 98"
    
    assert_last_resp_coll_parseable_nocast_ok('Invoice', 1)

    assert EXPT.include?(@jrawd_coll.first[:InvoiceDate])

  end

#  EXPT2 = [Time.new(2020,6,18,0,0,0,0).iso8601,    # --> 2020-06-18T00:00:00+00:00
#           Time.utc(2020,6,18,0,0,0).iso8601  ]    # --> 2020-06-18T00:00:00Z        
  EXPT2 = [Time.new(2020,6,18,0,0,0,0).to_edm_json,   # --> /Date(1592438400000)          
           Time.utc(2020,6,18,0,0,0).to_edm_json  ]   # --> /Date(1592438400000)
  def test_create_on_navcol_date_type
    
    count = Customer[1].invoices.size
    
    @data = {CustomerId: 1,
             InvoiceDate: Time.utc(2020,6,18,0,0,0).to_edm_json,
             BillingAddress: 'Champs Elises',
             BillingCity: 'Paris',
             BillingState: '',
             BillingCountry: 'FR',
             BillingPostalCode: '00001',
             Total: 0.99 }
    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'

    post '/Customer(1)/invoices', pbody
    
    assert_last_resp_nocast_created_one('Invoice')
        
    assert EXPT2.include?(@jrawd[:InvoiceDate])
    
    assert_equal count + 1, Customer[1].invoices.size
    
  end
  
end

# $filter math functions round, floor, ceil
module MathFilterTests

  # $filter datetime year (better covering is in botanics suite)
  def test_year_filter
    @filtstr = 'year(InvoiceDate) ge 2000'    
    gcollfilt {|inv| inv.InvoiceDate.year  >= 2000 }
  end  
  

  def test_round_filter
    @filtstr =  'round(Total) eq 8'
    gcollfilt {|inv| inv.Total.round ==  8 }
    
    @filtstr =  'round(Total) eq 9'
    gcollfilt {|inv| inv.Total.round ==  9 }
  end

# mixin adapter specific test modules 
  case Sequel::Model.db.adapter_scheme
  when :sqlite
    include MathFloorCeilFuncNotImplementedTest 
  else
    include MathFloorCeilFuncImplementedTest 
  end

# test error handling and reporting ... eg when ceil is used instead of ceiling
  def test_unknow_func_error_in_filter
    # OData defines func. ceiling, not ceil... should be reported as error
    @filtstr =  'ceil(Total) eq 9'
    gcollfilt_bad_request 'unknown', 'ceil', 'function', '$filter'
  end
end
# run the tests on all invoices set
class ChinookCollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
  end
  include MathFilterTests

end

# same tests should pass for a limited top300  set
class ChinookLimited300CollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
    @toppar = 300
  end
  include MathFilterTests

end

# same tests should pass for a limited top1  set
class ChinookLimited1CollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
    @toppar = 1
  end
  include MathFilterTests

end


# same tests should pass on a navigated collection all invoices of Customer(2)
class ChinookNavCollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Customer(2)/invoices'
    @etype = 'Invoice'
    @ecollbase = Customer[2].invoices
  end
  include MathFilterTests

end

