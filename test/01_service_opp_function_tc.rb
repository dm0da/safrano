
require_relative './test_botanics_db.rb' # Botanics-DB testdata

class ServiceOppFunction_TC < BotanicsRackTest

  def test_function_returning_complextype
    get '/tfunc?a=1&d=1foo&x=2'
    assert_last_resp_complextype_parseable_ok('ComplexType01')
    expected = '{"d":{"__metadata":{"type":"ODataPar.ComplexType01"},"a":1,"b":{"__metadata":{"type":"ODataPar.ComplexType00"},"x":2,"d":"1foo"}}}'
    assert_equal expected, last_response.body
  end
  
  def test_function_bad_request_not_all_params
    get '/tfunc?a=1&d=1'
    assert_bad_request 'tfunc', 'Parameter', 'missing', 'x'
  end
  
  def test_function_bad_request_wrong_type
    # x should be an Int32
    get '/tfunc?a=1&d=1foo&x=baz'
    assert_bad_request 'tfunc', 'type', 'Int32', 'baz'
  end
  
  def test_function_returning_complextype_
    d = URI.encode_www_form_component 'thiß is ä string'
    get "/tfunc?a=1&d=#{d}&x=2"
    assert_last_resp_complextype_parseable_ok('ComplexType01')
    expected = '{"d":{"__metadata":{"type":"ODataPar.ComplexType01"},"a":1,"b":{"__metadata":{"type":"ODataPar.ComplexType00"},"x":2,"d":"thiß is ä string"}}}'
    assert_equal expected, last_response.body
  end
  
  def test_function_returning_primitivetype
    f = URI.encode_www_form_component  '1.6'
    get "/vfunc?i=2&f=#{f}"
    assert_last_resp_primitive_parseable_ok('Edm.Double')
    expected = '{"d":{"__metadata":{"type":"Edm.Double"},"value":3.2}}'

    assert_equal expected, last_response.body
  end
  
  def test_function_primitivetype_uuid_input
    get "/uuid_to_s?uuid=guid'e2a8b3b4-4d0d-4294-8ad9-53bc62366574'"
    assert_last_resp_primitive_parseable_ok('Edm.String')
    expected = '{"d":{"__metadata":{"type":"Edm.String"},"value":"e2a8b3b44d0d42948ad953bc62366574"}}'

    assert_equal expected, last_response.body
    
    # Pass incorrect UUID --> Bad Request
    #   missing guid'' prefrix...
    get "/uuid_to_s?uuid='e2a8b3b4-4d0d-4294-8ad9-53bc62366574'"
    assert_bad_request 'uuid_to_s', 'type', 'Edm::Guid', 'e2a8b3b4-4d0d-4294-8ad9-53bc62366574'
    
    #   invalid uuid : too short
    get "/uuid_to_s?uuid=guid'a8b3b4-4d0d-4294-8ad9-53bc62366574'"
    assert_bad_request 'uuid_to_s', 'type', 'Edm::Guid', 'a8b3b4-4d0d-4294-8ad9-53bc62366574'   
 
    #   invalid uuid : 
    get "/uuid_to_s?uuid=guid'foo'"
    assert_bad_request 'uuid_to_s', 'type', 'Edm::Guid', 'foo' 
    
    #   invalid uuid : 
    get "/uuid_to_s?uuid=12355"
    assert_bad_request 'uuid_to_s', 'type', 'Edm::Guid', '12355'            
  end  
  
  def test_function_primitivetype_uuid_create
    get '/random_uuid'
    assert_last_resp_primitive_parseable_ok('Edm.Guid')
    expected = %Q<{"d":{"__metadata":{"type":"Edm.Guid"},"value":"0a1e8945-24d0-4d86-8d8e-d5616f786c05"}}>

    assert_equal expected, last_response.body
  end   
  
  # this one has been defined with a symbol parameter name
  def test_func_name_as_symbol
    get 'func_name_sym?inp=foo'
    assert_last_resp_primitive_parseable_ok('Edm.String')
    expected = %Q<{"d":{"__metadata":{"type":"Edm.String"},"value":"foo"}}>
  end
  
  
  def test_func_with_lambda_definition
    get 'func_with_lambda_def?inp=foo'
    assert_last_resp_primitive_parseable_ok('Edm.String')
    expected = %Q<{"d":{"__metadata":{"type":"Edm.String"},"value":"foo"}}>
  end
  
  def test_function_returning_entity
    get "/first_cultivar_func"
    assert_last_resp_entity_parseable_ok('Cultivar')
    assert_sequel_values_equal(Cultivar.first, @jresd)
  end

  def test_function_returning_entity_coll
    get "/top10_cultivar_func"
    expected = Cultivar.limit(10)
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    assert_sequel_coll_values_equal(expected, @jresd_coll)
  end

  def test_function_returning_primitivetype_collection
    get "/fcount?i=3"
    assert_last_resp_primitive_parseable_ok('Collection(Edm.String)')
    expected = '{"d":{"__metadata":{"type":"Collection(Edm.String)"},"results":["0","1","2","3"]}}'

    assert_equal expected, last_response.body
  end


  def test_function_returning_ct_collection
    get "/ctfunc?i=3&d=foo"
    assert_last_resp_ct_coll_parseable_ok('ComplexType00')
    
    expected = '{"d":{"results":[{"__metadata":{"type":"ODataPar.ComplexType00"},"x":0,"d":"foo"},{"__metadata":{"type":"ODataPar.ComplexType00"},"x":1,"d":"foofoo"},{"__metadata":{"type":"ODataPar.ComplexType00"},"x":2,"d":"foofoofoo"},{"__metadata":{"type":"ODataPar.ComplexType00"},"x":3,"d":"foofoofoofoo"}]}}'

    assert_equal expected, last_response.body
  end


  # test nested complex types with Entity as sub-elements
  # use function distinct_parent_child returning Collection of ParentChild
  # complex type for Parentage overview and testing Complex Type mixed/nested output
  # PCNames = Safrano::ComplexType(parent_name: Integer, child_name: Integer)
  # ParentChild = Safrano::ComplexType(parent_id: Integer, child_id: Integer,
  #                                 :parent => Cultivar, :child => Cultivar,
  #                                 :names => PCNames )
  def test_function_returning_ct_collection_deep
    get "/distinct_parent_child"
    assert_last_resp_ct_coll_parseable_ok('ParentChild')
    
    # just do some output consistency check with first and last records 
    firstpa = Parentage.to_a.first
    firstj = @jresd_coll.first 
    assert_equal firstpa.parent_id, firstj['parent_id']
    assert_equal firstpa.parent.name, firstj['names']['parent_name']
    assert_equal firstpa.child.name, firstj['names']['child_name']
    assert_equal firstpa.parent_id, firstj['parent']['id']
    assert_equal firstpa.parent.name, firstj['parent']['name']
    assert_equal firstpa.child.name, firstj['child']['name']
    
    lastpa = Parentage.to_a.last
    lastj = @jresd_coll.last 
    assert_equal lastpa.parent_id, lastj['parent_id']
    assert_equal lastpa.parent.name, lastj['names']['parent_name']
    assert_equal lastpa.child.name, lastj['names']['child_name']
    assert_equal lastpa.parent_id, lastj['parent']['id']
    assert_equal lastpa.parent.name, lastj['parent']['name']
    assert_equal lastpa.child.name, lastj['child']['name']
  end
  
  # test nested complex types with Entity as sub-elements
  def test_function_returning_complextype_deep

    get '/cultivar_by_id?id=3'
    assert_last_resp_complextype_parseable_ok('CT_Cultivar')
    assert_equal 3, @jresd['id']
    assert_sequel_ct_values_equal(Cultivar[3], @jresd['cultivar'])
  end  
  
  # error handling
  def test_function_error_handling
    get '/error_handling?id=666'
    assert_bad_request('reason', 'Foo')
    
    get '/error_handling?id=42'
    assert_server_error  
    
    get '/error_handling?id=31415'
    assert_server_error('Baz', 'Bar', 'Bam') 
    
    get '/error_handling?id=314159'
    assert_server_error('Baz', 'Bar', 'Bam', 'error_handling') 
    
    #Sequel exception raised
    get '/error_handling?id=7'
    assert_server_error('Sequel::NotNullConstraintViolation')    
                  
    # no error case
    get '/error_handling?id=1'
    assert_last_resp_primitive_parseable_ok('Edm.String')
    assert_equal 'You have request id 1', @jresd['value']
  end
  
end
