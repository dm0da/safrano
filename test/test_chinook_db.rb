#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require 'test/unit'
require 'rack/test'
require 'json'

require_relative './chinookn3/dbmodel_test.rb' # for testdata
require_relative './odata_model_asserts.rb'
require_relative './odata_model_tsserts.rb'

module Chinook
  module Ext
    def startup
      super
      Sequel::Model.db = $DB[:chinookn3]
    end
  end
end

# base class for tests that require the chinook DB to be loaded,
# but are not Rack-Tests
class ChinookDBTestII <  Safrano::Test::Unit::TestCase
  include TM_Model_Compare_Helper
  extend Chinook::Ext

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('ChinookDBTestII')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

class ChinookDBTest < Safrano::Test::Rack::Unit::TestCase

  extend Chinook::Ext
  
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('ChinookDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    super
    header 'MaxDataServiceVersion', '2.0'
  end
 
  def safrano_app
    ODataChinookApp.new
  end
 
  def odata_namespace
    'ODataChinook'
  end
  
end

