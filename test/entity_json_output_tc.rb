#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

class EntityJsonOutputTest < SailDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  # minimally test version negotiation a bit
  def test_version_negotiation
    header 'MaxDataServiceVersion', '1.0'
    get '/Race(1)'
    assert last_response.ok?
    assert_equal '1.0', last_response.headers['DataServiceVersion']

    header 'MaxDataServiceVersion', '2.0'
    get '/Race(1)'
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']
  end

  # real test starts here
  # test getting a single entity as json. The result is same in v1 and v2
  def test_get_1_entity
    header 'MaxDataServiceVersion', '1.0'
    get_1_entity_asserts_v1_v2

    header 'MaxDataServiceVersion', '2.0'
    get_1_entity_asserts_v1_v2
  end

  # test v1 json collection --> array is directly under "d"
  def test_v1_get_collection
    header 'MaxDataServiceVersion', '1.0'
    get '/Race'
    assert_last_resp_coll_parseable_ok('Race', Race.count, 1)
    assert_sequel_coll_values_equal(Race, @jresd_coll)
  end

  # test v2 json collection  array is  under "d/results"
  def test_v2_get_collection
    header 'MaxDataServiceVersion', '2.0'
    get '/Race'
    assert_last_resp_coll_parseable_ok('Race', Race.count)
    assert_sequel_coll_values_equal(Race, @jresd_coll)

  end


  # test v1 json $links collection --> array is directly under "d"
  def test_v1_get_links
    header 'MaxDataServiceVersion', '1.0'
    get '/Race(1)/$links/Edition'
    assert_last_resp_links_parseable_ok('Edition', Race[1].Edition.count, 1)

  end

  # test v2 json $links collection  array is  under "d/results"
  def test_v2_get_links
    header 'MaxDataServiceVersion', '2.0'
    get '/Race(1)/$links/Edition'
    assert_last_resp_links_parseable_ok('Edition', Race[1].Edition.count, 2)

  end

  def test_get_bad_request
    get '/Race(xyzwew)'
    assert last_response.bad_request?
  end


  def test_get_inexisting_record
    get '/Race(99999)'
    assert last_response.not_found?
  end

end

