#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

class ManyToManyBotanicsTest < BotanicsRackTest

  def test_basic_parents
    get 'Cultivar(12)/parents'
    
    # Cultivar(12) has parents 7 and 11
    expected = Cultivar[12].parents
    assert_last_resp_coll_parseable_ok('Cultivar', 2)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}

  end
  
  def test_basic_childs
    # All childs of 'James Grieve'
    get 'Cultivar(20)/childs'
    
    # Cultivar(20) has childs 9,29,30,32
    expected = Cultivar[20].childs
    assert_last_resp_coll_parseable_ok('Cultivar', expected.size)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}

  end  

  def test_filter_childs
    get 'Cultivar(20)/childs?$filter=id eq 29'
    # filter out the one with id 29
    expected = Cultivar.where(id: 29).to_a
    assert_last_resp_coll_parseable_ok('Cultivar', 1)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
        
  end
  
  def test_filter_childs_rel
  # the childs of Cultivar(20) whose breeder Last_name is Laxton !
  # --> Cultivar(9)
    get "Cultivar(20)/childs?$filter=breeder/last_name eq 'Laxton'"
    #expected = Cultivar.where(id: 9).to_a
    expected = Cultivar[20].childs.select{|ch| ch&.breeder&.last_name == 'Laxton' }
    assert_last_resp_coll_parseable_ok('Cultivar', 1)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
        
  end
  
  def test_filter_cultivar_childs_rel
  # the cultivars having child(s) who have breeder with Last_name is Laxton !
  # --> 20 & 27
    get "Cultivar?$filter=childs/breeder/last_name eq 'Laxton'"

    #expected = [Cultivar[20], Cultivar[27]]

    expected = Cultivar.to_a.select{|c| c.childs.find{|ch| ch&.breeder&.last_name == 'Laxton'}}

    assert_last_resp_coll_parseable_ok('Cultivar', expected.size)
    assert_sequel_coll_values_equal(expected, @jresd_coll)
    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
    
   # the cultivars having child(s) who have breeder with Last_name is Saxon !
  # --> empty Set
    get "Cultivar?$filter=childs/breeder/last_name eq 'Saxon'"   
    
    assert_last_resp_empty_coll_parseable_ok    
  end    
  
  # deep rels like childs/childs
  def test_filter_cultivar_childs_childs_rel
  # the cultivars having child(s) who have childs with id >= 1
  # --> 
    get "Cultivar?$filter=childs/childs/id ge  1"

    expected = Cultivar.to_a.select{|c| c.childs.find{|ch| ch&.childs&.find{|c| c.id >= 1 }}}

    assert_last_resp_coll_parseable_ok('Cultivar', expected.size)
    assert_sequel_coll_values_equal(expected, @jresd_coll)
    
    # the cultivars having child(s) who have childs with id eq 32
    # ie the grand-parents of 32
    get "Cultivar?$filter=childs/childs/id eq  32"
    expected = Cultivar.to_a.select{|c| c.childs.find{|ch| ch&.childs&.find{|c| c.id == 32 }}}

    assert_last_resp_coll_parseable_ok('Cultivar', expected.size)
    assert_sequel_coll_values_equal(expected, @jresd_coll)
   
    # This is invalid because Cultivar(32)/parents is a collection
    #get "Cultivar(32)/parents/parents"
    #expected = Cultivar.to_a.select{|c| c.childs.find{|ch| ch&.childs&.find{|c| c.id == 32 }}}

    
    # the cultivars having child(s) who have childs with id eq 0
    get "Cultivar?$filter=childs/childs/id eq  0"
    assert_last_resp_empty_coll_parseable_ok    
  end
  def test_filter_parents
    get 'Cultivar(12)/parents?$filter=id eq 11'
    # Cultivar(12) has parents 7 and 11; filter out the one with id 11
    expected = Cultivar.where(id: 11).to_a
    assert_last_resp_coll_parseable_ok('Cultivar', 1)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
        
  end
  
  def test_filter_parents_rel
  # the parents of Cultivar(1) whose breeder Last_name is Taylor !
  # --> Cultivar(3)
    get "Cultivar(1)/parents?$filter=breeder/last_name eq 'Taylor'"
    #expected = Cultivar.where(id: 3).to_a
    expected = Cultivar[1].parents.select{|p| p&.breeder&.last_name == 'Taylor' }
    
    assert_last_resp_coll_parseable_ok('Cultivar', 1)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
        
  end

  def test_filter_cultivar_parents_rel
  # the cultivars having parent(s) who have breeder with Last_name is Taylor !
  # --> Cultivar(1)
    get "Cultivar?$filter=parents/breeder/last_name eq 'Taylor'"
    #expected = Cultivar.where(id: 1).to_a
    expected = Cultivar.to_a.select{|c| c.parents.find{|p| p&.breeder&.last_name == 'Taylor'}}
    
    assert_last_resp_coll_parseable_ok('Cultivar', 1)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cu|  "Cultivar(#{cu.id})"}
    
   # the cultivars having parent(s) who have breeder with Last_name is Saylor !
  # --> empty Set
    get "Cultivar?$filter=parents/breeder/last_name eq 'Saylor'"   
    
    assert_last_resp_empty_coll_parseable_ok    
  end  
    
end


