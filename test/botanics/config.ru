###########
# config.ru
#

#require 'rack/logger'

require_relative './dbmodel_run.rb'

puts "Safrano version #{Safrano::VERSION  }"

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::Safrano::Builder.new do
#  use Rack::ODataCommonLogger
#  use Rack::ShowExceptions
   run ODataBotanicsApp.new
  #run $server
end

 
# Rack::Handler::Thin.run final_odata_app, :Port => 9494
run final_odata_app