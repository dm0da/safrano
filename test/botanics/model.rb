# frozen_string_literal: true

class Photo < Sequel::Model(:photo)
end


class Cultivar < Sequel::Model(:cultivar)

    # Navigation attributes
    one_to_many :parentage, :class => :Parentage, :key => :cultivar_id
    one_to_many :child_parentage, :class => :Parentage, :key => :parent_id
    one_to_many :photo, :class => :Photo, :key => :cultivar_id
    many_to_one :breeder, :key => :breeder_id
    many_to_one :institute, :class => :Institute, :key => :inst_id
    
    #self referential many_to_many parents-cultivars over parentage join-table
    many_to_many :parents, class: self, join_table: :parentage,
                 left_key: :cultivar_id , right_key: :parent_id
                 
    #self referential many_to_many child-cultivars over parentage join-table
    # inverted relation id's parent_id <--> cultivar_id  
    many_to_many :childs, class: self, join_table: :parentage,
                 left_key: :parent_id , right_key: :cultivar_id                 
end

class ParentageType < Sequel::Model(:par_type)
end

class Breeder < Sequel::Model(:breeder)
  one_to_many :cultivar, :key => :breeder_id
  many_to_one :photo, :key => :photo_id
end

class Institute < Sequel::Model(:institute)
end

class Parentage < Sequel::Model(:parentage)
  # A parentage is for a given cultivar; there can be multiple parentage records
  # (type: father or mother), or one (type seedling) or none
  #
    many_to_one :child, :class => :Cultivar, :key=>:cultivar_id
    many_to_one :parent, :class=> :Cultivar, :key=>:parent_id
    many_to_one :par_type, :class=>:ParentageType, :key => :ptype_id

end


class Photo < Sequel::Model(:photo)
end

ComplexType00 = Safrano::ComplexType(:x => Integer, :d => String )
ComplexType01 = Safrano::ComplexType(:a => Integer, :b => ComplexType00 )

# complex type for Parentage overview and testing Complex Type mixed/nested output
PCNames = Safrano::ComplexType(parent_name: Integer, child_name: Integer)
ParentChild = Safrano::ComplexType(parent_id: Integer, child_id: Integer,
                                   :parent => Cultivar, :child => Cultivar,
                                   :names => PCNames )


CT_Cultivar = Safrano::ComplexType(:id => Integer, :cultivar => Cultivar )

create_dummy_tables('botanics')

class Posts < Sequel::Model(:posts)
end

class Multi_PK < Sequel::Model(:multi_pk)
end

# create a few test records
create_dummy_data('botanics')

### SERVER Part #############################
include Safrano::Edm
 
#MediaRoot = File.absolute_path('media', File.dirname(__FILE__))
MediaRoot = File.absolute_path('botanics/media', TMPDIR)
MediaFlatRoot = File.absolute_path('flat', MediaRoot)
MediaTreeRoot = File.absolute_path('tree', MediaRoot)
class ODataBotanicsApp < Safrano::ServerApp

  publish_service do

    title  'Cultivar Parentage OData API'
    name  'ParentageService'
    namespace  'ODataPar'
    server_url 'http://example.org'
    path_prefix '/'
    
    # lets test user-defined type mapping with
    # postgresql's internal  1-byte "char" type, which is what comes closest to a byte type in pg
    # but OData expects it as a 1-byte int. Thus the convertion is with the String.ord (1-char ordinal)
    with_db_type('"char"') do 
      edm_type 'Edm.Byte'
      json_value ->(val){  val&.ord }
    end
    
    #with_db_type(/numeric|decimal/) do 
      #edm_type 'Edm.Decimal'
      #json_value ->(val){  output_as_BigDecimal(val) }
      
      #with_one_param{|param| 
        #edm_type "Edm.Decimal(#{param})"
        #json_value ->(val, parm){  output_as_Prec_BigDecimal(val, parm) }
      #}
      
      #with_two_params{|par1, par2| 
        #edm_type "Edm.Decimal(#{par1},#{par2})"
        #json_value ->(val, p1, p2){  output_as_Prec_BigDecimal(val, p1, p2) }
      #}
            
    #end
    
    publish_media_model Photo do 
      slug :name
      use Safrano::Media::Static, :root => MediaFlatRoot
    end
    
    publish_model Cultivar do
      add_nav_prop_single :breeder
      add_nav_prop_single :institute
      # one_to_many  Cultivar --> Parentage
      add_nav_prop_collection :parentage
      add_nav_prop_collection :child_parentage
            
      # many_to_many over the parentage join table
      add_nav_prop_collection :parents
      add_nav_prop_collection :childs

      add_nav_prop_collection :photo
    end
    publish_model Breeder do 
      add_nav_prop_collection :cultivar 
      add_nav_prop_single :photo 
    end
    publish_model ParentageType
    publish_model Parentage, :ptree do
      # this model has client-provided keys, thus we need to unrestrict 
      unrestrict_primary_key
      add_nav_prop_single :parent
      add_nav_prop_single :child
      add_nav_prop_single :par_type
    end
    publish_model Institute
    
    # dummy models for testing
    publish_model Posts
    publish_model Multi_PK

    
    # complex types
    publish_complex_type ComplexType00
    publish_complex_type ComplexType01
    publish_complex_type ParentChild
    publish_complex_type PCNames
    publish_complex_type CT_Cultivar
    
    # TODO TODO !!! check that used ComplexTypes have been published, or auto-publish 
    # if complextype is not published but used in function then
    # *  it's  namespace will be empty in output-type
    # * it will not be outputted to $metadata
    function_import('tfunc')
      .input(:x => Integer, :d => String, :a => Integer)
      .return(ComplexType01) do | x:, d:, a: |
        c00 = ComplexType00.new(x, d)
        ComplexType01.new(a, c00)
    end
    
    function_import('vfunc')
      .input(:i => Edm::Int32, :f => Edm::Double)
      .return(Edm::Double) {  | f:, i: |  i*f }
    
    function_import('fcount')
      .input(:i => Edm::Int32)
      .return_collection(Edm::String) {  |  i: | (0..i).map{|n| n.to_s }   }
      
    function_import('ctfunc')
      .input(:i => Integer, d: Edm::String )
      .return_collection(ComplexType00) do | i:, d: |
        dd = String.new
        (0..i).map{|j| dd << d; ComplexType00.new(j, dd.dup ) }
    end  

        
    function_import('first_cultivar_func')
      .return(Cultivar) do   Cultivar.first    end  
  
    function_import('first_cultivar_func_qpar')
      .auto_query_parameters
      .return(Cultivar) do   Cultivar.first    end 
      
    function_import('top10_cultivar_func')
      .return_collection(Cultivar) do
        
        Cultivar.limit(10)
    end
    
    # same as above but with auto_query_parameters
    function_import('top10_cultivar_func_qpar')
      .auto_query_parameters
      .return_collection(Cultivar) do
        
        Cultivar.limit(10)
    end
        
    function_import('distinct_parent_child')
        .return_collection(ParentChild) do
        ret=[]
        Parentage.each{ |p| 
          ret << ParentChild.new(parent_id: p.parent_id, child_id: p.cultivar_id, 
                                 parent: p.parent, child: p.child,
                                 names: PCNames.new(parent_name: p.parent.name, 
                                                    child_name: p.child.name) )  
        }
        ret 
      end   
      
    function_import('uuid_to_s')  
      .input(:uuid => Edm::Guid)
      .return(Edm::String) do |  uuid: | 
        # uuid is here a UUIDTools::UUID
        uuid.to_s.gsub('-', '')
      end
      
    function_import('random_uuid')
      .return(Edm::Guid) do 
        # UUIDTools::UUID.random_create.to_s
        # lets just return a constant uuid; easier to test
        UUIDTools::UUID.parse('0a1e8945-24d0-4d86-8d8e-d5616f786c05').to_s
      end  
      
    # allow to define func names with symbols
    function_import(:func_name_sym)  
      .input(:inp => Edm::String)
      .return(Edm::String) do |  inp: | 
        inp
      end   
      
    # allow to define func by using lambda
    function_import(:func_with_lambda_def)  
      .input(:inp => Edm::String)
      .return(Edm::String)
      .definition ->(inp:){ inp } 
      
        
           
    function_import(:cultivar_by_id)
      .input( :id => Integer )
      .return(CT_Cultivar) do | id: | 
        CT_Cultivar.new(id: id, cultivar: Cultivar[id] ) 
      end  
      
    function_import(:error_handling)
      .input( :id => Integer )
      .return(Edm::String) do | id: | 
        case id
          when 42
            Safrano::ServerError
          when 666
            Safrano::BadRequestError.new('Bad error reason Foo')
          when 7
            raise Sequel::NotNullConstraintViolation
          when 31415
            Safrano::ServiceOperationError.new(msg: 'Baz Bar Bam')
          when 314159
            Safrano::ServiceOperationError.new(msg: 'Baz Bar Bam', 
                                               sopname: :error_handling)  
          else
            "You have request id #{id}"
        end
      end   
          
    end # of publish
    
end # of class

