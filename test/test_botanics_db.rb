ENV['RACK_ENV'] = 'test'

require 'test/unit'
require 'rack/test'
require 'json'
require 'rexml/document'
require 'pp'
require 'uri'
require 'set'

require_relative 'rack_test_monkey.rb'
require_relative '../lib/safrano.rb'
require_relative './botanics/dbmodel_test.rb' # for testdata
require_relative './odata_model_asserts.rb'
require_relative './odata_model_tsserts.rb'

module Botanics
  module Ext
    def startup
      super
      Sequel::Model.db = $DB[:botanics]
    end
  end
end

# base class for tests that require the botanics DB to be loaded,
# but are not Rack-Tests
class BotanicsDBTest <  Safrano::Test::Unit::TestCase
  extend Botanics::Ext
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotanicsDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

class BotanicsRackTest < Safrano::Test::Rack::Unit::TestCase
   extend Botanics::Ext

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotanicsRackTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    header 'MaxDataServiceVersion', '2.0'
  end
  
  def safrano_app
    ODataBotanicsApp.new
  end

  def odata_namespace
    'ODataPar'
  end
  
end

