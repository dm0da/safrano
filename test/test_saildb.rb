#!/usr/bin/env ruby
require_relative 'test_helper.rb'

require 'test/unit'
require 'rack/test'
require 'json'
require 'rexml/document'
require 'pp'
require 'uri'
require 'set'

require_relative 'rack_test_monkey.rb'
require_relative '../lib/safrano.rb'
require_relative './sail/dbmodel_test.rb' # for testdata
require_relative './odata_model_asserts.rb'
require_relative './odata_model_tsserts.rb'

module Sail
  module Ext
    def startup
      super
      Sequel::Model.db = $DB[:sail]
    end
  end
end
# base class for tests that require the sail DB to be loaded,
# but are not Rack-Tests
class SailDBTestII <  Safrano::Test::Unit::TestCase
  extend Sail::Ext
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('SailDBTestII')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

# base class for rack-tests that require the sail DB to be loaded
class SailDBTest < Safrano::Test::Rack::Unit::TestCase
  extend Sail::Ext

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('SailDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    super
    header 'MaxDataServiceVersion', '2.0'
  end
  
  #redefine this in subclasses (batch, prefix, logging)
  def safrano_app
    ODataSailApp.new
  end
    
  def odata_namespace
    'ODataSail'
  end
  
# we use a set because ordering shall not be relevant
  def assert_person_set_equal(expected_tab, result_js)
    expected_vals_tab = expected_tab.map(&:values)
    returned_vals_tab = result_js.map do |js|
      { id: js[:id], first_name: js[:first_name], last_name: js[:last_name] }
    end
    assert_equal expected_vals_tab.to_set, returned_vals_tab.to_set
  end

# use  when we shall not ignore ordering
  def assert_race_equal(expected_tab, result_js)
    expected_vals_tab = expected_tab.map(&:values)
    returned_vals_tab = result_js.map do |js|
      { id: js[:id], name: js[:name], type: js[:type],
        periodicity: js[:periodicity],  crew_type: js[:crew_type]}
    end
    assert_equal expected_vals_tab, returned_vals_tab
  end

# use set when we shall ignore ordering
  def assert_race_set_equal(expected_tab, result_js)
    expected_vals_tab = expected_tab.map(&:values)
    returned_vals_tab = result_js.map do |js|
      { id: js[:id], name: js[:name], type: js[:type],
        periodicity: js[:periodicity],  crew_type: js[:crew_type]}
    end
    assert_equal expected_vals_tab.to_set, returned_vals_tab.to_set
  end

# use set when we shall ignore ordering
  def assert_ranking_set_equal(expected_tab, result_js)
    expected_vals_tab = expected_tab.map(&:values)
    returned_vals_tab = result_js.map do |js|
      { race_id: js[:race_id], race_num: js[:race_num], boat_class: js[:boat_class],
        rank: js[:rank],  exaequo: js[:exaequo], crew: js[:crew], arrival: js[:arrival]}
    end
    assert_equal expected_vals_tab.to_set, returned_vals_tab.to_set
  end

  def assert_pertype_set_equal(expected_tab, result_js)
    expected_vals_tab = expected_tab.map(&:values)
    returned_vals_tab = result_js.map do |js|
      { id: js[:id], description: js[:description], period: js[:period]}
    end
    assert_equal expected_vals_tab.to_set, returned_vals_tab.to_set
  end

  def get_1_entity_asserts_v1_v2

    get '/Race(1)'
    assert_last_resp_entity_parseable_ok('Race')
    assert_sequel_values_equal(Race[1], @jresd)

    get '/Race(2)'
    assert_last_resp_entity_parseable_ok('Race')
    assert_sequel_values_equal(Race[2], @jresd)
  end

end



SailDBEntitySets=['RaceType', 'Crew', 'Ranking','Edition',
                  'CrewMember', 'CrewType', 'Person',
                  'BoatClass', 'periodicity_type', 'Race' ]

