#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class ServiceTC < SailDBTest

  def test_it_has_service
    get '/'
    assert last_response.ok?
# TODO: put this in a separate test unit for version handling
# here we just test the default version is 2.0
    assert_equal '2.0', last_response.headers['DataServiceVersion']
    assert_equal "application/xml;charset=utf-8",last_response.content_type
    xml = REXML::Document.new(last_response.body)
    xmlr = xml.root
    assert_equal('http://www.w3.org/2005/Atom', xmlr.attributes['xmlns:atom'])
    assert_equal('http://www.w3.org/2007/app', xmlr.attributes['xmlns:app'])
    assert_equal('http://www.w3.org/2007/app', xmlr.attributes['xmlns'])
    assert_equal('service', xmlr.name)
    xmltitle = xml.elements.to_a('service/workspace/atom:title').first
    assert_equal 'Sail OData API', xmltitle.get_text.value
    
    collectionxml = xml.elements.to_a('service/workspace/collection')
    SailDBEntitySets.each {|esname|
      assert (colx = collectionxml.find{|c| c.attributes['href'] == esname })
      assert_equal esname, colx.elements.to_a('atom:title').first.text
    }
  end
  
  # get sevice root only returns xml. So if we request json --> 415
  def test_get_service_accept_xml
    header 'Accept', 'application/json'
    get '/'
    assert_equal 415, last_response.status
  end

  # get sevice metadata only returns xml. So if we request json --> 415
  def test_get_metadata_accept_xml
    header 'Accept', 'application/json'
    get '/$metadata'
    assert_equal 415, last_response.status
  end
  
  def test_it_has_metadata
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8",last_response.content_type

    assert last_response.ok?

    xml = REXML::Document.new(last_response.body)
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 xml.root.attributes['xmlns:edmx'])
    assert_equal('Edmx', xml.root.name)
    assert_equal('edmx', xml.root.prefix)

# check the entity container a bit
    enty_cont = xml.elements.to_a('edmx:Edmx/edmx:DataServices/Schema/EntityContainer').first
    assert_equal('SailService', enty_cont.attributes['Name'])

    entity_set = Set.new
    enty_cont.get_elements('EntitySet').map do |e|
      entity_set.add [e.attributes['Name'], e.attributes['EntityType']]
    end
    assert_equal ExpectedEntitySet, entity_set

  end

  def test_options
    options '/'
    assert_equal 501, last_response.status, "Last resp status is #{last_response.status}"

    options '/$metadata'
    assert_equal 501, last_response.status, "Last resp status is #{last_response.status}"
  end
  
  # Test the default Safrano Rack cors handling (cf. Rack::Safrano::Builder )
  def test_cors_options_default
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/'
    assert_cors_ok_allow(methods: [:get, :post], origin: 'http://localhost:9595' )

    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'GET'
    options '/'
    assert_cors_ok_allow(methods: [:get], origin: 'https://the.inter.net' )
    assert_cors_ok_not_allow(methods: [:post], origin: 'https://the.inter.net' )
    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'POST'
    options '/'
    assert_cors_not_allow
    
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/$metadata'
    assert_cors_ok_allow(methods: [:get], origin: 'http://localhost:9595' )

    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'GET'
    options '/$metadata'
    assert_cors_ok_allow(methods: [:get], origin: 'https://the.inter.net' )
    
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/$batch'
    assert_cors_not_allow

    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'POST'
    options '/$batch'
    assert_cors_not_allow
 

    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'POST'
    options '/$batch'
    assert_cors_ok_allow(methods: [:post], origin: 'http://localhost:9595' )
    assert_cors_ok_not_allow(methods: [:get], origin: 'http://localhost:9595' )
    
  end
  
  def test_it_has_error_handling
    get '/blabluxdfsf'
    assert !last_response.ok?
    # documentation about last_response methods can be found here:
    # http://www.rubydoc.info/github/rack/rack/master/Rack/Response/Helpers#bad_request%3F-instance_method
    # and the http status codes: https://de.wikipedia.org/wiki/HTTP-Statuscode
    # expected OData error is ???
  end

end

class ServiceBatchCORSOverTC < SailDBTest
  def safrano_app
    super.enable_batch
  end
  def safrano_builder_mwuses_before
    sp = super
    ->(builder) do
      
      # this will allow POST /$batch
      #      everything else GET but not POST  
      builder.use ::Rack::Cors do

        allow do
          origins '*'
          # we need to use regexp ressource because a bug in rack-cors not handling properly $ char in ressource strings
          resource /\A#{Regexp.escape('/$batch')}\z/, headers: :any, methods: [:post,  :head, :options]
          resource '*', headers: :any, methods: [:get,  :head, :options]
        end         
   
      end  
      sp.call(builder)
    end
  end

 # Test overriden Safrano Rack cors handling (cf. test_saildb.rc SailDBBatchCORSOVerrideTest.app)
        # this will allow POST /$batch
        #      everything else GET but not POST   
  def test_cors_options_overriden
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/'
    assert_cors_ok_allow(methods: [:get], origin: 'http://localhost:9595' )
    assert_cors_ok_not_allow(methods: [:post], origin: 'http://localhost:9595' )
    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'GET'
    options '/'
    assert_cors_ok_allow(methods: [:get], origin: 'https://the.inter.net' )
    assert_cors_ok_not_allow(methods: [:post], origin: 'https://the.inter.net' )
    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'POST'
    options '/'
    assert_cors_not_allow
    
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/$metadata'
    assert_cors_ok_allow(methods: [:get], origin: 'http://localhost:9595' )
    assert_cors_ok_not_allow(methods: [:post], origin: 'http://localhost:9595' )
    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'GET'
    options '/$metadata'
    assert_cors_ok_allow(methods: [:get], origin: 'https://the.inter.net' )
    assert_cors_ok_not_allow(methods: [:post], origin: 'http://localhost:9595' )
    
    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'GET'
    options '/$batch'
    assert_cors_not_allow

    
    header 'origin', 'https://the.inter.net'
    header 'Access-Control-Request-Method', 'POST'
    options '/$batch'
    assert_cors_ok_allow(methods: [:post], origin: 'https://the.inter.net' )
    assert_cors_ok_not_allow(methods: [:get], origin: 'https://the.inter.net' )
 

    header 'origin', 'http://localhost:9595'
    header 'Access-Control-Request-Method', 'POST'
    options '/$batch'
    assert_cors_ok_allow(methods: [:post], origin: 'http://localhost:9595' )
    assert_cors_ok_not_allow(methods: [:get], origin: 'http://localhost:9595' )
    
  end
  


end

class ServiceVersionTC < SailDBTest
  def test_it_finds_v2
    header 'MaxDataServiceVersion', '3.0'
    header 'MinDataServiceVersion', '2.0'
    get '/'
  
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']
  end
  def test_it_finds_v2_bis
    header 'MaxDataServiceVersion', '2.0'
    header 'MinDataServiceVersion', '1.0'
    get '/'
  
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']
  end
  def test_it_finds_default_v2
    header 'MinDataServiceVersion', '1.0'
    get '/'
  
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']
  end
  
  def test_it_finds_v1
    header 'MaxDataServiceVersion', '1.0'
    get '/'
  
    assert last_response.ok?
    assert_equal '1.0', last_response.headers['DataServiceVersion']
  end
  
  def test_badrequest_min_gt_max
    header 'MinDataServiceVersion', '2.0'
    header 'MaxDataServiceVersion', '1.0'
    get '/'
    
    assert_bad_request 'MinDataServiceVersion', 'MaxDataServiceVersion', 'larger'
  end
  
  def test_err_too_new
    header 'MinDataServiceVersion', '3.0'
    header 'MaxDataServiceVersion', '4.0'
    get '/'
    
    assert_not_implemented 
  end
  
  def test_bad_request_maxv
    header 'MaxDataServiceVersion', '0.5'
    get '/'
    
    assert_bad_request  'MaxDataServiceVersion', 'not', 'parsed'
  end
  
  
  def test_bad_request_maxv_b
    header 'MaxDataServiceVersion', '2.x'
    get '/'
    
    assert_bad_request  'MaxDataServiceVersion', 'not', 'parsed'
  end
  
  def test_bad_request_minv
    header 'MinDataServiceVersion', '5.5'
    get '/'
    
    assert_bad_request  'MinDataServiceVersion', 'not', 'parsed'
  end
end