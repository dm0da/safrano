#!/usr/bin/env ruby

# uncomment this to use a local safrano lib
salib = '~/dev/Safrano/safrano/lib'
$LOAD_PATH.unshift(salib) unless $LOAD_PATH.include?(salib)
require 'safrano'
require_relative '../lib/odata/request/json'

require 'rest_client'
require 'pp'
require 'json'
require 'pry-byebug'
# if we run on gitlab-CI, pg host is postgres, otherwise assume it runs locally
PG_HOST = ENV['CI'] ? 'postgres'.freeze : 'localhost'.freeze

# this would be for local unix socket conn. But that does not seem to make any 
# difference in execution time
#PG_HOST = ENV['CI'] ? 'postgres'.freeze : '/var/run/postgresql'.freeze

PG_DB = 'safrano'.freeze
PG_USER = 'safrano'.freeze
PG_PASSWORD = 'password'.freeze
PG_OPTIONS = '--client-min-messages=warning'.freeze                                             
PG_SETENV = %Q(env PGOPTIONS="#{PG_OPTIONS}" PGHOST="#{PG_HOST}" PGUSER="#{PG_USER}" PGPASSWORD="#{PG_PASSWORD}" )


$base = 'http://localhost:9494'
Sequel::Model.db = Sequel.postgres(PG_DB,
                                    host: PG_HOST,
                                    user: PG_USER,
                                    password: PG_PASSWORD)
                                    
                                    
require_relative './sail/model.rb'
                                    
class Test
  def call
  end

  def verify
  end

  def output
    @passed ? print('.') : print('F')
  end
end



class TestGet < Test

  def header 
    {}
  end
  
  def call
    begin
      @response = RestClient.get query, header 
    rescue => error
      @error = error
      @response = error.response 
    end
  end


end

class TestGetWithModel < TestGet
  VARIANTS = [Person, Race, BoatClass, Crew, RaceType]
  def self.random 
    self.new(VARIANTS.at(rand(VARIANTS.size)))
  end
  
  def initialize(modelk)
    @modelk = modelk
    set_random_key
    set_random_params
  end
  
  def set_random_key
    @idx = rand(@modelk.all.size+1)
  end
  
  def set_random_params
  end  
  
  def random 
  end
  
  def header
    { accept: :json }
  end
  
  def base
    "#{$base}/#{@modelk}"
  end

  def equals_entity(jresd, idx)
    expected = @modelk[idx]
    ! @modelk.columns.find{|c|   expected[c] != jresd[c] }
  end
  
end

class TestGetService < TestGet

  def query
    "#{base}"
  end
  def verify
    @passed = @response.code == 200
  end
end

class TestGetServiceRoot < TestGetService

  def base
    "#{$base}/"
  end

end

class TestGetServiceMetadata < TestGetService

  def base
    "#{$base}/$metadata"
  end

end

class TestGetService
  VARIANTS = [TestGetServiceRoot, TestGetServiceMetadata]
  def self.random
    VARIANTS.at(rand(VARIANTS.size)).new
  end

  def random
    nil
  end  
end
class TestGetEntity < TestGetWithModel
  def query
    "#{base}(#{@idx})"
  end
  def parse
    begin
      @jresd = Safrano::XJSON.parse_one(@response.body)
    rescue 
      @jresd_raw = JSON.parse(@response.body)
    end
  end
  def verify
    parse 
    @passed  = if @idx == 0   # db index start at 1 --> 404
      @error.http_code == 404
    else
      equals_entity(@jresd, @idx)
    end
  end    
end

class TestGetCollection < TestGetWithModel

  
  def parse 
    begin
      jresd = Safrano::XJSON.parse_coll(@response.body)
      @jresd_coll = jresd['results']
    rescue => error
      binding.pry
    end  
  end
  
end
class TestGetCollById < TestGetCollection
  def query
    "#{base}?$filter=id eq #{@idx}"
  end
   
  def verify
    parse 
    begin
      @passed  = if @idx == 0   # filter by ID 0 --> empty Collect.
        @jresd_coll.empty?
      else
        equals_entity(@jresd_coll[0], @idx)
      end
    rescue => error
      binding.pry
    end
  end  
  
end

class TestGetCollTopN < TestGetCollection
  def query
    "#{base}?$filter=id ge #{@idx}&$top=2"
  end
   
  def verify
    parse 
    begin
      not_passed = @jresd_coll.find{|e|  (e[:id] > @idx + 2) || (e[:id] < @idx ) }
      @passed = !not_passed
    rescue => error
      binding.pry
    end
  end  
  
end


class Test
  VARIANTS = [TestGetEntity, TestGetCollById, TestGetCollTopN,  TestGetService ]
  def self.random
    idx = rand(VARIANTS.size)
    VARIANTS.at(idx)
  end
end
count = ARGV[0].to_i
puts "Start, count= #{count}"

$h = Hash.new
i = 0
puts( "Building Test-objects")
count.times do

  to = Test
  path = []
  while tnext = to.random
    path << tnext 
    to = tnext
  end
  pp path
  $h[i] = to
  i += 1
end

  puts "...done"
  print( "Calling all tests-objects in threads...")
  $h.map{|i,to|
    Thread.new do
      to.call
    end
    }
    .each{|thr| thr.join }
  puts "...done"
  puts "...Now verifying"
  
  $h.each { |i, t| t.verify; t.output }
puts
puts 'Verifed done.'
puts 'Bye bye'
