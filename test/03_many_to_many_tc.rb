#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

class ManyToManyTest < SailDBTest

  def test_basic_crews_of_person
    get 'Person(34)/crews'
    
    # Person(34) is in two Crews
    expected = Person[34].crews
    assert_last_resp_coll_parseable('Crew', 2)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|cr|  "Crew(#{cr.id})"}

  end
  
  def test_basic_members_of_crew
    # Crew(38) has two members
    get 'Crew(38)/members'
    expected = Crew[38].members
    assert_last_resp_coll_parseable('Person', 2)
    assert_sequel_coll_values_equal(expected, @jresd_coll)

    assert_metadata_uri(expected.first, @jresd_coll.first){|pr|  "Person(#{pr.id})"}
    
  end
  
  def test_expand_crews_of_person
    get 'Person(34)?$expand=crews'
    
    # Person(34) is in two Crews
    assert_last_resp_entity_parseable_ok('Person')
    assert_expanded_entity_mult(Person[34], @jresd, :crews){|cr|
      "Crew(#{cr.id})" }
  end

  def test_bykey_crew_of_person
    # from crews of PErson(34) the one with id 37
    get 'Person(34)/crews(37)'
    
    assert_last_resp_entity_parseable_ok('Crew')
    assert_sequel_values_equal(Crew[37], @jresd)
    
    # from  crews of PErson(34) the with id 6  --> not any
    get 'Person(34)/crews(6)'
    
    # Note : Crews(6) is valid but Person(34)/crews(6) is not
    #   because Person(34) has no crew with id 6
    assert last_response.not_found?
       
  end

  def test_filter_crews_of_person
    # from crews of PErson(34) the ones with id 37
    get 'Person(34)/crews?$filter=id eq 37'
    assert_last_resp_coll_parseable('Crew', 1)
    expected = [ Crew[37] ]
    
    assert_sequel_coll_values_equal(expected, @jresd_coll)

       
  end

  def test_ordered_crews_of_person
    # ordered crews of PErson(34)
    get 'Person(34)/crews?$orderby=id desc'
    
    expected = Person[34].crews
    assert_last_resp_coll_parseable_ok('Crew', expected.count)
    assert_sequel_coll_set_equal(expected, @jresd_coll)


    assert_sequel_coll_ord_desc_by(@jresd_coll){|jr| jr[:id ] }

    get 'Person(34)/crews?$orderby=id'
    
    assert_last_resp_coll_parseable_ok('Crew', expected.count)
    assert_sequel_coll_set_equal(expected, @jresd_coll)

    assert_sequel_coll_ord_asc_by(@jresd_coll){|jr| jr[:id ] }


       
  end

  def test_expand_persons_of_crew
    get 'Crew(38)?$expand=members'
    
    assert_last_resp_entity_parseable_ok('Crew')
    assert_expanded_entity_mult(Crew[38], @jresd, :members){|pr|
      "Person(#{pr.id})" }
  end  
  
  # test empty crews or crew-less persons
  def test_basic_empty
    get 'Person(44)/crews'
    assert_last_resp_empty_coll_parseable_ok
    
    get 'Crew(48)/members'
    assert_last_resp_empty_coll_parseable_ok    
  end
  
end


