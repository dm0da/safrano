
$LOAD_PATH << '../../lib/'
require_relative '../../lib/safrano.rb'

  
#require_relative  '../run_helper'

  DB = {}

#dbrname = File.join(__dir__, 'sail_test.db3')
#DB[:sail] = Sequel::Model.db = Sequel.sqlite(dbrname)

require_relative '../test_helper.rb'
create_db(:postgres)
Safrano::Test::DB.postgres(:sail)

PRF = nil
#PRF = RubyProf::Profile.new
#PRF.start
#PRF.pause

#DevLog = Logger.new(STDOUT)
#DB[:botanics] = Sequel::Model.db = Sequel.connect("sqlite://#{dbrname}", logger: DevLog)

at_exit {
 Sequel::Model.db.disconnect if Sequel::Model.db 
 if PRF
   result = PRF.stop
  # print a flat profile to text
printer = RubyProf::CallStackPrinter.new(result)
File.open('profile.html', 'w') do |f|
  printer.print(f)
end 

printer2 = RubyProf::FlatPrinter.new(result)
File.open('profile.flat', 'w') do |f2|
  printer2.print(f2)
end 
end
 }

require_relative './model.rb'
