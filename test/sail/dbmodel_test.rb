
require_relative './db.rb'
require_relative './model.rb'


# This is just for testing;used in 00_service_tc

ExpectedEntitySet = %w(RaceType Race Person Edition Ranking
                         Crew CrewMember CrewType BoatClass).map do |t|
                        [t, "ODataSail.#{t}"]
                      end.to_set
ExpectedEntitySet.add(['periodicity_type','ODataSail.PeriodicityType'])
TCSailPrefix = '/Sail.svc'
