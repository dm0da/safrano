###########
# config.ru
#

# $LOAD_PATH << '../../lib/'

require_relative './dbmodel_run.rb'

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::Safrano::Builder.new do
  use Rack::ODataCommonLogger 
  run ODataSailApp.new
end

#Rack::Handler::Thin.run final_odata_app, Port: 9595
#Rack::Handler::WEBrick.run final_odata_app, Port: 9595
run final_odata_app
