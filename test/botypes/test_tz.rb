#!/usr/bin/env ruby
require 'sqlite3'

begin
    db = SQLite3::Database.open './botypes_v0.db3'
    # Attempt some action that may have an exception in SQLite    
    db.execute( "select * from dbtypedatetime" ) do |row|
      p row
    end
rescue SQLite3::Exception => e 
    # Handle the exception gracefully
ensure
    # If the whole application is going to exit and you don't
    # need the database at all any more, ensure db is closed.
    # Otherwise database closing might be handled elsewhere.
    db.close if db
end


