BEGIN TRANSACTION;
 PRAGMA foreign_keys=OFF;
CREATE TABLE IF NOT EXISTS `dbtypenum` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`nume`	NUMERIC,
        `deci`	DECIMAL,
	`nume2`	NUMERIC(2),
        `deci2`	DECIMAL(2),
	`nume5_2`	NUMERIC(5,2),
        `deci5_2`	DECIMAL(5,2),
        `nume16_2`	NUMERIC(16,2),
        `tfield`        TEXT,
        `expe_str`      TEXT
);
INSERT INTO `dbtypenum` ( `id`, `nume`, `tfield`, `expe_str`) 
VALUES (1, 3.1415, 'nume',  '3.1415');
INSERT INTO `dbtypenum` ( `id`, `deci`, `tfield`, `expe_str` ) 
VALUES (2, 3.1415, 'deci', '3.1415' );
INSERT INTO `dbtypenum` ( `id`, `nume2`, `tfield`, `expe_str` ) 
VALUES (3, 3.1, 'nume2', '3.1');
INSERT INTO `dbtypenum` ( `id`, `deci2`, `tfield`, `expe_str` ) 
VALUES (4, 3.1, 'deci2', '3.1');
INSERT INTO `dbtypenum` ( `id`, `nume5_2`, `tfield`, `expe_str` ) 
VALUES (5, 312.11, 'nume5_2', '312.11');
INSERT INTO `dbtypenum` ( `id`, `nume5_2`, `tfield`, `expe_str` ) 
VALUES (6, 312.10, 'nume5_2', '312.10');
INSERT INTO `dbtypenum` ( `id`, `nume5_2`, `tfield`, `expe_str` ) 
VALUES (7, 12.11, 'nume5_2', '12.11');
INSERT INTO `dbtypenum` ( `id`, `nume5_2`, `tfield`, `expe_str` ) 
VALUES (8, 12.10, 'nume5_2', '12.10');
INSERT INTO `dbtypenum` ( `id`, `nume5_2`, `tfield`, `expe_str` ) 
VALUES (9, 12.1, 'nume5_2', '12.10');
INSERT INTO `dbtypenum` ( `id`, `deci5_2`, `tfield`, `expe_str` ) 
VALUES (10, 312.11, 'deci5_2', '312.11');
INSERT INTO `dbtypenum` ( `id`, `deci5_2`, `tfield`, `expe_str` ) 
VALUES (11, 312.10, 'deci5_2', '312.10');
INSERT INTO `dbtypenum` ( `id`, `deci5_2`, `tfield`, `expe_str` ) 
VALUES (12, 12.11, 'deci5_2', '12.11');
INSERT INTO `dbtypenum` ( `id`, `deci5_2`, `tfield`, `expe_str` ) 
VALUES (13, 12.10, 'deci5_2', '12.10');
INSERT INTO `dbtypenum` ( `id`, `deci5_2`, `tfield`, `expe_str` ) 
VALUES (14, 12.1, 'deci5_2', '12.10');
INSERT INTO `dbtypenum` ( `id`, `nume16_2`, `tfield`, `expe_str` ) 
VALUES (15, 12022.1, 'nume16_2', '12022.10');
INSERT INTO `dbtypenum` ( `id`, `nume16_2`, `tfield`, `expe_str` ) 
VALUES (16, 12022555555555.5, 'nume16_2', '12022555555555.50');
INSERT INTO `dbtypenum` ( `id`, `nume16_2`, `tfield`, `expe_str` ) 
VALUES (17, 1202255555555.55, 'nume16_2', '1202255555555.55');

-- float and
-- int variants
-- /\A\s*(smallint|integer|int2|int4|int8|int|mediumint|bigint|serial|bigserial)\s*/i.freeze
CREATE TABLE IF NOT EXISTS `dbtypefloat` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        `realn` REAL,
        `doublen` DOUBLE,
        `floatn` FLOAT,
        `float2` FLOAT(2),
        `float30` FLOAT(30), 
        `smallint` SMALLINT,
        `int2` INT2,
        `integer` INTEGER,
        `int4` INT4,
        `int` INT,
        `mediumint` MEDIUMINT,
        `int8` INT8,
        `bigint` BIGINT  ,      
        `ubigint` unsigned big int,
        `boolean` boolean,
        `binary` binary,
        `byte` tinyint,
        `blob` blob
        
);
INSERT INTO `dbtypefloat` ( `id`, `realn` ) VALUES (1, 1.12346);
INSERT INTO `dbtypefloat` ( `id`, `doublen` ) VALUES (2, 1.12345678912346);
INSERT INTO `dbtypefloat` ( `id`, `floatn` ) VALUES (3, 1.12346);
INSERT INTO `dbtypefloat` ( `id`, `float2` ) VALUES (4, 1.12346);
INSERT INTO `dbtypefloat` ( `id`, `float30` ) VALUES (5, 1.12345678912346);
INSERT INTO `dbtypefloat` ( `id`, `smallint` ) VALUES (6, 32000);
INSERT INTO `dbtypefloat` ( `id`, `int2` ) VALUES (7, 32000);
INSERT INTO `dbtypefloat` ( `id`, `integer` ) VALUES (8, 2147483647);
INSERT INTO `dbtypefloat` ( `id`, `int4` ) VALUES (9, 2147483647);
INSERT INTO `dbtypefloat` ( `id`, `int` ) VALUES (10, 2147483647);
INSERT INTO `dbtypefloat` ( `id`, `int8` ) VALUES (11, 9223372036854775806);
INSERT INTO `dbtypefloat` ( `id`, `bigint` ) VALUES (12, 9223372036854775806);
INSERT INTO `dbtypefloat` ( `id`, `mediumint` ) VALUES (13, 4294967290);
-- doesnt work yet
--INSERT INTO `dbtypefloat` ( `id`, `ubigint` ) VALUES (14, 18446744073709551614	);
INSERT INTO `dbtypefloat` ( `id`, `mediumint` ) VALUES (14, 4294967290);
-- -- --
INSERT INTO `dbtypefloat` ( `id`, `byte` ) VALUES (15, 120);
INSERT INTO `dbtypefloat` ( `id`, `boolean` ) VALUES (16, 0);
INSERT INTO `dbtypefloat` ( `id`, `boolean` ) VALUES (17, 1);
INSERT INTO `dbtypefloat` ( `id`, `blob` ) VALUES (18, 'oz');

DROP TABLE IF EXISTS "dbtypedatetime";
CREATE TABLE IF NOT EXISTS "dbtypedatetime" (
	"id"	INTEGER NOT NULL,
	"date"	DATE,
	"datetime"	DATETIME,
        "datetimetz"	DATETIME,
	"iso8601_utc"	TEXT,
	"iso8601"	TEXT,
	"edm_json_utc"	TEXT,
	"edm_json"	TEXT,
	"comment"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);

INSERT INTO "dbtypedatetime" ("id","date","datetime","datetimetz","iso8601_utc","iso8601","edm_json_utc","edm_json","comment") 
VALUES 
 (1,'2022-07-15 00:00:00',NULL,NULL,'2022-07-15T00:00:00','2022-07-15T00:00:00Z','/Date(1657843200000)/','/Date(1657843200000)/','Time.utc(2022,07,15,0,0,0)'),
 (2,NULL,'2022-07-15 08:37:05','2022-07-15 08:37:05+00:00','2022-07-15T08:37:05','2022-07-15T08:37:05Z','/Date(1657874225000)/','/Date(1657874225000)/','Time.utc(2022,07,15,8,37,5)'),
 (3,NULL,NULL,'2022-07-15 10:37:05+02:00','2022-07-15T08:37:05','2022-07-15T10:37:05+02:00','/Date(1657874225000)/','/Date(1657874225000+120)/','Time.new(2022,07,15,10,37,5) wit local timezone +0200'),
 (4,NULL,NULL,'2022-07-15 08:37:05+02:00','2022-07-15T06:37:05','2022-07-15T08:37:05+02:00','/Date(1657867025000)/','/Date(1657867025000+120)/','Time.new(2022,07,15,8,37,5) wit local timezone +0200'),
 (5,NULL,NULL,'1970-01-01 00:00:00-04:00','1970-01-01T04:00:00','1970-01-01T00:00:00-04:00','/Date(14400000)/','/Date(14400000-240)/','Time.new(1970,01,01,0,0,0) wit local timezone -0400'),
 (6,NULL,NULL,'1970-01-01 04:00:00+04:00','1970-01-01T00:00:00','1970-01-01T04:00:00+04:00','/Date(0)/','/Date(0+240)/','Time.new(1970,01,01,4,0,0) in local timezone +0400'),
 (7,NULL,NULL,'1970-01-01 02:00:00+02:00','1970-01-01T00:00:00','1970-01-01T02:00:00+02:00','/Date(0)/','/Date(0+120)/','Time.new(1970,01,01,2,0,0) in local timezone +0200'),
 (8,NULL,NULL, '2022-07-15 12:37:05+04:00','2022-07-15T08:37:05','2022-07-15T12:37:05+04:00','/Date(1657874225000)/','/Date(1657874225000+240)/','Time.new(2022,07,15,12,37,5) wit local timezone +0400');
 
 DROP TABLE IF EXISTS "dbtypeguid";
 CREATE TABLE "dbtypeguid" (
	"guid"	BLOB NOT NULL,
        "guid_str" TEXT,
	"comment"	TEXT,
	PRIMARY KEY("guid")
);

INSERT INTO "dbtypeguid" ("guid", "guid_str", "comment") VALUES
(x'43ae7f2af38c4d1688bc4b2149725357', '43ae7f2a-f38c-4d16-88bc-4b2149725357', 'guid 43ae7f2a-f38c-4d16-88bc-4b2149725357'),
(x'ba5cab01d963456f842c28fbb6778c74', 'ba5cab01-d963-456f-842c-28fbb6778c74', 'guid ba5cab01-d963-456f-842c-28fbb6778c74');
COMMIT;
