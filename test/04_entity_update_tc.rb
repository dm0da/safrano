#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class Entity_Update_TC < SailDBTest

  def  payload_cleanup_nava

    #remove nav attribs
    @data.delete('rtype')
    @data.delete('CrewType')
    @data.delete('PeriodicityType')
    @data.delete('Edition')
    @data
  end

  def  get_a_valid_payload
    get '/Race(12)'
    assert_last_resp_entity_parseable_ok('Race')

# copy with changed data
    @data = JSON.parse(last_response.body)['d']
    payload_cleanup_nava
  end

# test read + update with a single PK id
# TODO: PUT normall should do a full replace,
#       while PATCH/MERGE only a merge-update
  def test_simple_read_update

  # TODO better way to provide a test payload
    get_a_valid_payload

    @data['name'] = 'Test-Race record Updated ! '
    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    pbody = @data.to_json

    put '/Race(12)', pbody
    assert last_response.successful?
    assert last_response.no_content?
    Race[12].refresh
    assert_equal 'Test-Race record Updated ! ', Race[12].name
  end

# test read + merge-update with a single PK id
  def test_simple_read_merge

    # for MERGE we want to test with only changed data passed in the payload
    changed_data = { '__metadata' => 'someting  meta',
                     'name' => 'Test-Race record Updated AGAIN! ' }

    pbody = changed_data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    xmerge '/Race(12)', pbody

    assert last_response.successful?
    assert last_response.no_content?

#   we need to reload !!!
    Race[12].refresh
    assert_equal 'Test-Race record Updated AGAIN! ', Race[12].name
  end

# test read + merge-update with a single PK id, BAD request
# (syntax error in JSON payload)
  def test_simple_merge_bad_request_syntax
    pbody = +'{this aint be valid JSON '

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    xmerge '/Race(12)', pbody

    # check for 400
    assert last_response.bad_request?

  end

# test read + merge-update with a single PK id
  def test_simple_read_merge_unprocessable

    # for MERGE we want to test with only changed data passed in the payload
    changed_data = { '__metadata' => 'something meta',
                     'name_XXX' => 'Some other thing'}
    pbody = changed_data.to_json
    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    xmerge '/Race(12)', pbody

    # check for 422
    assert last_response.unprocessable?

  end

end
