ENV['RACK_ENV'] = 'test'

require 'test/unit'
require 'rack/test'
require 'json'
require 'rexml/document'
require 'pp'
require 'uri'
require 'set'

require_relative 'rack_test_monkey.rb'
require_relative '../lib/safrano.rb'
require_relative './botypes/dbmodel_test.rb' # for testdata
require_relative './odata_model_asserts.rb'
require_relative './odata_model_tsserts.rb'

module Botypes
  module Ext
    def startup
      super
      Sequel::Model.db = $DB[:botypes]
    end
  end
end
# base class for tests that require the botypes DB to be loaded,
# but are not Rack-Tests
class BotypesDBTest <  Safrano::Test::Unit::TestCase
  extend Botypes::Ext

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotypesDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

# for testing Safrano's Standard types settings
class BotypesRackTest < Safrano::Test::Rack::Unit::TestCase
  extend Botypes::Ext
  
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotypesRackTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    header 'MaxDataServiceVersion', '2.0'
  end
  
  def safrano_app
    ODataBotypesApp.new
  end
  
  def odata_namespace
    'ODataTy'
  end
  
end

# for testing Safrano's type settings overriding functionality 
class BotypesRackTestII < Safrano::Test::Rack::Unit::TestCase
  extend Botypes::Ext
  
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotypesRackTestII')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    header 'MaxDataServiceVersion', '2.0'
  end

  def app
    ODataBotypesAppII.new
  end

  def odata_namespace
    'ODataTyII'
  end
  
end
