
require_relative './test_botanics_db.rb' # Botanics-DB testdata

class ServiceSequelExceptions_TC < BotanicsRackTest

  def test_exception_as_odata_server_error
  
    # delete the Posts table, and query it afterwards,
    # this should trigger a Sequel Exception

    Sequel::Model.db.drop_table(:posts)
    get '/Posts'
   
    assert_server_error  'no', 'table', 'posts'
    
  end
end

