#!/usr/bin/env ruby

# uncomment this to use a local safrano lib
salib = '~/dev/Safrano/safrano/lib'
$LOAD_PATH.unshift(salib) unless $LOAD_PATH.include?(salib)
require 'safrano'
require_relative '../lib/odata/request/json'

require 'rest_client'
require 'pp'
require 'json'
require 'pry-byebug'
# if we run on gitlab-CI, pg host is postgres, otherwise assume it runs locally
PG_HOST = ENV['CI'] ? 'postgres'.freeze : 'localhost'.freeze

# this would be for local unix socket conn. But that does not seem to make any 
# difference in execution time
#PG_HOST = ENV['CI'] ? 'postgres'.freeze : '/var/run/postgresql'.freeze

PG_DB = 'safrano'.freeze
PG_USER = 'safrano'.freeze
PG_PASSWORD = 'password'.freeze
PG_OPTIONS = '--client-min-messages=warning'.freeze                                             
PG_SETENV = %Q(env PGOPTIONS="#{PG_OPTIONS}" PGHOST="#{PG_HOST}" PGUSER="#{PG_USER}" PGPASSWORD="#{PG_PASSWORD}" )


$base = 'http://localhost:9494'
Sequel::Model.db = Sequel.postgres(PG_DB,
                                    host: PG_HOST,
                                    user: PG_USER,
                                    password: PG_PASSWORD)
                                    
                                    
require_relative './sail/model.rb'
                                    
class Test
  def call
  end

  def verify
  end

  def output
    @passed ? print('.') : print('F')
  end
end

# Fixed size Enumerable...
# uses self.at and self.size
class RandomStepEnumerator < Enumerator
  def initialize(idx, len, dimsize: nil )
    @idx = idx
    @dimsize = dimsize
    @count = 0
    @len = len
  end
  def next
    raise StopIteration if @count == @len
    # mx = rand(3) - 1 
    mx = 2*rand(2) - 1 
    @count += 1
    @idx += mx
    @idx = @dimsize ?  @idx  %  @dimsize  : @idx
  end
end




  # perform a random walk 
  # each step left, right or stay in place (index -1, 0, +1 )
  def random_walk(startidx = 0, dimsize: nil, len: 1 )
    iter =  RandomStepEnumerator.new(startidx, len, dimsize: dimsize)
    if block_given?
      loop do
        yield iter.next
      end
    else
      iter
    end
  end


def multidim_walk(*dims, len: 1)
  iterlist = dims.map{|d| random_walk( dimsize: d, len: len ) }
  loop do
    yield iterlist.map{|iter| iter.next}
  end
end

class TestGet < Test
  def self.factory(idx, ei, ivariant)
    vklass = $V[ivariant]
    case vklass.to_s.to_sym
    when :TestGetService, :TestGetServiceMetadata
      vklass.new
    else
      entity = $E[ei]
      vklass.new(idx, entity)
    end
  end  
  def header 
    {}
  end
  def call
    begin
      @response = RestClient.get query, header 
    rescue => error
      binding.pry
    end
  end


end

class TestGetWithModel < TestGet


  def initialize(idx, modelk)
    @idx = idx % modelk.all.size + 1    # Sequel / Db index from 1 up
    @modelk = modelk
  end
  
  def header
    { accept: :json }
  end
  
  def base
    "#{$base}/#{@modelk}"
  end

  def equals_entity(jresd, idx)
    expected = @modelk[idx]
    ! @modelk.columns.find{|c|   expected[c] != jresd[c] }
  end
end

class TestGetService < TestGet
  def base
    "#{$base}/"
  end
  def query
    "#{base}"
  end
  def verify
    @passed = @response.code == 200
  end
end

class TestGetServiceMetadata < TestGet
  def base
    "#{$base}/$metadata"
  end
  def query
    "#{base}"
  end
  def verify
    @passed = @response.code == 200
  end
end

class TestGetEntity < TestGetWithModel
  def query
    "#{base}(#{@idx})"
  end
  def parse
    @jresd = Safrano::XJSON.parse_one(@response.body)
  end
  def verify
    parse 
        
    @passed  =  equals_entity(@jresd, @idx)

  end    
end

class TestGetCollection < TestGetWithModel

  
  def parse 
    begin
      jresd = Safrano::XJSON.parse_coll(@response.body)
      @jresd_coll = jresd['results']
    rescue => error
      binding.pry
    end  
  end
  
end
class TestGetCollById < TestGetCollection
  def query
    "#{base}?$filter=id eq #{@idx}"
  end
   
  def verify
    parse 
    begin
      @passed  =  equals_entity(@jresd_coll[0], @idx)
    rescue => error
      binding.pry
    end
  end  
  
end

class TestGetCollTopN < TestGetCollection
  def query
    "#{base}?$filter=id ge #{@idx}&$top=2"
  end
   
  def verify
    parse 
    begin
      not_passed = @jresd_coll.find{|e|  (e[:id] > @idx + 2) || (e[:id] < @idx ) }
      @passed = !not_passed
    rescue => error
      binding.pry
    end
  end  
  
end



$E=[Person, Race, BoatClass, Crew, RaceType]
$V=[TestGetEntity, TestGetCollById, TestGetCollTopN, TestGetServiceMetadata, TestGetService ]
# $V=[ TestGetServiceMetadata, TestGetService ]

$h = Hash.new
def hammer(count)
  i = 0
  puts( "Building Test-objects")
  multidim_walk(nil, $E.size, $V.size, len: count){|a| 
    
    idx = a[0]
    ei = a[1]
    ivariant = a[2]
    to = $h[i] = TestGet.factory(idx, ei, ivariant)
    puts to.query
    i += 1 
  }
  puts "...done"
  print( "Calling all tests-objects in threads...")
  $h.map{|i,to|
    Thread.new do
      to.call
    end
    }
    .each{|thr| thr.join }
  puts "...done"
end

# resj = JSON.parse(response.body)
# pp :body, resj

count = ARGV[0].to_i
puts "Start, count= #{count}"
hammer(count)

$h.each { |i, t| t.verify; t.output }
puts
puts 'Verifed done.'
puts 'Bye bye'
