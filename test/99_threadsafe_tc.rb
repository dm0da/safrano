
ENV['RACK_ENV'] = 'none'

require_relative './test_saildb.rb' # Sail-DB testdata

# add some trigger points for sleeping... after all it's REST
module Safrano
  module Filter 
    class IdentityFuncTree 
    
      def arity_full?(cursize)
        # slow down $filter=(xyz) processing
        while $FILTER_SLEEP_LOCK
          sleep 0.01
          puts "filter sleeping"
        end
        
        # this is the original "productive" code
        arity_full_monkey?(cursize)
      end
    end
  end
  
end

# monkey patch sleep for the two ThreadSafeEntityTest below
  class ODataSailApp
    def before
      begin
        if  @request.params['$expand']
          while $EXPAND_SLEEP_LOCK
            sleep 0.1
            puts "before expand sleeping"
          end
        end
      # accessing params like above breaks two testcases 
      # - $batch and OPTION related - (because params does merge)
      # so we need to rescue
      rescue
      end
      super
    end
  end
  
# check entity odata_get is thread safe. 
# This fails if ServerApp.call
# is not thread safe (cf. added _call.dup) 
class ThreadSafeEntityTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    $EXPAND_SLEEP_LOCK = true
  end
  def teardown
    $EXPAND_SLEEP_LOCK = false
  end

  def test_two_entity_requests_parallel
    # thread 1 will process slow because it uses an expand
    # but thread 2 without $expand will be fast, and potentially changing 
    # thread1's data 
    
    # the test request in t1/t2 are carefully choosen to maximise risk
    # of overwriting potentially shared data 
    t1 = Thread.new do
      puts "T1e Started app is #{p @app}"
      last_r = get '/Race(1)?$expand=rtype'
      jrd = tssert_last_resp_entity_parseable_ok(last_r, 'Race')
      assert_expanded_entity(Race[1], jrd, :rtype){|expanded|
        "RaceType(#{expanded.id})"}
      puts 'T1e end'
    end
    t2 = Thread.new do
      # ensure that t1 's  request is started first
      sleep 0.01
      puts "T2e Started app is #{p @app}"
      last_r = get '/Race(1)?$select=foo'
      tssert_bad_request(last_r,  'entityset', 'foo')
      puts 'T2e end'
      # wakeup t1e...
      $EXPAND_SLEEP_LOCK = false
    end
    t1.join 
    t2.join 
  end

  def test_two_entity_requests_parallel_2_1
    # thread 1 will process slow because it uses an expand
    # but thread 2 without $expand will be fast, and potentially changing 
    # thread1's data 
        
    # the test request in t1/t2 are carefully choosen to maximise risk
    # of overwriting potentially shared data 
    ee1 = Race[1]
    t1 = Thread.new do
      puts "T1e Started app is #{p @app}"
      last_r = get '/Race(1)?$expand=rtype'
      jrd = tssert_last_resp_entity_parseable_ok(last_r, 'Race')
      assert_expanded_entity(ee1, jrd, :rtype){|expanded|
        "RaceType(#{expanded.id})"}
      puts 'T1e end '
    end
    t2 = Thread.new do
      # ensure that t1 's  request is started first
      sleep 0.01      
      puts "T2e Started app is #{p @app}"
      last_r =  get '/Race(1)?$select=foo'
      tssert_bad_request last_r,  'entityset', 'foo'
      puts 'T2e end'
      # wakeup t1e...
      $EXPAND_SLEEP_LOCK = false
    end
    t1.join 
    t2.join 
  end
  
end

# check collections odata_get is thread safe 
class ThreadSafeCollectionTest < SailDBTest
  def setup
    @jresd_coll = nil
    @jresd = nil
    $FILTER_SLEEP_LOCK = true
  end
  def teardown
    $FILTER_SLEEP_LOCK = false
  end

  def test_two_coll_requests_parallel
    # thread 1 will process slow because it uses parentheses
    # but thread 2 will be fast, and potentially changing 
    # thread1's data 
    # note: this test probably failed up to Safrano version 0.4.3 ... when we
    #       started looking into this topic
    # --> fixed in 0.4.4 by better thread safe collection design
    expected_tab1 = Person.where(id: 1).to_a
    expected_tab2 = Person.where(id: 2).to_a
    t1 = Thread.new do
      puts 'T1 Started' 
      last_r = get '/Person?$filter=(id eq 1) and (id eq 1) and (id eq 1) and (id eq 1)'
      jrdc = tssert_last_resp_coll_parseable_ok(last_r, 'Person', expected_tab1.count)
      assert_person_set_equal(expected_tab1, jrdc)
      puts 'T1 end'
    end
    t2 = Thread.new do
      # ensure that t1 's  request is started first
      sleep 0.01    
      puts 'T2 Started'
      last_r = get '/Person?$filter=id eq 2'
      jrdc = tssert_last_resp_coll_parseable_ok(last_r, 'Person', expected_tab2.count)
      assert_person_set_equal(expected_tab2, jrdc )
      puts 'T2 end'
      # wakeup t1...
      $FILTER_SLEEP_LOCK = false
    end
    t1.join 
    t2.join 
  end
end

