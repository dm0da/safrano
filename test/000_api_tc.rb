require 'test/unit'
require 'pp'
require 'set'
require_relative '../lib/safrano.rb'

require_relative 'test_botanics_db.rb'
require_relative '000_api_botanics_model.rb'

class ODataBotanicsAPI_TC1_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCAssoc_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCDuplAttr1_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCDuplAttr2_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TCOnlyFuncNoModel_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TCFuncDefProcMissing_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCFuncDefProcNotMissing_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TCFuncProcRedefined_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCFuncProcRedefined2_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TCNoPrefix_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCPrefix_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCBatch_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TCMedia_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TC_NS0_App < Safrano::ServerApp
end

class ODataBotanicsAPI_TC_NS1_App < Safrano::ServerApp
end
class ODataBotanicsAPI_TC_NS2_App < Safrano::ServerApp
end

class API_TC < BotanicsDBTest
  
  
  def test_api_publish_ok
    assert_nothing_raised do
      ODataBotanicsAPI_TC1_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    app = ODataBotanicsAPI_TC1_App.new
    assert_equal Safrano::ServiceV1, app.v1.class
    assert_equal Safrano::ServiceV2, app.v2.class

    assert_equal '', app.xpath_prefix
    assert_equal '', app.v1.xpath_prefix
    assert_equal '', app.v2.xpath_prefix


    assert_equal Safrano::Batch::DisabledHandler, app.batch_handler.class
    assert_equal Safrano::Batch::DisabledHandler, app.v1.batch_handler.class
    assert_equal Safrano::Batch::DisabledHandler, app.v2.batch_handler.class

  end

  def test_path_prefix

    assert_nothing_raised do
      ODataBotanicsAPI_TCPrefix_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/botany.svc'

        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    app = ODataBotanicsAPI_TCPrefix_App.new
    assert_equal '/botany.svc', app.xpath_prefix
    assert_equal '/botany.svc', app.v1.xpath_prefix
    assert_equal '/botany.svc', app.v2.xpath_prefix

  end

  def test_nopath_prefix

    assert_nothing_raised do
      ODataBotanicsAPI_TCNoPrefix_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        # path_prefix not called !
        
        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    app = ODataBotanicsAPI_TCNoPrefix_App.new
    assert_equal '', app.xpath_prefix
    assert_equal '', app.v1.xpath_prefix
    assert_equal '', app.v2.xpath_prefix

  end


  def test_enable_batch

    assert_nothing_raised do
      ODataBotanicsAPI_TCBatch_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/botany.svc'
        enable_batch

        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    app = ODataBotanicsAPI_TCBatch_App.new

    assert_equal Safrano::Batch::EnabledHandler, app.batch_handler.class
    assert_equal Safrano::Batch::EnabledHandler, app.v1.batch_handler.class
    assert_equal Safrano::Batch::EnabledHandler, app.v2.batch_handler.class
  end

  def test_publish_invalid_model
    assert_raise Safrano::API::ModelNameError do
      ODataBotanicsAPI_TC1_App.publish_service do
        publish_model NonModelClass
      end
      ODataBotanicsAPI_TC1_App.new
    end
  end

  def test_publish_invalid_type
    assert_raise Safrano::API::ComplexTypeNameError do
      ODataBotanicsAPI_TC1_App.publish_service do
        publish_complex_type NonModelClass
      end
      ODataBotanicsAPI_TC1_App.new
    end
  end

  def test_publish_invalid_association_single
    assert_raise Safrano::API::ModelAssociationNameError do
      ODataBotanicsAPI_TCAssoc_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model API00Test::Cultivar do
          # hey...
          add_nav_prop_single :breeeeeeeeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
      ODataBotanicsAPI_TCAssoc_App.new
    end
  end


  def test_publish_invalid_association_collection
    assert_raise Safrano::API::ModelAssociationNameError do
      ODataBotanicsAPI_TCAssoc_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder
          # hey...
          add_nav_prop_collection :paaaaaaaaaaaarent
          add_nav_prop_collection :child
        end
        publish_model API00Test::Breeder do add_nav_prop_collection :cultivar end

        publish_model API00Test::Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child

        end
      end
      ODataBotanicsAPI_TCAssoc_App.new
    end
  end
  
  def test_publish_duplicate_entity_attribute
    assert_raise Safrano::API::ModelDuplicateAttributeError do
      ODataBotanicsAPI_TCDuplAttr1_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model API00Test::Cultivar do
          # try to define nav attr breeder_id--° dupl. attr name
          add_nav_prop_single :breeder, 'breeder_id'
        end
      end
      ODataBotanicsAPI_TCDuplAttr1_App.new      
    end
  end
  # no error if service has only function imports but no models published
  def test_publish_only_function_no_model
    assert_nothing_raised do
      ODataBotanicsAPI_TCOnlyFuncNoModel_App.publish_service do
        function_import(:func_servertime)  
          .input(:inp => Edm::String)
          .return(Edm::String) 
          .definition ->(inp){ "#{Time.now}" }      
      end
      ODataBotanicsAPI_TCOnlyFuncNoModel_App.new
    end
  end
  # function import did not provide a definiton lambda or a return-block 
  def test_function_import_missing_definition
    assert_raise Safrano::FunctionImport::DefinitionMissing do
      ODataBotanicsAPI_TCFuncDefProcMissing_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'
        
        publish_model API00Test::Cultivar
        function_import(:func_without_defproc)  
          .input(:inp => Edm::String)
          .return(Edm::String) # no return code block
          # no definition(lambda)
      end
      ODataBotanicsAPI_TCFuncDefProcMissing_App.new
    end
  end
  # function import block/lambda redefinition
  def test_function_proc_redefinition
    assert_raise Safrano::FunctionImport::ProcRedefinition do
      ODataBotanicsAPI_TCFuncProcRedefined_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'
        
        publish_model API00Test::Cultivar
        function_import(:func_with_redefined_defproc)  
          .input(:inp => Edm::String)
          .return(Edm::String) do  inp end # 1st proc=block
          .definition ->(inp){ "redefinition of def-proc"}
      end
      ODataBotanicsAPI_TCFuncProcRedefined_App.new
    end    
    # same test with return_collection
    assert_raise Safrano::FunctionImport::ProcRedefinition do
      ODataBotanicsAPI_TCFuncProcRedefined_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'
        
        publish_model API00Test::Cultivar
        function_import(:func2_with_redefined_defproc)  
          .input(:inp => Edm::String)
          .return_collection(API00Test::Cultivar) do  API00Test::Cultivar end # 1st proc=block
          .definition ->(inp){ API00Test::Cultivar  } # 2nd proc=lambda
      end
      ODataBotanicsAPI_TCFuncProcRedefined_App.new
    end       
  end
  def test_function_proc_definition_ok
    assert_nothing_raised do
      ODataBotanicsAPI_TCFuncDefProcNotMissing_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'
        
        publish_model API00Test::Cultivar
        
        function_import(:func1_with_singledefined_defproc)  
          .input(:inp => Edm::String)
          .return_collection(API00Test::Cultivar) 
          .definition ->(inp){ API00Test::Cultivar  } # 1st proc=lambda
          
        function_import(:func2_with_singledefined_defproc)  
          .input(:inp => Edm::String)
          .return(API00Test::Cultivar) 
          .definition ->(inp){ API00Test::Cultivar.first  } # 1st proc=lambda  
           
        function_import(:func3_with_singledefined_defproc)  
          .input(:inp => Edm::String)
          .return_collection(Cultivar) do Cultivar end  # 1st proc=block
          # no lambda redefinition
          
        function_import(:func4_with_singledefined_defproc)  
          .input(:inp => Edm::String)
          .return(API00Test::Cultivar)  do API00Test::Cultivar.first end  # 1st proc=block
          # no lambda redefinition                    
      end
      ODataBotanicsAPI_TCFuncDefProcNotMissing_App.new
    end
  end
#  class ODataBotanicsAPI_TCFuncDefNotMissing_App < Safrano::ServerApp

  def test_publish_duplicate_coll_attribute
    assert_raise Safrano::API::ModelDuplicateAttributeError do
      ODataBotanicsAPI_TCDuplAttr2_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model API00Test::Cultivar do
          add_nav_prop_single :breeder, 'parent'
          add_nav_prop_collection :parent # duplicate attr. with the previous line 
          add_nav_prop_collection :child
        end

      end
      ODataBotanicsAPI_TCDuplAttr2_App.new
    end
  end
  
  def test_no_namespace

    assert_nothing_raised do
      ODataBotanicsAPI_TC_NS0_App.publish_service do
        title 'Cultivar Parentage OData API without namespace'
        name 'ParentageServiceNS0'

        publish_model API00Test::Cultivar 
        
      end
      ODataBotanicsAPI_TC_NS0_App.new
    end
    assert_equal 'API00Test::Cultivar', API00Test::Cultivar.type_name
  end
  
  def test_namespace

    assert_nothing_raised do
      ODataBotanicsAPI_TC_NS1_App.publish_service do
        title 'Cultivar Parentage OData API with namespace'
        name 'ParentageServiceNS1'
        namespace 'ODataPar'
        publish_model API00Test::Cultivar 
        
      end
      ODataBotanicsAPI_TC_NS1_App.new
    end
    assert_equal 'ODataPar.API00Test::Cultivar', API00Test::Cultivar.type_name
    
    # same but when namespace spec is done at the end
    assert_nothing_raised do
      ODataBotanicsAPI_TC_NS2_App.publish_service do
        title 'Cultivar Parentage OData API with namespace at end'
        name 'ParentageServiceNS2'

        publish_model API00Test::Cultivar 
        
        namespace 'ODataPar2'
      end
      ODataBotanicsAPI_TC_NS2_App.new
    end
    assert_equal 'ODataPar2.API00Test::Cultivar', API00Test::Cultivar.type_name    
  end
  
end
