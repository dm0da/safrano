

require 'tmpdir'
require 'pathname'
TESTDIR = Pathname.new(__dir__).freeze

# if we run on gitlab-CI, pg host is postgres, otherwise assume it runs locally
PG_HOST = ENV['CI'] ? 'postgres'.freeze : 'localhost'.freeze

# this would be for local unix socket conn. But that does not seem to make any 
# difference in execution time
#PG_HOST = ENV['CI'] ? 'postgres'.freeze : '/var/run/postgresql'.freeze

PG_DB = 'safrano'.freeze
PG_USER = 'safrano'.freeze
PG_PASSWORD = 'password'.freeze
PG_OPTIONS = '--client-min-messages=warning'.freeze                                             
PG_SETENV = %Q(env PGOPTIONS="#{PG_OPTIONS}" PGHOST="#{PG_HOST}" PGUSER="#{PG_USER}" PGPASSWORD="#{PG_PASSWORD}" )



def link_db_connect(type)
  case type.to_sym
  when :sqlite, :postgres
    Dir.chdir(TESTDIR) do
      # TODO use a single central Test-DB registry eg $DB or Safrano::Test:DB...
      %W(sail botanics chinookn3 botypes).each{|testdb|
        Dir.chdir(testdb) do FileUtils.ln_sf("db_#{type}.rb", 'db.rb') end
        puts "Linked db_#{type}.rb to db.rb in test-dir #{testdb}"
      }
    end
  else
    raise 'Unsupported DB type'
  end
end

def create_db(type)
  # reset/create fresh postgresql test-db
  case type.to_sym
  when :postgres
    pg_dropdb && pg_createdb 
  end
end

# cleanup after testing
def drop_db(type)
  $DB.each{|name, db_conn| db_conn.disconnect }
  case type.to_sym
  when :postgres
    pg_dropdb 
  end
end

def pg_dropdb
 system %Q(#{PG_SETENV} dropdb  #{PG_DB} --if-exists)
end

def pg_createdb
  system %Q(#{PG_SETENV} createdb -l 'C.UTF-8' -T template0 #{PG_DB} )
end

def system_pgsql_load(dumpfile)
  system %Q(#{PG_SETENV} psql -q #{PG_DB}  < #{dumpfile} > pg_init_load.log )
end

# refresh/rebuild postgresql test-db's
def refresh_postgresql_testdb(name)
  Dir.chdir(TESTDIR + name) do
    print "Refresh Postgresql #{name} test-DB..."
    init_load = case name
                when 'botanics', 'botypes'
                  system_pgsql_load("./#{name}_v0_schema.sql") && 
                  system_pgsql_load("./#{name}_v0_data.sql")
                when 'sail', 'chinookn3'
                  system_pgsql_load("./#{name}_pg_dump.sql")
                end
    if init_load
      puts 'done'
    else
      raise "'could not refresh postgresql #{name} test-db !'"
    end
  end
  refresh_media_testdata(name)
end

def create_dummy_tables(name)

  if (name == 'botanics')

    # dummy table for testing Sequel exceptions
    Sequel::Model.db.create_table :posts do
      primary_key :id
      column :title, :text
      String :content
    end unless Sequel::Model.db.tables.include? :posts
    
    
    # dummy table for testing multi-pk models with mixed type (string/integer)
    Sequel::Model.db.create_table :multi_pk do
      column :id, :integer, :null=>false
      column :ids, String, :null=>false 
      column :title, :text
      primary_key([:id, :ids])
    end unless Sequel::Model.db.tables.include? :multi_pk
    
    # dummy table for testing Media API error
    Sequel::Model.db.create_table :not_a_media do
      primary_key :id
      column :title, :text
      String :content
    end unless Sequel::Model.db.tables.include? :not_a_media

  end
end

# create a few test records
def create_dummy_data(name)

  if (name == 'botanics')

    $DB[:botanics].transaction do
      Multi_PK.insert(id: 1, ids: 'foo', title: 'Records 1.foo' )
      Multi_PK.insert(id: 1, ids: 'bar', title: 'Records 1.bar' )
      Multi_PK.insert(id: 2, ids: 'foo', title: 'Record 2.foo' )
      Multi_PK.insert(id: 2, ids: 'bar', title: 'Record 2.bar' )
    end unless Multi_PK[2, 'bar']

  end
  
end
# refresh/rebuild sqlite test-db's
def refresh_sqlite_testdb(name)

    
    dbfile = File.join(TMPDIR, "#{name}_test.db3")
    
  # create the botanics and sail db3 from sqlite dump file because its more flexibel
  # when schema changes are needed
  # TODO: generalise this to the other test-db's

  Dir.chdir(TESTDIR + name) do
    print "Refresh Sqlite #{name} test-DB..."
    begin
      
      case name
        when 'sail'
          FileUtils.rm "#{name}_v0.db3" if File.exist? "#{name}_v0.db3"
          system('sqlite3 sail_v0.db3 < sail_db3_schema.sql  && sqlite3 sail_v0.db3 < sail_db3_data.sql')
        when 'chinookn3'
        else
          FileUtils.rm "#{name}_v0.db3" if File.exist? "#{name}_v0.db3"
          system("sqlite3 #{name}_v0.db3 < #{name}_v0.db3.sql ")
      end
      FileUtils.cp("#{name}_v0.db3", dbfile)
      puts 'done'
    rescue
      raise "Could not refresh sqlite #{name} db !"
    end
  end

  refresh_media_testdata(name)

end

def   refresh_media_testdata(name)
  begin
    case name 
      when 'botanics'
        print "Refreshing  #{name} media test-data..."
        test_media_dir = File.join(TMPDIR, "#{name}/media")
        uploadfiles_dir = File.join(TMPDIR, "#{name}/uploadfiles")
        Dir.chdir(TESTDIR + name) do
          FileUtils.rm_rf test_media_dir if Dir.exist?(test_media_dir)
          FileUtils.mkdir_p test_media_dir
          FileUtils.mkdir_p uploadfiles_dir unless Dir.exist?(uploadfiles_dir)
          Dir.chdir 'media_v0' do
            Dir.glob('*').each{|f|
              FileUtils.cp_r f, test_media_dir
              }
            
          end
          Dir.chdir 'uploadfiles' do
            Dir.glob('*').each{|f|
              FileUtils.cp_r f, uploadfiles_dir
              }
            
          end
        end
        puts 'done'
      else
    end
  rescue 
    raise "Could not refresh  #{name} media test-data !"
  end
end

module Safrano
  module Test
    module DB
      def self.postgres(typsym)
        refresh_postgresql_testdb(typsym.to_s)
        Sequel::Model.db = Sequel.postgres(PG_DB,
                                           host: PG_HOST,
                                           user: PG_USER,
                                           password: PG_PASSWORD)
        self.register_db(typsym)                         
      end

      def self.sqlite(typsym)
        refresh_sqlite_testdb(typsym.to_s)
    
        dbrname = File.join(TMPDIR, "#{typsym}_test.db3")
        Sequel::Model.db = Sequel.sqlite(dbrname)
        
        self.register_db(typsym)
  
      end      
      def self.register_db(typsym)
          $DB = $DB || {}
          at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
          $DB[typsym] =  Sequel::Model.db
      end      
    end

  end
end