
require_relative 'test_helper.rb'
require_relative './test_chinook_db.rb' # Chinook-DB testdata

class ChinookEntityRelationTest < ChinookDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  def test_nav_to_empty_nav_collection
    get '/Customer(60)/invoices'
    assert_last_resp_empty_coll_parseable_ok
  end

  def expand_nav_empty_nav_collection
    get '/Customer(60)?$expand=invoices'
    assert_expanded_entity_mult(Customer[60], @jresd, :invoices){|exp|
      "Invoice(#{exp.InvoiceId})"
    }
  end
  
  # expand nav attribut of target cardinality 1
  def test_expand_to_one_attr
    get '/Customer(60)?$expand=salesrep'
    assert_last_resp_entity_parseable_ok('Customer')
    assert_expanded_entity(Customer[60], @jresd, :salesrep){|expanded| "Employee(#{expanded.EmployeeId})"}
  end

  # expand empty nav attribut (FK NULL) of target cardinality 1
  def test_expand_empty_to_one_attr
    # employee(1) reports to nobody
    get '/Employee(1)?$expand=repto'
    assert_last_resp_entity_parseable_ok('Employee')
    assert_equal Hash.new, @jresd[:repto]
  end


  # should still pass if we expand one level deeper
  def test_expand_to_one_attr_2a
    get '/Customer(60)?$expand=salesrep/repto'
    assert_last_resp_entity_parseable_ok('Customer')
    assert_expanded_entity(Customer[60], @jresd, :salesrep){|expanded| "Employee(#{expanded.EmployeeId})"}
  end

  # and we can check data one level deeper
  def test_expand_to_one_attr_2b
    get '/Customer(60)?$expand=salesrep/repto'
    assert_last_resp_entity_parseable_ok('Customer')
    assert_expanded_entity(Customer[60].salesrep,
                           @jresd[:salesrep], :repto){|expanded| "Employee(#{expanded.EmployeeId})"}
  end

  # expand deep nav attribut of target cardinality N
  # Invoice(411) with his customer with all invoices expanded
  def test_expand_to_multi_attr
    get '/Invoice(411)?$expand=customer/invoices'

    assert_last_resp_entity_parseable_ok('Invoice')

    # on first level (customer) it's a to-1 rel... ==> assert_expanded_entity
    assert_expanded_entity(Invoice[411], @jresd, :customer){|exp|
      "Customer(#{exp.CustomerId})"
    }

    # on second level (customer/invoices) it is a to-N rel ==> assert_expanded_entity_mult
    assert_expanded_entity_mult(Invoice[411].customer, @jresd[:customer], :invoices){|exp|
      "Invoice(#{exp.InvoiceId})"
    }
  end
  
  def test_expand_bad_request
    
    # positive test
    get '/Invoice(411)?$expand=invoice_items/track'
    assert_last_resp_entity_parseable_ok('Invoice')
    
    # bad requ.
    get '/Invoice(411)?$expand=invoice_items/treck'
    assert_bad_request '$expand', 'path', 'invoice_items/treck', 'invalid'

    # bad requ.
    get '/Invoice(411)?$expand=invoice_items/tracking'
    assert_bad_request '$expand', 'path', 'invoice_items/tracking', 'invalid'


  end
  
end


