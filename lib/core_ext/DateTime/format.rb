# frozen_string_literal: true

module Safrano
  module CoreExt
    module DateTime
      module Format
        REGEX = /\/Date\((-?\d+)(?:([-+])(\d+))?\)\//.freeze
        # Input:   /Date(120000+150)/
        # Match groups
        # 1.	120000
        # 2.	+
        # 3.	150

        def from_edm_json(instr)
          return unless (md = instr.match(REGEX))

          if md[3].nil? # no offset
            ::DateTime.strptime(md[1], '%Q')
          else
            # offset in hours / mins
            off_h, off_m = md[3].to_i.divmod(60)
            # DateTime.strptime("120000+2:30", '%Q%z')   milliseconds since epoch with a z-offset
            # --> #<DateTime: 1970-01-01T02:32:00+02:30 ((2440588j,120s,0n),+9000s,2299161j)>
            ::DateTime.strptime("#{md[1]}#{md[2]}#{off_h}:#{off_m}", '%Q%z')
          end
        end
      end
    end
  end

  module CoreIncl
    module DateTime
      module Format
        # https://www.odata.org/documentation/odata-version-2-0/json-format/
        # Edm.DateTime
        # "/Date(<ticks>["+" | "-" <offset>)/"
        #                      <ticks> = number of milliseconds since midnight Jan 1, 1970
        #                      <offset> = number of minutes to add or subtract
        # https://stackoverflow.com/questions/10286204/what-is-the-right-json-date-format/10286228#10286228

        # Formatting: look at
        # https://services.odata.org/V2/Northwind/Northwind.svc/Employees?$format=json
        # the json raw data is like this :   "HireDate": "\/Date(704678400000)\/"
        # --> \/\/
        def to_edm_json
          if offset.zero?
            # no offset
            # --> %Q milliseconds since epoch
            strftime('/Date(%Q)/')
          else
            # same as above with GMT offset in minutes
            # DateTime offset is Rational ; fraction of hours per Day --> *24*60
            min_off_s = (min_off = (offset * 60 * 24).to_i).positive? ? "+#{min_off}" : min_off.to_s
            "/Date(#{strftime('%Q')}#{min_off_s})/"
          end
        end
      end
    end
  end
end
