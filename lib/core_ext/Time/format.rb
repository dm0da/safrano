# frozen_string_literal: true

module Safrano
  module CoreExt
    module Time
      module Format
        REGEX = /\/Date\((-?\d+)(?:([-+])(\d+))?\)\//.freeze
        # Input:   /Date(120000+150)/
        # Match groups
        # 1.	120000
        # 2.	+
        # 3.	150

        def from_edm_json(instr)
          return unless (md = instr.match(REGEX))

          sec, milli = md[1].to_i.divmod(1000)
          secm = milli.zero? ? sec : sec + (Float(milli) / 1000)
          if md[3].nil? # no offset
            #            ::Time.at(sec, milli,  :millisecond)  # not supported in ruby 2.4
            ::Time.gm(1970, 1, 1) + secm
          else
            # offset in hours / mins
            off_h, off_m = md[3].to_i.divmod(60)
            # add leading 0 because Time.at is expecting "+HH:MM", "-HH:MM"
            off_h = off_h < 10 ? "0#{off_h}" : off_h.to_s
            off_m = off_m < 10 ? "0#{off_m}" : off_m.to_s
            # Time.strptime("120000+2:30", '%Q%z')   milliseconds since epoch with a z-offset
            # --> #<DateTime: 1970-01-01T02:32:00+02:30 ((2440588j,120s,0n),+9000s,2299161j)>
            # ::Time.at(sec, milli,  :millisecond, in: "#{md[2]}#{off_h}:#{off_m}") # not supported in ruby 2.4
            offset_str = "#{md[2]}#{off_h}:#{off_m}"
            ::Time.new(1970, 1, 1, 0, 0, 0, offset_str) + secm + ::Time.zone_offset(offset_str)
          end
        end
      end
    end
  end

  module CoreIncl
    module Time
      module Format
        # Welcome to Hell
        # https://www.odata.org/documentation/odata-version-2-0/json-format/
        # Edm.DateTime
        # "/Date(<ticks>["+" | "-" <offset>)/"
        #                      <ticks> = number of milliseconds since midnight Jan 1, 1970
        #                      <offset> = number of minutes to add or subtract
        # https://stackoverflow.com/questions/10286204/what-is-the-right-json-date-format/10286228#10286228
        # also https://www.hanselman.com/blog/on-the-nightmare-that-is-json-dates-plus-jsonnet-and-aspnet-web-api
        # and https://itecnote.com/tecnote/c-use-json-net-to-parse-json-date-of-format-dateepochtime-offset/
        # https://docs.microsoft.com/de-de/dotnet/standard/datetime/system-text-json-support
        # https://docs.microsoft.com/en-us/dotnet/framework/wcf/feature-details/stand-alone-json-serialization#datestimes-and-json
        # https://blogs.sap.com/2017/01/05/date-and-time-in-sap-gateway-foundation/

        # Formatting: look at
        # https://services.odata.org/V2/Northwind/Northwind.svc/Employees?$format=json
        # the json raw data is like this :   "HireDate": "\/Date(704678400000)\/"
        # --> \/\/
        def to_edm_json
          if utc? || gmt_offset.zero?
            # no offset
            # %s : seconds since unix epoch
            # %L : milliseconds 000-999
            # --> %S%L milliseconds since epoch
            strftime('/Date(%s%L)/')
          else
            # same as above with GMT offset in minutes

            min_off_s = (min_off = gmt_offset / 60).positive? ? "+#{min_off}" : min_off.to_s
            "/Date(#{strftime('%s%L')}#{min_off_s})/"
          end
        end
      end
    end
  end
end
