# picked from activsupport; needed for ruby < 2.5
# Destructively converts all keys using the +block+ operations.
# Same as +transform_keys+ but modifies +self+.
module Safrano
  module CoreIncl
    module Hash
      module Transform
        unless method_defined? :transform_keys!
          def transform_keys!
            keys.each do |key|
              self[yield(key)] = delete(key)
            end
            self
          end
        end

        def symbolize_keys!
          transform_keys! do |key|
            begin
              key.to_sym
            rescue StandardError
              key
            end
          end
        end
      end
    end
  end
end
