# frozen_string_literal: true

module Safrano
  module CoreIncl
    module REXML
      module Document
        module Output
          def to_pretty_xml
            formatter = ::REXML::Formatters::Pretty.new(2)
            formatter.compact = true
            formatter.write(root, strio = ::String.new)
            strio
          end
        end
      end
    end
  end
end
