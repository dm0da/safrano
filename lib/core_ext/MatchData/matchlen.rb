# mach_length for ruby < 3.0
module Safrano
  module CoreIncl
    module MatchData
      module MatchLen
        unless method_defined? :match_length
          def  match_length(i)
            self[i].length
          end
        end
      end
    end
  end
end
