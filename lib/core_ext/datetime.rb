require_relative 'DateTime/format'
require 'date'

DateTime.include Safrano::CoreIncl::DateTime::Format
DateTime.extend Safrano::CoreExt::DateTime::Format
