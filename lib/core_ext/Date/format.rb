# frozen_string_literal: true

module Safrano
  module CoreExt
    module Date
      module Format
        # Date is a DateTime with time 00:00:00 thus the milliseconds are ALWAYS 000
        # excepted when we get Date(0)  = Epoch date
        REGEX = /\/Date\((?:(-?\d+)000|0+)\)\//.freeze
        # Input:   /Date(86400000)/
        # Match groups
        # 1.	86400
        # Input:   /Date(0)/
        # Match groups
        # 1.

        def from_edm_json(instr)
          return unless (md = instr.match(REGEX))

          if md[1].nil? # no offset relative to Epoch
            ::Date.new(1970, 1, 1).freeze
          else
            # parse as seconds since epoch
            ::Date.strptime(md[1], '%s')
          end
        end
      end
    end
  end

  module CoreIncl
    module Date
      module Format
        # https://www.odata.org/documentation/odata-version-2-0/json-format/
        # Edm.DateTime
        # "/Date(<ticks>["+" | "-" <offset>)/"
        #                      <ticks> = number of milliseconds since midnight Jan 1, 1970
        #                      <offset> = number of minutes to add or subtract
        # https://stackoverflow.com/questions/10286204/what-is-the-right-json-date-format/10286228#10286228
        def to_edm_json
          # no offset
          # --> %Q milliseconds since epoch
          # Formatting: look at
          # https://services.odata.org/V2/Northwind/Northwind.svc/Employees?$format=json
          # the json raw data is like this :   "HireDate": "\/Date(704678400000)\/"
          # --> \/\/
          strftime('/Date(%Q)/')
        end
      end
    end
  end
end
