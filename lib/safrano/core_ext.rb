# frozen_string_literal: true

Dir.glob(File.expand_path('../core_ext/*.rb', __dir__)).sort.each do |path|
  require path
end

# small helper method
# http://stackoverflow.com/
#        questions/24980295/strictly-convert-string-to-integer-or-nil
def number_or_nil(str)
  num = str.to_i
  num if num.to_s == str
end
