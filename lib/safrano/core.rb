# frozen_string_literal: true

# our main namespace
module Safrano
  # frozen empty Array/Hash to reduce unncecessary object creation
  EMPTY_ARRAY = [].freeze
  EMPTY_HASH = {}.freeze
  EMPTY_HASH_IN_ARY = [EMPTY_HASH].freeze
  EMPTY_STRING = ''
  ARY_204_EMPTY_HASH_ARY = [204, EMPTY_HASH, EMPTY_ARRAY].freeze
  SPACE = ' '
  COMMA = ','

  # some prominent constants... probably already defined elsewhere eg in Rack
  # but lets KISS
  CONTENT_TYPE = 'content-type'
  CONTENT_LENGTH = 'content-length'
  LOCATION = 'location'

  TEXTPLAIN_UTF8 = 'text/plain;charset=utf-8'
  APPJSON = 'application/json'
  APPXML = 'application/xml'
  MP_MIXED = 'multipart/mixed'
  APPXML_UTF8 = 'application/xml;charset=utf-8'
  APPATOMXML_UTF8 = 'application/atomsvc+xml;charset=utf-8'
  APPJSON_UTF8 = 'application/json;charset=utf-8'

  CT_JSON = { CONTENT_TYPE => APPJSON_UTF8 }.freeze
  CT_TEXT = { CONTENT_TYPE => TEXTPLAIN_UTF8 }.freeze
  CT_ATOMXML = { CONTENT_TYPE => APPATOMXML_UTF8 }.freeze
  CT_APPXML = { CONTENT_TYPE => APPXML_UTF8 }.freeze

  module NavigationInfo
    attr_reader :nav_parent
    attr_reader :navattr_reflection
    attr_reader :nav_name

    def set_relation_info(parent, name)
      @nav_parent = parent
      @nav_name = name
      @navattr_reflection = parent.class.association_reflections[name.to_sym]
      @nav_klass = @navattr_reflection[:class_name].constantize
    end
  end
end
