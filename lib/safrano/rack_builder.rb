# frozen_string_literal: true

require 'rack'
require 'rack/cors'

module Rack
  module Safrano
    # just a Wrapper to ensure (force?) that mandatory middlewares are acutally
    # used
    LOCALHOST_ANY_PORT_RGX = /\A(?:https?:\/\/)?localhost(?::\d+)?\z/.freeze
    CORS_RO_METHODS = %i[get head options].freeze
    CORS_BATCH_METHODS = %i[post head options].freeze
    CORS_RW_METHODS = %i[get post put patch delete head options].freeze
    class Builder < ::Rack::Builder
      def initialize(default_app = nil, &block)
        super(default_app) {}
        @middlewares = []

        instance_eval(&block) if block_given?
        prepend_rackcors_ifneeded
      end

      def use(middleware, *args, &block)
        @middlewares << middleware
        super(middleware, *args, &block)
      end

      def prepend_rackcors_ifneeded
        return if stack_has_rackcors

        # get the safrano app path prefix
        # normally @run is a Safrano Server app
        return unless @run.is_a? ::Safrano::ServerApp

        service_path_prefix = @run.get_path_prefix.dup

        # due to a bug in rack-cors
        # we cant use the batch ressource path as
        # a string but need to pass it as a regexp
        # ( bug fixed in rack-cors git / new release? but still need to workaround it for
        # current/old releases ),
        batch_path_regexp = if service_path_prefix.empty?
                              # Ressource /$batch
                              /\A\/\$batch\z/
                            else
                              # Ressource  like  /foo/bar/baz/$batch
                              service_path_prefix.sub!(::Safrano::TRAILING_SLASH_RGX, '')
                              service_path_prefix.sub!(::Safrano::LEADING_SLASH_RGX, '')
                              # now is  foo/bar/baz
                              path_prefix_rgx = Regexp.escape("/#{service_path_prefix}/$batch")
                              # now  escaped path regexp /foo/bar/baz/$batch
                              # finaly just add start / end anchors
                              /\A#{path_prefix_rgx}\z/
                            end
        # this will append rack-cors mw
        # per default allow GET * from everywhere
        #             allow POST $batch from localhost only
        use ::Rack::Cors do
          allow do
            origins LOCALHOST_ANY_PORT_RGX
            resource batch_path_regexp, headers: :any, methods: CORS_BATCH_METHODS
            resource '*', headers: :any, methods: CORS_RW_METHODS
          end
          allow do
            origins '*'
            resource '*', headers: :any, methods: CORS_RO_METHODS
          end
        end

        # we need it in first place... move last element to beginin of mw stack
        rackcors = @use.delete_at(-1)
        @use.insert(0, rackcors)
      end

      def stack_has_rackcors
        @middlewares.find { |mw| mw == Rack::Cors }
      end
    end
  end
end
