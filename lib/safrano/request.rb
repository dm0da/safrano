# frozen_string_literal: true

require 'rack'
require 'rfc_2047'

module Safrano
  # handle GET PUT etc
  module MethodHandlers
    def odata_options
      # cf. stackoverflow.com/questions/22924678/sinatra-delete-response-headers
      headers.delete('Content-Type')
      @response.headers.delete('Content-Type')
      @response.headers['Content-Type'] = ''

      # we only let rack-cors handle Cors OPTIONS .
      # otherwise dont do it...
      # see https://www.mnot.net/blog/2012/10/29/NO_OPTIONS
      # 501 not implemented
      [501, EMPTY_HASH, '']
    end

    def odata_delete
      @walker.finalize.tap_error { |err| return err.odata_get(self) }
             .if_valid { |context| context.odata_delete(self) }
    end

    def odata_put
      @walker.finalize.tap_error { |err| return err.odata_get(self) }
             .if_valid { |context| context.odata_put(self) }
    end

    def odata_patch
      @walker.finalize.tap_error { |err| return err.odata_get(self) }
             .if_valid { |context| context.odata_patch(self) }
    end

    def odata_get
      @walker.finalize.tap_error { |err| return err.odata_get(self) }
             .if_valid { |context| context.odata_get(self) }
    end

    def odata_post
      @walker.finalize.tap_error { |err| return err.odata_get(self) }
             .if_valid { |context| context.odata_post(self) }
    end

    def odata_head
      [200, EMPTY_HASH, [EMPTY_STRING]]
    end
  end

  # monkey patch deactivate Rack/multipart because it does not work on simple
  # OData $batch requests when the content-length
  # is not passed
  class Request < Rack::Request
    HEADER_PARAM = /\s*[\w.]+=(?:[\w.]+|"(?:[^"\\]|\\.)*")?\s*/.freeze
    HEADER_VAL_RAW = '(?:\w+|\*)\/(?:\w+(?:\.|\-|\+)?|\*)*'
    HEADER_VAL_WITH_PAR = /(?:#{HEADER_VAL_RAW})\s*(?:;#{HEADER_PARAM})*/.freeze
    ON_CGST_ERROR = (proc { |r| raise(Sequel::Rollback) if r.in_changeset })

    METHODS_REGEXP = Regexp.new('HEAD|OPTIONS|GET|POST|PATCH|MERGE|PUT|DELETE').freeze
    NOCACHE_HDRS = { 'Cache-Control' => 'no-cache',
                     'Expires' => '-1',
                     'Pragma' => 'no-cache' }.freeze
    DATASERVICEVERSION = 'DataServiceVersion'

    # borowed from Sinatra
    class AcceptEntry
      attr_accessor :params
      attr_reader :entry

      def initialize(entry)
        params = entry.scan(HEADER_PARAM).map! do |s|
          key, value = s.strip.split('=', 2)
          value = value[1..-2].gsub(/\\(.)/, '\1') if value.start_with?('"')
          [key, value]
        end

        @entry = entry
        @type = entry[/[^;]+/].delete(' ')
        @params = Hash[params]
        @q = @params.delete('q') { 1.0 }.to_f
      end

      def <=>(other)
        other.priority <=> priority
      end

      def priority
        # We sort in descending order; better matches should be higher.
        [@q, -@type.count('*'), @params.size]
      end

      def respond_to?(*args)
        super || to_str.respond_to?(*args)
      end

      def to_str
        @type
      end
    end

    ## original coding:
    #    # The set of media-types. Requests that do not indicate
    #    # one of the media types presents in this list will not be eligible
    #    # for param parsing like soap attachments or generic multiparts
    #    PARSEABLE_DATA_MEDIA_TYPES = [
    #      'multipart/related',
    #      'multipart/mixed'
    #    ]
    #    # Determine whether the request body contains data by checking
    #    # the request media_type against registered parse-data media-types
    #    def parseable_data?
    #      PARSEABLE_DATA_MEDIA_TYPES.include?(media_type)
    #    end
    def parseable_data?
      false
    end

    # OData extension
    attr_accessor :service_base
    attr_accessor :service
    attr_accessor :walker

    # request is part of a $batch changeset
    attr_accessor :in_changeset

    # content_id of request in a $batch changeset
    attr_accessor :content_id

    # content-id references map
    attr_accessor :content_id_references

    include MethodHandlers

    def initialize(env, service_base)
      super(env)
      @service_base = service_base
    end

    # process the request and return finished response
    def process
      begin
        @response = Safrano::Response.new

        before.tap_error { |err| dispatch_error(err) }
              .tap_valid { |_res| dispatch }

      # handle remaining Sequel errors that we couldnt prevent with our
      # own pre-checks
      rescue Sequel::Error => e
        dispatch_error(SequelExceptionError.new(e))
      end
      @response.finish
    end

    def before
      negotiate_service_version.tap_valid do
        myhdrs = NOCACHE_HDRS.dup
        myhdrs[DATASERVICEVERSION] = @service.data_service_version
        headers myhdrs
      end
    end

    # dispatch for all methods requiring parsing of the path
    # with walker (ie. allmost all excepted HEAD)
    def dispatch_with_walker
      @walker = create_odata_walker
      case request_method
      when 'GET'
        odata_get
      when 'POST'
        odata_post
      when 'DELETE'
        odata_delete
      when 'OPTIONS'
        odata_options
      when 'PUT'
        odata_put
      when 'PATCH', 'MERGE'
        odata_patch
      else
        raise Error
      end
    end

    def dispatch_error(err)
      @response.status, rsph, @response.body = err.odata_get(self)
      headers rsph
    end

    def dispatch
      req_ret = if request_method !~ METHODS_REGEXP
                  [404, EMPTY_HASH, ['Did you get lost?']]
                elsif request_method == 'HEAD'
                  odata_head
                else
                  dispatch_with_walker
                end
      @response.status, rsph, @response.body = req_ret
      headers rsph
    end

    # Set multiple response headers with Hash.
    def headers(hash = nil)
      @response.headers.merge! hash if hash
      @response.headers
    end

    # stores the newly created entity for the current content-id of
    # the processed request
    def register_content_id_ref(new_entity)
      return unless @in_changeset
      return unless @content_id

      @content_id_references[@content_id] = new_entity
    end

    def create_odata_walker
      @env['safrano.walker'] = @walker = Walker.new(@service,
                                                    path_info,
                                                    self,
                                                    @content_id_references)
    end

    def accept
      @env['safrano.accept'] ||= begin
        if @env.include?('HTTP_ACCEPT') && (@env['HTTP_ACCEPT'].to_s != '')
          @env['HTTP_ACCEPT'].to_s.scan(HEADER_VAL_WITH_PAR)
                             .map! { |e| AcceptEntry.new(e) }.sort
        else
          [AcceptEntry.new('*/*')]
        end
      end
    end

    def accept?(type)
      preferred_type(type).to_s.include?(type)
    end

    def preferred_type(*types)
      accepts = accept # just evaluate once
      return accepts.first if types.empty?

      types.flatten!
      return types.first if accepts.empty?

      accepts.detect do |pattern|
        type = types.detect { |t| File.fnmatch(pattern, t) }
        return type if type
      end
    end

    def with_media_data
      if (filename = @env['HTTP_SLUG'])

        yield @env['rack.input'],
               content_type.split(';').first,
               Rfc2047.decode(filename)

      else
        ON_CGST_ERROR.call(self)
        [400, EMPTY_HASH, ['File upload error: Missing SLUG']]
      end
    end

    def with_parsed_data(type)
      if content_type == APPJSON
        # Parse json payload
        begin
          data = Safrano::OData::JSON.parse_one(body.read, type)
        rescue JSON::ParserError => e
          ON_CGST_ERROR.call(self)
          return [400, EMPTY_HASH, ['JSON Parser Error while parsing payload : ',
                                    e.message]]
        end

        yield data

      else # TODO: other formats

        [415, EMPTY_HASH, EMPTY_ARRAY]
      end
    end

    # input is the       DataServiceVersion request header string, eg.
    # '2.0;blabla' ---> Version -> 2
    DATASERVICEVERSION_RGX = /\A([1234])(?:\.0);*\w*\z/.freeze

    MAX_DTSV_PARSE_ERROR = Safrano::BadRequestError.new(
      'MaxDataServiceVersion could not be parsed'
    ).freeze
    def get_maxversion
      if (rqv = env['HTTP_MAXDATASERVICEVERSION'])
        if (m = DATASERVICEVERSION_RGX.match(rqv))
          # client request an too old version --> 501
          if (maxv = m[1]) < Safrano::MIN_DATASERVICE_VERSION
            Safrano::VersionNotImplementedError
          else
            Contract.valid(maxv)
          end
        else
          MAX_DTSV_PARSE_ERROR
        end
      else
        # not provided in request header --> take ours
        Safrano::CV_MAX_DATASERVICE_VERSION
      end
    end

    DTSV_PARSE_ERROR = Safrano::BadRequestError.new(
      'DataServiceVersion could not be parsed'
    ).freeze
    def get_version
      if (rqv = env['HTTP_DATASERVICEVERSION'])
        if (m = DATASERVICEVERSION_RGX.match(rqv))
          # client request an too new version --> 501
          if ((v = m[1]) > Safrano::MAX_DATASERVICE_VERSION) ||
             (v < Safrano::MIN_DATASERVICE_VERSION)
            Safrano::VersionNotImplementedError
          else
            Contract.valid(v)
          end
        else
          DTSV_PARSE_ERROR
        end
      else
        # not provided in request header --> take our maxv
        Safrano::CV_MAX_DATASERVICE_VERSION
      end
    end

    MIN_DTSV_PARSE_ERROR = Safrano::BadRequestError.new(
      'MinDataServiceVersion could not be parsed'
    ).freeze

    def get_minversion
      if (rqv = env['HTTP_MINDATASERVICEVERSION'])
        if (m = DATASERVICEVERSION_RGX.match(rqv))
          # client request an too new version --> 501
          if (minv = m[1]) > Safrano::MAX_DATASERVICE_VERSION
            Safrano::VersionNotImplementedError
          else
            Contract.valid(minv)
          end
        else
          MIN_DTSV_PARSE_ERROR
        end
      else
        # not provided in request header --> take ours
        Safrano::CV_MIN_DATASERVICE_VERSION
      end
    end

    MAX_LT_MIN_DTSV_ERROR = Safrano::BadRequestError.new(
      'MinDataServiceVersion is larger as MaxDataServiceVersion'
    ).freeze

    def negotiate_service_version
      get_maxversion.if_valid do |maxv|
        get_minversion.if_valid do |minv|
          return MAX_LT_MIN_DTSV_ERROR if minv > maxv

          get_version.if_valid do |_v|
            @service = nil
            @service = case maxv
                       when '1'
                         @service_base.v1
                       when '2', '3', '4'
                         @service_base.v2
                       else
                         return Safrano::VersionNotImplementedError
                       end

            Contract::OK
          end # valid get_version
        end # valid get_minversion
      end # valid get_maxversion
    end
  end
end
