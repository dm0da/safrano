# frozen_string_literal: true

require 'rexml/document'
require 'odata/relations'
require 'odata/batch'
require 'odata/complex_type'
require 'odata/function_import'
require 'safrano/type_mapping'
require 'odata/error'
require 'odata/filter/sequel'
require 'set'
require 'odata/collection'

module Safrano
  # these modules have all methods related to expand/defered output preparation
  # and will be included in Service class

  METADATA_K = '__metadata'
  EMPTYH = {}.freeze

  module ExpandHandler
    PATH_SPLITTER = %r{\A(\w+)/?(.*)\z}.freeze
    DEFERRED = '__deferred'
    URI = 'uri'

    def get_deferred_odata_h(entity_uri:, attrib:)
      { DEFERRED => { URI => "#{entity_uri}/#{attrib}" } }
    end

    # default v2
    # overriden in ServiceV1
    RESULTS_K = 'results'
    COUNT_K = '__count'
    def get_coll_odata_h(array:, template:, icount: nil)
      array.map! do |w|
        get_entity_odata_h(entity: w, template: template)
      end
      icount ? { RESULTS_K => array, COUNT_K => icount } : { RESULTS_K => array }
    end

    # handle $links ... Note: $expand seems to be ignored when $links
    # are  requested
    def get_entity_odata_link_h(entity:)
      { uri: entity.uri }
    end

    def get_entity_odata_h(entity:, template:)
      hres = {}
      # finalise the template according to options
      # (eg. skip metadata and or deferred...)
      @final_template_func.call(template)

      template.each do |elmt, arg|
        case elmt
        when :meta
          hres[METADATA_K] = entity.metadata_h

        when :all_values
          hres.merge! entity.casted_values

        when :selected_vals
          hres.merge! entity.casted_values(arg)

        when :expand_e

          arg.each do |attr, templ|
            enval = entity.send(attr)
            hres[attr] = if enval
                           get_entity_odata_h(entity: enval, template: templ)
                         else
                           # FK is NULL --> nav_value is nil --> return empty json
                           EMPTYH
                         end
          end

        when :expand_c
          arg.each do |attr, templ|
            next unless  (encoll = entity.send(attr))

            #  nav attributes that are a collection (x..n)
            hres[attr] = get_coll_odata_h(array: encoll, template: templ)
            # else error ?
          end

        when :deferr
          euri = entity.uri
          arg.each do |attr|
            hres[attr] = get_deferred_odata_h(entity_uri: euri, attrib: attr)
          end
        end
      end
      hres
    end
  end
end

module Safrano
  # xml namespace constants needed for the output of XML service and
  # and metadata
  module XMLNS
    MSFT_ADO = 'http://schemas.microsoft.com/ado'
    MSFT_ADO_2009_EDM = "#{MSFT_ADO}/2009/11/edm"
    MSFT_ADO_2007_EDMX = "#{MSFT_ADO}/2007/06/edmx"
    MSFT_ADO_2007_META = "#{MSFT_ADO}/2007/08/dataservices/metadata"

    W3_2005_ATOM = 'http://www.w3.org/2005/Atom'
    W3_2007_APP = 'http://www.w3.org/2007/app'
  end
end

# Link to Model
module Safrano
  MAX_DATASERVICE_VERSION = '2'
  MIN_DATASERVICE_VERSION = '1'
  CV_MAX_DATASERVICE_VERSION = Contract.valid(MAX_DATASERVICE_VERSION).freeze
  CV_MIN_DATASERVICE_VERSION = Contract.valid(MIN_DATASERVICE_VERSION).freeze
  TRAILING_SLASH_RGX = %r{/\z}.freeze
  LEADING_SLASH_RGX = %r{\A/}.freeze
  include XMLNS
  GENERIC_415_RESP = [415, {}, ['']].freeze

  class ServerApp
    include Safrano
    include Safrano::Edm
    include ExpandHandler

    XML_PREAMBLE = %(<?xml version="1.0" encoding="utf-8" standalone="yes"?>\r\n)

    # This is just a hash of entity Set Names to the corresponding Class
    # Example
    # Book < Sequel::Model(:book)
    #    @entity_set_name = 'books'
    # end
    # ---> @cmap ends up as   {'books' => Book }
    attr_reader :cmap
    attr_reader :xbugfix_create_response
    attr_reader :allowed_transitions

    # this is just the *sorted* list of the entity classes
    # (ie... @cmap.values.sorted)
    attr_reader :collections

    attr_reader :xtitle
    attr_reader :xname
    attr_reader :xnamespace
    attr_reader :xpath_prefix
    attr_reader :xserver_url
    attr_reader :uribase
    attr_reader :meta
    attr_reader :batch_handler
    attr_reader :complex_types
    attr_reader :function_imports
    attr_reader :function_import_keys
    attr_reader :type_mappings
    attr_reader :final_template_func
    attr_reader :response_format_options

    # Instance attributes for specialized Version specific Instances
    attr_reader :v1
    attr_reader :v2

    # TODO: more elegant design
    attr_reader :data_service_version

    # for response format options
    FINAL_TEMPLATE_FUNC_DEFAULT = ->(_template) {}
    FINAL_TEMPLATE_FUNC_SKIP_META = ->(template) { template.delete(:meta) }
    FINAL_TEMPLATE_FUNC_SKIP_DEFERR = ->(template) { template.delete(:deferr) }
    FINAL_TEMPLATE_FUNC_SKIP_META_DEFERR = ->(template) { template.delete(:meta); template.delete(:deferr) }

    def self.app_setup_singleton
      @app_setup_singleton
    end

    def initialize(template_instance: nil)
      @meta = ServiceMeta.new(self)
      @batch_handler = Safrano::Batch::DisabledHandler.new
      @complex_types = Set.new
      @function_imports = {}
      @function_import_keys = []
      @cmap = {}
      @type_mappings = {}
      @response_format_options = []
      @final_template_func = FINAL_TEMPLATE_FUNC_DEFAULT
      # enabled per default starting from 0.6
      @xbugfix_create_response = true
      klass = self.class
      # TODO better error
      raise StandardError unless (klass.publish_proc or template_instance)

      tmpli = if template_instance
                template_instance
              elsif klass.app_setup_singleton
                klass.app_setup_singleton
              end
      initialize_fork(tmpli) if tmpli
    end

    def initialize_fork(template_instance)
      @batch_handler = template_instance.batch_handler.dup
      @complex_types = template_instance.complex_types.dup
      @function_imports = template_instance.function_imports.dup
      @function_import_keys = template_instance.function_import_keys.dup
      @cmap = template_instance.cmap.dup
      @type_mappings = template_instance.type_mappings.dup
      @response_format_options = template_instance.response_format_options.dup
      @final_template_func = template_instance.final_template_func.dup
      @xbugfix_create_response = template_instance.xbugfix_create_response.dup
      @xpath_prefix = template_instance.xpath_prefix.dup
      @collections = template_instance.collections.dup
      @xname = template_instance.xname.dup
      @xserver_url = template_instance.xserver_url.dup
      @uribase = template_instance.uribase.dup
      @xtitle = template_instance.xtitle.dup
      @xnamespace = template_instance.xnamespace.dup
      @v1 = template_instance.v1.dup
      @v2 = template_instance.v2.dup
      @data_service_version = template_instance.data_service_version.dup
      @allowed_transitions = template_instance.allowed_transitions.dup
    end

    def call(env)
      Safrano::Request.new(env, self).process
    end

    def process_publish_service(&block)
      instance_eval(&block)
      finalize_publishing
      enable_v1_service
      enable_v2_service
    end

    def self.publish_service(&block)
      @publish_proc = block
      @app_setup_singleton = self.new
      @app_setup_singleton.process_publish_service(&block)
    end

    def self.publish_proc
      @publish_proc
    end

    DEFAULT_PATH_PREFIX = '/'
    DEFAULT_SERVER_URL = 'http://localhost:9494'

    def enable_batch
      @batch_handler = Safrano::Batch::EnabledHandler.new
      (@v1.enable_batch) if @v1
      (@v2.enable_batch) if @v2
      self
    end

    def enable_v1_service
      @v1 = Safrano::ServiceV1.new(template_instance: self)
    end

    def enable_v2_service
      @v2 = Safrano::ServiceV2.new(template_instance: self)
    end

    # public API
    def name(nam)
      @xname = nam
    end

    def namespace(namsp)
      @xnamespace = namsp
    end

    def title(tit)
      @xtitle = tit
    end

    def path_prefix(path_pr)
      @xpath_prefix = path_pr.sub(TRAILING_SLASH_RGX, '')
      @xpath_prefix.freeze
      (@v1.path_prefix(path_pr)) if @v1
      (@v2.path_prefix(path_pr)) if @v2
      self
    end

    # needed for safrano-rack_builder
    def get_path_prefix
      @xpath_prefix
    end

    def server_url(surl)
      @xserver_url = surl.sub(TRAILING_SLASH_RGX, '')
      @xserver_url.freeze
      (@v1.server_url(surl)) if @v1
      (@v2.server_url(surl)) if @v2
    end

    VALID_RESP_FORMAT_OPTS = [:skip_deferred, :skip_metadata]
    def valid_resp_format_options(*args)
      args.map!(&:to_sym)
      args.each { |arg|
        raise API::InvalidRespFormatOption.new(arg) unless VALID_RESP_FORMAT_OPTS.include?(arg)
      }
      yield(args)
    end

    def response_format_options(*args)
      valid_resp_format_options(*args) do |vargs|
        @response_format_options = vargs.dup
        process_response_format_options
        @v1.response_format_options(*vargs) if @v1
        @v2.response_format_options(*vargs) if @v2
      end
      self
    end

    # keep the bug active for now, but allow to de-activate the fix
    def bugfix_create_response(bool)
      @xbugfix_create_response = bool
    end

    # end public API
    def process_response_format_options
      @final_template_func = if (@response_format_options.include?(:skip_metadata) &&
                                    @response_format_options.include?(:skip_deferred))
                               FINAL_TEMPLATE_FUNC_SKIP_META_DEFERR
                             elsif @response_format_options.include?(:skip_metadata)
                               FINAL_TEMPLATE_FUNC_SKIP_META
                             elsif @response_format_options.include?(:skip_deferred)
                               FINAL_TEMPLATE_FUNC_SKIP_DEFERR
                             else
                               FINAL_TEMPLATE_FUNC_DEFAULT
                             end
    end

    def set_uribase
      @uribase = if @xpath_prefix.empty?
                   @xserver_url
                 elsif @xpath_prefix[0] == '/'
                   "#{@xserver_url}#{@xpath_prefix}"
                 else
                   "#{@xserver_url}/#{@xpath_prefix}"
                 end
      (@v1.uribase = @uribase) if @v1
      (@v2.uribase = @uribase) if @v2
    end

    # this is a central place. We extend Sequel models with OData functionality
    # The included/extended modules depends on the properties(eg, pks, field types) of the model
    # we differentiate
    #  * Single/Multi PK
    #  * Media/Non-Media entity
    # Putting this logic here in modules loaded once on start shall result in less runtime overhead
    def register_model(modelklass, entity_set_name = nil, is_media: false)
      # check that the provided klass is a Sequel Model

      raise(Safrano::API::ModelNameError, modelklass) unless modelklass.is_a? Sequel::Model::ClassMethods

      if modelklass.primary_key.is_a?(Array) # first API call... (normal non-testing case)
        modelklass.extend Safrano::EntityClassMultiPK
        modelklass.include Safrano::EntityMultiPK
      else

        modelklass.extend Safrano::EntityClassSinglePK
        modelklass.include Safrano::EntitySinglePK

      end

      # initialize state
      modelklass.reset

      # Media/Non-media
      if is_media
        modelklass.extend Safrano::EntityClassMedia
        # set default media handler . Can be overridden later with the
        #  "use HandlerKlass, options" API

        modelklass.set_default_media_handler
        modelklass.api_check_media_fields
        modelklass.include Safrano::MediaEntity
      else
        modelklass.extend Safrano::EntityClassNonMedia
        modelklass.include Safrano::NonMediaEntity
      end

      #      this needs to be done later as it can depend on overrided attribute specific Edm type
      #      eg. Edm-Guid. --> moved to finalize_publishing
      #      modelklass.prepare_pk

      modelklass.prepare_fields

      esname = (entity_set_name || modelklass).to_s.freeze
      serv_namespace = @xnamespace
      modelklass.instance_eval do
        @entity_set_name = esname
        @namespace = serv_namespace
      end
      @cmap[esname] = modelklass
      set_collections_sorted(@cmap.values)
    end

    def publish_model(modelklass, entity_set_name = nil, &block)
      register_model(modelklass, entity_set_name)
      # we need to execute the passed block in a deferred step
      # after all models have been registered (due to rel. dependancies)
      #      modelklass.instance_eval(&block) if block_given?
      modelklass.deferred_iblock = block if block_given?
    end

    def publish_media_model(modelklass, entity_set_name = nil, &block)
      register_model(modelklass, entity_set_name, is_media: true)
      # we need to execute the passed block in a deferred step
      # after all models have been registered (due to rel. dependancies)
      #      modelklass.instance_eval(&block) if block_given?
      modelklass.deferred_iblock = block if block_given?
    end

    def copy_namespace_to(klass)
      serv_namespace = @xnamespace
      klass.instance_eval { @namespace = serv_namespace }
    end

    def publish_complex_type(ctklass)
      # check that the provided klass is a Safrano ComplexType

      raise(Safrano::API::ComplexTypeNameError, ctklass) unless ctklass.superclass == Safrano::ComplexType

      copy_namespace_to(ctklass)

      @complex_types.add ctklass
    end

    def function_import(name_p)
      name = name_p.to_s
      funcimp = Safrano::FunctionImport(name)
      @function_imports[name] = funcimp
      @function_import_keys << name
      set_funcimports_sorted
      funcimp
    end

    def with_db_type(*dbtypnams, &proc)
      m = RgxTypeMapping.builder(*dbtypnams, &proc)
      @type_mappings[m.db_types_rgx] = m
    end

    def cmap=(imap)
      @cmap = imap
      set_collections_sorted(@cmap.values)
    end

    # take care of sorting required to match longest first while parsing
    # with     base_url_regexp
    # example: CrewMember must be matched before Crew otherwise we get error
    def set_collections_sorted(coll_data)
      @collections = coll_data
      @collections.sort_by! { |klass| klass.entity_set_name.size }.reverse! if @collections
      @collections
    end

    # need to be sorted by size too
    def set_funcimports_sorted
      @function_import_keys.sort_by!(&:size).reverse!
    end
    # to be called at end of publishing block to ensure we get the right names
    # and additionally build the list of valid attribute path's used
    # for validation of $orderby or $filter params

    def finalize_publishing
      # build the cmap
      @cmap = {}
      @collections&.each do |klass|
        @cmap[klass.entity_set_name] = klass
        # set namespace needed to have qualified type name
        copy_namespace_to(klass)
      end

      # now that we know all model klasses we can handle relationships
      execute_deferred_iblocks

      # set default path prefix if path_prefix was not called
      path_prefix(DEFAULT_PATH_PREFIX) unless @xpath_prefix

      # set default server url if server_url was not called
      server_url(DEFAULT_SERVER_URL) unless @xserver_url

      set_uribase

      # finalize the uri's and include NoMappingBeforeOutput or MappingBeforeOutput as needed
      @collections&.each do |klass|
        klass.finalize_publishing(self)

        klass.build_uri(@uribase)

        # Output create (POST) as single entity (Standard) or as array (non-standard buggy)
        klass.include(@xbugfix_create_response ? Safrano::EntityCreateStandardOutput : Safrano::EntityCreateArrayOutput)

        # define the most optimal casted_values method for the given model(klass)
        if klass.casted_cols.empty?
          klass.send(:define_method, :casted_values) do |cols = nil|
            cols ? selected_values_for_odata(cols) : values_for_odata
          end
        else
          klass.send(:define_method, :casted_values) do |cols = nil|
            # we need to dup the model values as we need to change it before passing to_json,
            # but we dont want to interfere with Sequel's owned data
            # (eg because then in worst case it could happen that we write back changed values to DB)
            vals = cols ? selected_values_for_odata(cols) : values_for_odata.dup
            self.class.casted_cols.each { |cc, lambda| vals[cc] = lambda.call(vals[cc]) if vals.key?(cc) }
            vals
          end
        end
        # needed for example if the pk is a guid, saved as binary on DB and the DB
        # does not have a real uuid type but just return binary data...
        # --> we need to convert the pk similarly as for json output
        unless klass.primary_key.is_a?(Array) # we do this only for single PK
          # guid in multi-PK not yet supported
          if klass.pk_castfunc
            klass.include Safrano::PKUriWithFunc
          else
            klass.include Safrano::PKUriWithoutFunc
          end
        end
      end

      # build allowed transitions (requires that @collections are filled and sorted for having a
      # correct base_url_regexp)
      build_allowed_transitions

      # mixin adapter specific modules where needed
      case Sequel::Model.db.adapter_scheme
      when :postgres
        Safrano::Filter::FuncTree.include Safrano::Filter::FuncTreePostgres
        Safrano::Filter::DateTimeLit.include Safrano::Filter::DateTimeDefault
        Safrano::Filter::DateTimeOffsetLit.include Safrano::Filter::DateTimeDefault
      when :sqlite
        Safrano::Filter::FuncTree.include Safrano::Filter::FuncTreeSqlite
        Safrano::Filter::DateTimeLit.include Safrano::Filter::DateTimeSqlite
        Safrano::Filter::DateTimeOffsetLit.include Safrano::Filter::DateTimeSqlite
      else
        Safrano::Filter::FuncTree.include Safrano::Filter::FuncTreeDefault
        Safrano::Filter::DateTimeLit.include Safrano::Filter::DateTimeDefault
        Safrano::Filter::DateTimeOffsetLit.include Safrano::Filter::DateTimeDefault
      end

      # check function import definition
      function_imports.each_value(&:check_definition)
    end

    def execute_deferred_iblocks
      return unless @collections

      @collections.each do |k|
        k.instance_eval(&k.deferred_iblock) if k.deferred_iblock
      end
    end

    ## Warning: base_url_regexp depends on '@collections', and this needs to be
    ## evaluated after '@collections' is filled !
    # A regexp matching all allowed base entities (eg product|categories )
    def base_url_regexp
      return unless @collections

      @collections.map(&:entity_set_name).join('|')
    end

    def base_url_func_regexp
      @function_import_keys.join('|')
    end

    def service
      hres = {}
      hres['d'] = { 'EntitySets' => @collections.map(&:type_name) }
      hres
    end

    def service_xml(_req)
      doc = REXML::Document.new
      # separator required ? ?
      root = doc.add_element('service', 'xml:base' => @uribase)

      root.add_namespace('xmlns:atom', XMLNS::W3_2005_ATOM)
      root.add_namespace('xmlns:app', XMLNS::W3_2007_APP)

      # this generates the main xmlns attribute
      root.add_namespace(XMLNS::W3_2007_APP)
      wp = root.add_element 'workspace'

      title = wp.add_element('atom:title')
      title.text = @xtitle

      @collections.each do |klass|
        col = wp.add_element('collection', 'href' => klass.entity_set_name)
        ct = col.add_element('atom:title')
        ct.text = klass.entity_set_name
      end

      XML_PREAMBLE + doc.to_pretty_xml
    end

    def add_metadata_xml_entity_type(schema)
      relman = Safrano::RelationManager.new
      @collections.each do |klass|
        enty = klass.add_metadata_rexml(schema)
        klass.add_metadata_navs_rexml(enty, relman)
      end
      # return the collected rels needed later
      relman
    end

    def add_metadata_xml_complex_types(schema)
      @complex_types.each { |ctklass| ctklass.add_metadata_rexml(schema)   }
    end

    def add_metadata_xml_function_imports(ec)
      @function_imports.each_value { |func| func.add_metadata_rexml(ec)    }
    end

    def add_metadata_xml_associations(schema, relman)
      relman.each_rel do |rel|
        rel.with_metadata_info(@xnamespace) do |name, bdinfo|
          assoc = schema.add_element('Association', 'Name' => name)
          bdinfo.each do |bdi|
            assoend = { 'Type' => bdi[:type],
                        'Role' => bdi[:role],
                        'Multiplicity' => bdi[:multiplicity] }
            assoc.add_element('End', assoend)
          end
        end
      end
    end

    def add_metadata_xml_entity_container(schema, relman)
      ec = schema.add_element('EntityContainer',
                              'Name' => @xname,
                              'm:IsDefaultEntityContainer' => 'true')
      @collections.each do |klass|
        # 3.a Entity set's
        ec.add_element('EntitySet',
                       'Name' => klass.entity_set_name,
                       'EntityType' => klass.type_name)
      end

      # 3.b Association set's
      relman.each_rel do |rel|
        assoc = ec.add_element('AssociationSet',
                               'Name' => rel.name,
                               'Association' => "#{@xnamespace}.#{rel.name}")

        rel.each_endobj do |eo|
          clazz = Object.const_get(eo)
          assoend = { 'EntitySet' => clazz.entity_set_name.to_s, 'Role' => eo }
          assoc.add_element('End', assoend)
        end
      end

      # 4 function imports
      add_metadata_xml_function_imports(ec)
    end

    def metadata_xml(_req)
      doc = REXML::Document.new
      doc.add_element('edmx:Edmx', 'Version' => '1.0')
      doc.root.add_namespace('xmlns:edmx', XMLNS::MSFT_ADO_2007_EDMX)
      serv = doc.root.add_element('edmx:DataServices',
                                  # TODO: export the real version (result from version negotions)
                                  # but currently we support only v1 and v2, and most users will use v2
                                  'm:DataServiceVersion' => '2.0')
      # 'm:DataServiceVersion' => "#{self.dataServiceVersion}" )
      # DataServiceVersion: This attribute MUST be in the data service
      # metadata namespace
      # (http://schemas.microsoft.com/ado/2007/08/dataservices) and SHOULD
      # be present on an
      # edmx:DataServices element [MC-EDMX] to indicate the version of the
      # data service CSDL
      # annotations (attributes in the data service metadata namespace) that
      # are used by the document.
      # Consumers of a data-service metadata endpoint ought to first read this
      # attribute value to determine if
      # they can safely interpret all constructs within the document. The
      # value of this attribute MUST be 1.0
      # unless a "FC_KeepInContent" customizable feed annotation
      # (section 2.2.3.7.2.1) with a value equal to
      # false is present in the CSDL document within the edmx:DataServices
      # node. In this case, the
      # attribute value MUST be 2.0 or greater.
      # In the absence of DataServiceVersion, consumers of the CSDL document
      # can assume the highest DataServiceVersion they can handle.
      serv.add_namespace('xmlns:m', XMLNS::MSFT_ADO_2007_META)

      schema = serv.add_element('Schema',
                                'Namespace' => @xnamespace,
                                'xmlns' => XMLNS::MSFT_ADO_2009_EDM)

      # 1. a. all EntityType (and get the collected rels metadata)
      relman = add_metadata_xml_entity_type(schema)

      # 1. b. all ComplexType
      add_metadata_xml_complex_types(schema)

      # 2. Associations
      add_metadata_xml_associations(schema, relman)

      # 3. Enty container
      add_metadata_xml_entity_container(schema, relman)

      XML_PREAMBLE + doc.to_pretty_xml
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      DOLLAR_ID_REGEXP = Regexp.new('\A\/\$')
      ALLOWED_TRANSITIONS_FIXED = [
        Safrano::TransitionEnd,
        Safrano::TransitionMetadata,
        Safrano::TransitionBatch,
        Safrano::TransitionContentId
      ].freeze

      include Safrano::Transitions::GetNextTrans::ByLongestMatch

      def build_allowed_transitions
        @allowed_transitions = if @function_imports.empty?
                                 (ALLOWED_TRANSITIONS_FIXED + [
                                   Safrano::Transition.new(%r{\A/(#{base_url_regexp})(.*)},
                                                           trans: 'transition_collection')
                                 ]).freeze
                               else
                                 (ALLOWED_TRANSITIONS_FIXED + [
                                   Safrano::Transition.new(%r{\A/(#{base_url_regexp})(.*)},
                                                           trans: 'transition_collection'),
                                   Safrano::Transition.new(%r{\A/(#{base_url_func_regexp})(.*)},
                                                           trans: 'transition_service_op')
                                 ]).freeze
                               end
      end

      def allowed_transitions
        @allowed_transitions
      end

      def thread_safe_collection(collklass)
        Safrano::OData::Collection.new(collklass)
      end

      def transition_collection(match_result)
        [thread_safe_collection(@cmap[match_result[1]]), :run] if match_result[1]
        #        [@cmap[match_result[1]], :run] if match_result[1]
      end

      def transition_service_op(match_result)
        [@function_imports[match_result[1]], :run] if match_result[1]
      end

      def transition_batch(_match_result)
        [@batch_handler, :run]
      end

      def transition_content_id(match_result)
        [match_result[2], :run_with_content_id]
      end

      def transition_metadata(_match_result)
        [@meta, :run]
      end

      def transition_end(_match_result)
        Safrano::Transition::RESULT_END
      end
    end

    include Transitions

    def odata_get(req)
      if req.accept?(APPXML)
        # OData V2 reference service implementations are returning app-xml-u8
        # so we do
        [200, CT_APPXML, [service_xml(req)]]
      else
        # this is returned by http://services.odata.org/V2/OData/Safrano.svc
        GENERIC_415_RESP
      end
    end
  end

  # for OData V1
  class ServiceV1 < ServerApp
    def initialize(template_instance: nil)
      super
      @data_service_version = '1.0'
    end

    def get_coll_odata_links_h(array:, icount: nil)
      array.map do |w|
        get_entity_odata_link_h(entity: w)
      end
    end

    def get_coll_odata_h(array:, template:, icount: nil)
      array.map! do |w|
        get_entity_odata_h(entity: w,
                           template: template)
      end
      array
    end

    def get_emptycoll_odata_h
      EMPTY_HASH_IN_ARY
    end
  end

  # for OData V2
  class ServiceV2 < ServerApp
    def initialize(template_instance: nil)
      super
      @data_service_version = '2.0'
    end

    def get_coll_odata_links_h(array:, icount: nil)
      res = array.map do |w|
        get_entity_odata_link_h(entity: w)
      end
      if icount
        { RESULTS_K => res, COUNT_K => icount }
      else
        { RESULTS_K => res }
      end
    end

    def get_emptycoll_odata_h
      { RESULTS_K => EMPTY_HASH_IN_ARY }
    end
  end

  # a virtual entity for the service metadata
  class ServiceMeta
    attr_reader :service

    def initialize(service)
      @service = service
    end

    ALLOWED_TRANSITIONS_FIXED = [Safrano::TransitionEnd].freeze
    def allowed_transitions
      ALLOWED_TRANSITIONS_FIXED
    end

    def transition_end(_match_result)
      Safrano::Transition::RESULT_END
    end

    include Safrano::Transitions::GetNextTrans::ForJustTransitionEnd

    def odata_get(req)
      if req.accept?(APPXML)
        [200, CT_APPXML, [@service.metadata_xml(req)]]
      else
        GENERIC_415_RESP
      end
    end
  end
end
# end of Module OData
