# frozen_string_literal: true

# Note: Safrano has hardcoded mapping rules for most of types.
# But
#   it might not be 100% complete
#   it might not always do what is expected
# The type mapping functionality here  allows Safrano users to design type mapping themselves
# and fill or fix the two above issues
module Safrano
  # Base class
  class TypeMapping
    attr_reader :castfunc
    attr_reader :edm_type
  end

  # Model attribute (column) specific mapping
  class AttributeTypeMapping < TypeMapping
    attr_reader :attr_name

    def initialize(builder)
      @edm_type = builder.xedm_type
      @castfunc = builder.castfunc
    end

    # wrapper to handle API
    class Builder
      attr_reader :xedm_type
      attr_reader :castfunc
      attr_reader :attr_name

      def initialize(atnam)
        @attr_name = atnam
      end

      def edm_type(input)
        @xedm_type = input
      end

      def type_mapping
        AttributeTypeMapping.new(self)
      end
    end

    def self.builder(atnam, &proc)
      builder = Builder.new(atnam)
      builder.instance_eval(&proc)
      builder
    end
  end

  # Regexp based Global mapping
  class RgxTypeMapping < TypeMapping
    attr_reader :db_types_rgx

    # wrapper to handle API
    class Builder
      attr_reader :xedm_type
      attr_reader :castfunc
      attr_reader :db_types_rgx
      attr_reader :bui1
      attr_reader :bui2

      def initialize(*dbtyps)
        @db_types_rgx = dbtyps.join('|')
        @rgx = /\A\s*(?:#{@db_types_rgx})\s*\z/i
      end

      def edm_type(input)
        @xedm_type = input
      end

      def with_one_param(lambda = nil, &proc)
        proc1 = block_given? ? proc : lambda
        @bui1 = Builder1Par.new(@db_types_rgx, proc1)
      end

      def with_two_params(lambda = nil, &proc)
        proc2 = block_given? ? proc : lambda
        @bui2 = Builder2Par.new(@db_types_rgx, proc2)
      end

      def json_value(lambda = nil, &proc)
        @castfunc = block_given? ? proc : lambda
      end

      def match(curtyp)
        if (@bui2 && (m = @bui2.match(curtyp))) ||
           (@bui1 && (m = @bui1.match(curtyp)))
          m
        elsif @rgx.match(curtyp)
          type_mapping
        end
      end

      def type_mapping
        # TODO: perf; return always same object when called multiple times
        RgxFixedTypeMapping.new(self)
      end
    end # Builder

    def self.builder(*dbtypnams, &proc)
      builder = Builder.new(*dbtypnams)
      builder.instance_eval(&proc)
      builder
    end

    class Builder1Par < Builder
      def initialize(db_ty_rgx, proc)
        @db_types_rgx = db_ty_rgx
        @proc = proc
        @rgx = /\A\s*(?:#{@db_types_rgx})\s*\(\s*(\d+)\s*\)\z/i
      end

      def match(curtyp)
        (@md = @rgx.match(curtyp)) ? type_mapping : nil
      end

      def json_value(lambda = nil, &proc)
        @castfunc = block_given? ? proc : lambda
      end

      # this is a bit advanced/obscure programming
      # the intance_exec here is required to produce correct results
      # ie. it produces concrete instances of edm_type and json_val lambda
      # for the later, it is kind of currying but with the *implicit* parameters
      # from the calling context eg par1

      # probably this is not best-practice programing as we
      # have a mutating object (the builder) that
      # produces different lambdas after each type_mapping(mutation) calls
      def type_mapping
        p1val = @md[1]
        instance_exec p1val, &@proc

        RgxTypeMapping1Par.new(self)
      end
    end

    class Builder2Par < Builder
      def initialize(db_ty_rgx, proc)
        @db_types_rgx = db_ty_rgx
        @proc = proc
        @rgx = /\A\s*(?:#{@db_types_rgx})\s*\(\s*(\d+)\s*,\s*(\d+)\s*\)\s*\z/i
      end

      def match(curtyp)
        (@md = @rgx.match(curtyp)) ? type_mapping : nil
      end

      # this is a bit advanced/obscure programming
      # the intance_exec here is required to produce correct results
      # ie. it produces concrete instances of edm_type and json_val lambda
      # for the later, it is kind of currying but with the *implicit* parameters
      # from the calling context eg par1 and par2

      # probably this is not best-practice programing as we
      # have a mutating object (the builder) that
      # produces different lambdas after each type_mapping(mutation) calls
      def type_mapping
        p1val = @md[1]
        p2val = @md[2]
        instance_exec p1val, p2val, &@proc
        RgxTypeMapping2Par.new(self)
      end
    end
  end

  # Fixed type (ie. without variable parts)
  class RgxFixedTypeMapping < RgxTypeMapping
    def initialize(builder)
      @edm_type = builder.xedm_type
      @castfunc = builder.castfunc
    end
  end

  class RgxTypeMapping1Par < RgxTypeMapping
    def initialize(builder)
      @edm_type = builder.xedm_type
      @castfunc = builder.castfunc
    end
  end

  class RgxTypeMapping2Par < RgxTypeMapping
    def initialize(builder)
      @edm_type = builder.xedm_type
      @castfunc = builder.castfunc
    end
  end
end
