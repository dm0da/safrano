# frozen_string_literal: true

require 'rack'
require_relative '../odata/walker'
require_relative 'request'
require_relative 'response'
