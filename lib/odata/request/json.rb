require 'json'
require 'time'
require 'base64'
# client parsing functionality to ease testing

module Safrano
  module RFC2047
    def self.encode(str)
      "=?utf-8?b?#{Base64.strict_encode64(str)}?="
    end
  end

  module OData
    # this is used to parse inbound json payload on POST / PUT & co
    # it does not do symbolize but proper (hopefully) type casting when needed
    module JSON
      # def self.parse(*args)
      #  ::JSON.parse(*args)
      # end

      def self.cast_values_in(resd, typ)
        typ.db_schema.each do |f, props|
          case props[:type]
          when :datetime, :date
            fstr = f.to_s
            resd[fstr] = Sequel.datetime_class.from_edm_json(resd[fstr]) if resd[fstr]
          end
        end

        resd
      end

      def self.parse_one(*args, type)
        cast_values_in(::JSON.parse(*args), type)
      end
    end
  end

  # This is used from the Test-suite code !
  # it does recursive / deep symbolize additionally to inbound casting
  module XJSON
    def self.get_class_from_meta(meta)
      # type is normally namespaced !  --> split.last is the class
      meta[:type].split('.').last.constantize
    end

    # symbolise keys and cast/parse values back to the proper ruby type;
    # proper type meaning the one that Sequel chooses when loading data from the DB
    # apply recursively to nested navigation attributes sub-structures
    def self.cast_values_in(resd)
      resd.symbolize_keys!

      if (defered = resd[:__deferred])
        defered.symbolize_keys!
      elsif (meta = resd[:__metadata])
        meta.symbolize_keys!

        # type is normally namespaced !  --> split.last is the class
        typ = get_class_from_meta(meta)

        typ.db_schema.each do |f, props|
          metadata = typ.cols_metadata[f]
          case props[:type]
          when :datetime
            #              resd[f] = Time.parse(resd[f]) if resd[f]
            #              resd[f] = DateTime.strptime(resd[f], '/Date(%Q)').to_time if resd[f]
            resd[f] = Sequel.datetime_class.from_edm_json(resd[f]) if resd[f]
          when :decimal
            resd[f] = BigDecimal(resd[f])
          when :float
            resd[f] = Float(resd[f])
          else
            # TODO: better typ-system
            # Currently Sequel loads Numeric(x,y) as Float
            resd[f] = Float(resd[f]) if metadata[:edm_type] =~ /\A\s*Edm.Decimal/
          end
        end

        if typ.nav_entity_attribs
          typ.nav_entity_attribs.each_key do |nattr|
            cast_values_in(resd[nattr.to_sym]) if resd[nattr.to_sym]
          end
        end

        if typ.nav_collection_attribs
          typ.nav_collection_attribs.each_key do |ncattr|
            if (resd_attr = resd[ncattr.to_sym])
              if (defered = resd_attr['__deferred'])
                defered.symbolize_keys! if defered
              else
                resd_attr.symbolize_keys! #  'results' --> :results
                resd_attr[:results].each { |enty| cast_values_in(enty) }
              end
            end
          end
        end
      end
      resd
    end

    # symbolise attrib names (h-keys)
    # apply recursively to nested navigation attributes sub-structures
    def self.symbolize_attribs_in(resd)
      resd.symbolize_keys!

      if (defered = resd[:__deferred])
        defered.symbolize_keys!
      elsif (meta = resd[:__metadata])
        meta.symbolize_keys!

        typ = get_class_from_meta(meta)

        if typ.nav_entity_attribs
          typ.nav_entity_attribs.each_key do |nattr|
            symbolize_attribs_in(resd[nattr.to_sym])
          end
        end

        if typ.nav_collection_attribs
          typ.nav_collection_attribs.each_key do |ncattr|
            if (defered = resd[ncattr.to_sym]['__deferred'])
              defered.symbolize_keys!
            else
              resd[ncattr.to_sym].each { |enty| symbolize_attribs_in(enty) }
            end
          end
        end

      end
      resd
    end

    def self.parse_one_nocast(*args)
      resh = ::JSON.parse(*args)
      symbolize_attribs_in(resh['d'])
    end

    def self.parse_one(*args)
      resh = ::JSON.parse(*args)
      cast_values_in(resh['d'])
    end

    def self.v1_parse_coll(*args)
      resh = ::JSON.parse(*args)

      resh['d'].map! { |currd| cast_values_in(currd) }

      resh['d']
    end

    def self.parse_coll(*args)
      resh = ::JSON.parse(*args)

      resh['d']['results'].map! { |currd| cast_values_in(currd) }

      resh['d']
    end

    def self.v1_parse_coll_nocast(*args)
      resh = ::JSON.parse(*args)

      resh['d'].map! { |currd| symbolize_attribs_in(currd) }

      resh['d']
    end

    def self.parse_coll_nocast(*args)
      resh = ::JSON.parse(*args)

      resh['d']['results'].map! { |currd| symbolize_attribs_in(currd) }

      resh['d']
    end
  end
end
