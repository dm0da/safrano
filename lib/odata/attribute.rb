# frozen_string_literal: true

require 'json'
require_relative '../safrano/core'
require_relative './entity'

module Safrano
  # Represents a named and valued attribute of an Entity
  class Attribute
    attr_reader :name
    attr_reader :entity

    def initialize(entity, name)
      @entity = entity
      @name = name
      @allowed_transitions = ALLOWED_TRANSITIONS
    end

    def value
      # WARNING ... this require more work to handle the timezones topci
      # currently it is just set to make some minimal testcase work
      # See also model_ext.rb
      case (v = @entity.values[@name.to_sym])
      when Date, Time, DateTime
        v.to_edm_json
      else
        v
      end
    end

    def odata_get(req)
      if req.walker.raw_value
        [200, CT_TEXT, value.to_s]
      elsif req.accept?(APPJSON)
        # json is default content type so we dont need to specify it here again
        [200, EMPTY_HASH, to_odata_json(service: req.service)]
      else # TODO: other formats
        406
      end
    end

    # output as OData json (v2)
    def to_odata_json(*)
      { 'd' => { @name => value } }.to_json
    end

    # for testing purpose (assert_equal ...)
    def ==(other)
      (@entity == other.entity) && (@name == other.name)
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def transition_end(_match_result)
        Transition::RESULT_END
      end

      def transition_value(_match_result)
        [self, :end_with_value]
      end

      ALLOWED_TRANSITIONS = [Safrano::TransitionEnd,
                             Safrano::TransitionValue].freeze

      def allowed_transitions
        Transitions::ALLOWED_TRANSITIONS
      end

      include Safrano::Transitions::GetNextTrans::BySimpleDetect
    end
    include Transitions
  end
end
