# frozen_string_literal: true

require 'rack'
require 'fileutils'
require 'tempfile'
require 'pathname'
require_relative './navigation_attribute'

module Safrano
  module Media
    # base class for Media Handler
    class Handler
      def check_before_create(data:, entity:, filename:)
        Contract::OK
      end
    end

    # Simple static File/Directory based media store handler
    # similar to Rack::Static
    # with a flat directory structure
    class Static < Handler
      def initialize(root: nil, mediaklass:)
        super()
        @root = Pathname(File.absolute_path(root || Dir.pwd))
        @files_class = ::Rack.release[0..2] == '2.0' ? ::Rack::File : ::Rack::Files
        @media_class = mediaklass
        @media_dir_name = Pathname(mediaklass.to_s)
        @semaphore = Thread::Mutex.new

        register
      end

      def register
        @abs_klass_dir = @root + @media_dir_name
        @abs_temp_dir = @abs_klass_dir.join('tmp')
      end

      def create_abs_class_dir
        FileUtils.makedirs @abs_klass_dir
      end

      def create_abs_temp_dir
        FileUtils.makedirs @abs_temp_dir
      end

      def finalize
        create_abs_class_dir
        create_abs_temp_dir
      end

      # see also ...
      # File activesupport/lib/active_support/core_ext/file/atomic.rb, line 21
      def atomic_write(file_name)
        Tempfile.open('', @abs_temp_dir) do |temp_file|
          temp_file.binmode
          return_val = yield temp_file
          temp_file.close

          # Overwrite original file with temp file
          File.rename(temp_file.path, file_name)
          return_val
        end
      end

      # minimal working implementation...
      #  Note: files_app works relative to @root directory
      def odata_get(request:, entity:)
        media_env = request.env.dup
        media_env['PATH_INFO'] = filename(entity)
        # new app instance for each call for thread safety
        files_app = @files_class.new(@root)
        fsret = files_app.call(media_env)
        if fsret.first == 200
          # provide own content type as we keep it in the media entity
          fsret[1]['Content-Type'] = entity.content_type
        end
        fsret
      end

      # this is relative to @root
      # eg. Photo/1
      def media_path(entity)
        @media_dir_name + media_directory(entity)
      end

      # relative to @root
      # eg Photo/1/1
      def filename(entity)
        media_path(entity) + ressource_version(entity)
      end

      # /@root/Photo/1
      def abs_path(entity)
        @root + media_path(entity)
      end

      # absolute filename
      def abs_filename(entity)
        @root + filename(entity)
      end

      # this is relative to abs_klass_dir(entity) eg to /@root/Photo
      # simplest implementation is media_directory = entity.media_path_id
      # --> we get a 1 level depth flat directory structure
      def media_directory(entity)
        entity.media_path_id
      end

      # the same as above but absolute
      def abs_media_directory(entity)
        @abs_klass_dir + entity.media_path_id
      end

      # yields the absolute path of media directory
      # and ensure the directory exists
      def with_media_directory(entity)
        mpi = abs_media_directory(entity)
        FileUtils.mkdir_p mpi
        yield Pathname(mpi)
      end

      def odata_delete(entity:)
        mpi = abs_media_directory(entity)
        return unless Dir.exist?(mpi)

        mpi.children.each { |oldp| File.delete(oldp) }
      end

      # Here as well, MVP implementation
      def save_file(data:, filename:, entity:)
        with_media_directory(entity) do |d|
          filename = d.join('1')
          atomic_write(filename) { |f| IO.copy_stream(data, f) }
        end
      end

      # needed for having a changing media ressource "source" metadata
      # after each upload, so that clients get informed about new versions
      # of the same media ressource
      def ressource_version(entity)
        abs_media_directory(entity).children(false).max.to_s
      end

      # Note:  add a new Version and remove the previous one
      def replace_file(data:, entity:)
        with_media_directory(entity) do |d|
          tp = Tempfile.open('', @abs_temp_dir) do |temp_file|
            temp_file.binmode
            IO.copy_stream(data, temp_file)
            temp_file.path
          end

          # picking new filename and the "move" operation must
          # be protected
          @semaphore.synchronize do
            # new filename =  "version" + 1
            v = ressource_version(entity)
            filename = d + (v.to_i + 1).to_s

            # Move temp file to original target file
            File.rename(tp, filename)

            # remove the previous version
            filename = d + v
            File.delete(filename)
          end
        end
      end
    end
    # Simple static File/Directory based media store handler
    # similar to Rack::Static
    # with directory Tree structure

    class StaticTree < Static
      SEP = '/00/'
      VERS = '/v'

      def self.path_builder(ids)
        ids.map { |id| id.to_s.chars.join('/') }.join(SEP) << VERS
      end

      # this is relative to abs_klass_dir(entity) eg to /@root/Photo
      # tree-structure
      #  media_path_ids = 1  -->  1/v
      #  media_path_ids = 15  -->  1/5/v
      #  media_path_ids = 555  -->  5/5/5/v
      #  media_path_ids = 5,5,5  -->  5/00/5/00/5/v
      #  media_path_ids = 5,00,5  -->  5/00/0/0/00/5/v
      #  media_path_ids = 5,xyz,5  -->  5/00/x/y/z/00/5/v
      def media_directory(entity)
        StaticTree.path_builder(entity.media_path_ids)
      end

      # the same as above but absolute
      def abs_media_directory(entity)
        @abs_klass_dir + StaticTree.path_builder(entity.media_path_ids)
      end

      # yields the absolute path of media directory
      # and ensure the directory exists
      def with_media_directory(entity)
        mpi = abs_media_directory(entity)

        FileUtils.makedirs mpi
        yield Pathname(mpi)
      end

      def odata_delete(entity:)
        mpi = abs_media_directory(entity)
        return unless Dir.exist?(mpi)

        mpi.children.each { |oldp| File.delete(oldp) if File.file?(oldp) }
      end
    end
  end

  # special handling for media entity
  module EntityClassMedia
    attr_reader :media_handler
    attr_reader :slug_field

    # API method for defining the media handler
    # eg.
    # publish_media_model photos do
    #   use Safrano::Media::Static, :root => '/media_root'
    # end

    def set_default_media_handler
      @media_handler = Safrano::Media::Static.new(mediaklass: self)
    end

    def finalize_media
      @media_handler.finalize
    end

    def use(klass, args)
      args[:mediaklass] = self
      @media_handler = klass.new(**args)
      @media_handler.create_abs_class_dir
    end

    # API method for setting the model field mapped to SLUG on upload
    def slug(inp)
      @slug_field = inp
    end

    def api_check_media_fields
      raise(Safrano::API::MediaModelError, self) unless db_schema.key?(:content_type)
    end

    #  END API methods

    def new_media_entity(mimetype:)
      nh = { 'content_type' => mimetype }
      new_from_hson_h(nh)
    end

    # POST for media entity collection --> Create media-entity linked to
    # uploaded file from payload
    # 1. create new media entity
    # 2. get the pk/id from the new media entity
    # 3. Upload the file and use the pk/id to get an unique Directory/filename
    #    assignment to the media entity record
    # NOTE: we will implement this first in a MVP way. There will be plenty of
    #       potential future enhancements (performance, scallability, flexibility... with added complexity)
    # 4. create relation to parent if needed
    def odata_create_entity_and_relation(req, assoc = nil, parent = nil)
      req.with_media_data do |data, mimetype, filename|
        ## future enhancement: validate allowed mimetypes ?
        # if (invalid = invalid_media_mimetype(mimetype))
        #  ::Safrano::Request::ON_CGST_ERROR.call(req)
        #  return [422, {}, ['Invalid mime type: ', invalid.to_s]]
        # end

        if req.accept?(APPJSON)

          new_entity = new_media_entity(mimetype: mimetype)

          if slug_field

            new_entity.set_fields({ slug_field => filename },
                                  data_fields,
                                  missing: :skip)
          end

          # call before_create_entity media hook
          new_entity.before_create_media_entity(data: data, mimetype: mimetype) if new_entity.respond_to? :before_create_media_entity

          media_handler.check_before_create(data: data,
                                            entity: new_entity,
                                            filename: filename).if_valid do |_ret|
            # to_one rels are create with FK data set on the parent entity
            if parent
              odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
            else
              # in-changeset requests get their own transaction
              new_entity.save(transaction: !req.in_changeset)
            end

            req.register_content_id_ref(new_entity)
            new_entity.copy_request_infos(req)

            # call before_create_media hook
            new_entity.before_create_media if new_entity.respond_to? :before_create_media

            media_handler.save_file(data: data,
                                    entity: new_entity,
                                    filename: filename)

            # call after_create_media hook
            new_entity.after_create_media if new_entity.respond_to? :after_create_media

            # json is default content type so we dont need to specify it here again
            #           Contract.valid([201, EMPTY_HASH, new_entity.to_odata_post_json(service: req.service)])
            # TODO quirks array mode !
            Contract.valid([201, EMPTY_HASH, new_entity.to_odata_create_json(request: req)])
          end.tap_error { |e| return e.odata_get(req) }.result

        else # TODO: other formats
          415
        end
      end
    end
  end
end
