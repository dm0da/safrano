# frozen_string_literal: true

require_relative 'error'

# our main namespace
module Safrano
  module Transitions
    module GetNextTrans
      module BySimpleDetect
        def get_next_transresult(path_remain)
          # current url-parsing context
          # has no ambiguous next match and we dont need to find longest match
          # but it's sufficient to find the one matching (or nil)
          tres_next = nil
          @allowed_transitions.detect { |t| tres_next = t.result(path_remain) }
          tres_next
        end
      end

      module ForJustTransitionEnd
        def get_next_transresult(path_remain)
          Safrano::TransitionEnd.result(path_remain)
        end
      end

      # Transitions::GetNextTrans::ByLongestMatch
      module ByLongestMatch
        def get_next_transresult(path_remain)
          # current url-parsing context
          # has  ambiguous next match and we need to find longest match
          # example: current context is "the top level service" and we have
          # entity types Race and RaceType

          match_len = -1
          tres_next = nil

          @allowed_transitions.each { |t|
            if (res = t.longer_match(path_remain, match_len))
              tres_next = res
              match_len = tres_next.match_length
            end
          }
          tres_next
        end
      end

      # same as ByLongestMatch but use the getter method allowed_transitions instead of
      # directly @allowed_transitions
      module ByLongestMatchDyn
        def get_next_transresult(path_remain)
          # current url-parsing context
          # has  ambiguous next match and we need to find longest match
          # example: current context is "the top level service" and we have
          # entity types Race and RaceType

          match_len = -1
          tres_next = nil

          allowed_transitions.each { |t|
            if (res = t.longer_match(path_remain, match_len))
              tres_next = res
              match_len = tres_next.match_length
            end
          }
          tres_next
        end
      end
    end
  end

  # represents a state transition when navigating/parsing the url path
  # from left to right
  class Transition
    # attr_accessor :trans
    # attr_accessor :match_result
    # attr_accessor :trans_result
    # attr_accessor :rgx
    # attr_reader   :remain_idx

    RESULT_BAD_REQ_ERR = [nil, :error, ::Safrano::BadRequestError].freeze
    RESULT_NOT_FOUND_ERR = [nil, :error, ::Safrano::ErrorNotFound].freeze
    RESULT_SERVER_TR_ERR = [nil, :error, ServerTransitionError].freeze
    RESULT_END = [nil, :end].freeze

    def initialize(arg, trans: nil, remain_idx: 2)
      @rgx = if arg.respond_to? :each_char
               Regexp.new(arg)
             else
               arg
             end.freeze
      @trans = trans.freeze
      @remain_idx = remain_idx.freeze
    end

    def result(str)
      return unless (mres = @rgx.match(str))

      TransitionResult.new(trans: @trans, match_result: mres, remain_idx: @remain_idx)
    end

    # return  match-result for str, if longer  as   min_match_length
    def longer_match(str, min_match_length)
      return unless (mres = @rgx.match(str))
      return unless (mres.match_length(1) > min_match_length)

      TransitionResult.new(trans: @trans, match_result: mres, remain_idx: @remain_idx)
    end
  end

  class TransitionResult
    attr_reader :match

    EMPTYSTR = ''
    SLASH = '/'

    def initialize(trans:, match_result:, remain_idx:)
      @trans = trans
      @match = match_result
      @remain_idx = remain_idx
    end

    def match_length
      @match.match_length(1)
    end

    # remain_idx is the index of the last match-data. ususally its 2
    # but can be overidden
    def path_remain
      @match[@remain_idx] if @match && @match[@remain_idx]
    end

    def path_done
      if @match
        @match[1] || EMPTYSTR
      else
        EMPTYSTR
      end
    end

    def do_transition(ctx)
      ctx.__send__(@trans, @match)
    end
  end

  # Transition that does not move/change the input
  class InplaceTransition < Transition
    def initialize(trans:)
      @trans = trans
    end

    def result(str)
      @str = str
      InplaceTransitionResult.new(trans: @trans, match_result: @str)
    end

    # return  match-result for str, if longer  as   min_match_length
    def longer_match(str, min_match_length)
      @str = str
      return unless (@str.size > min_match_length)

      InplaceTransitionResult.new(trans: @trans, match_result: @str)
    end
  end

  # Transition that does not move/change the input
  class InplaceTransitionResult < TransitionResult
    def initialize(trans:, match_result:)
      @trans = trans
      @match = match_result
      @str = match_result
    end

    def match_length
      @str.size
    end

    def path_remain
      @str
    end

    def path_done
      EMPTYSTR
    end
  end

  TransitionEnd = Transition.new('\A(\/?)\z', trans: :transition_end)
  TransitionExecuteFunc = InplaceTransition.new(trans: :transition_execute_func)
  TransitionMetadata = Transition.new('\A(\/\$metadata)(.*)',
                                      trans: :transition_metadata)
  TransitionBatch = Transition.new('\A(\/\$batch)(.*)',
                                   trans: :transition_batch)
  TransitionContentId = Transition.new('\A(\/\$(\d+))(.*)',
                                       trans: :transition_content_id,
                                       remain_idx: 3)
  TransitionCount = Transition.new('(\A\/\$count)(.*)\z',
                                   trans: :transition_count)
  TransitionValue = Transition.new('(\A\/\$value)(.*)\z',
                                   trans: :transition_value)
  TransitionLinks = Transition.new('(\A\/\$links)(.*)\z',
                                   trans: :transition_links)
  attr_accessor :allowed_transitions
end
