# frozen_string_literal: true

require 'json'
require 'rexml/document'
require_relative '../safrano/contract'

# Error handling
module Safrano
  # for errors occurring in API (publishing) usage --> Exceptions
  module API
    # when published class is not a Sequel Model
    class ModelNameError < NameError
      def initialize(name)
        super("class #{name} is not a Sequel Model")
      end
    end

    # when published complex type is not a Complex Type
    class ComplexTypeNameError < NameError
      def initialize(name)
        super("class #{name} is not a ComplexType")
      end
    end

    # when published class as media does not have the mandatory media fields
    class MediaModelError < NameError
      def initialize(name)
        super("Model #{name} does not have the mandatory media attributes content_type/media_src")
      end
    end

    # when published association was not defined on Sequel level
    class ModelAssociationNameError < NameError
      def initialize(klass, symb)
        symbname = symb.to_s
        msg = "There is no association :#{symbname} defined in class #{klass}"
        super(msg, symbname)
      end
    end

    # duplicate attribute name
    class ModelDuplicateAttributeError < NameError
      def initialize(klass, symb)
        symbname = symb.to_s
        msg = "There is already an attribute :#{symbname} defined in class #{klass}"
        super(msg, symbname)
      end
    end

    # invalid response format option
    class InvalidRespFormatOption < NameError
      def initialize(opt)
        msg = "Invalid response format option: #{opt}"
        super(msg, opt.to_s)
      end
    end
  end

  # used in function import error handling. cf. func import / do_execute_func
  def self.with_error(result)
    return unless result.respond_to?(:error) && (err = result.error)

    yield err
  end

  # base module for HTTP errors, when used as a Error Class
  module ErrorClass
    include ::Safrano::Contract::Invalid
    def http_code
      const_get(:HTTP_CODE)
    end
    EMPTYH = {}.freeze
    def odata_get(req)
      message = (m = @msg.to_s).empty? ? to_s : m
      if req.accept?(APPJSON)
        # json is default content type so we dont need to specify it here again
        [http_code, EMPTY_HASH,
         { 'odata.error' => { 'code' => http_code.to_s,
                              'type' => to_s,
                              'message' => message } }.to_json]
      else
        [http_code, CT_TEXT, message]
      end
    end
  end

  # base module for HTTP errors, when used as an Error instance
  module ErrorInstance
    include ::Safrano::Contract::Invalid
    # can(should) be overriden in subclasses
    def msg
      @msg
    end

    def odata_get(req)
      message = (m = msg.to_s).empty? ? self.class.to_s : m
      if req.accept?(APPJSON)
        # json is default content type so we dont need to specify it here again
        [self.class.http_code, EMPTY_HASH,
         { 'odata.error' => { 'code' => self.class.http_code.to_s,
                              'type' => self.class.to_s,
                              'message' => message } }.to_json]
      else
        [self.class.http_code, CT_TEXT, message]
      end
    end
  end

  # generic http 500 server err
  class ServerError
    extend  ErrorClass
    include ErrorInstance
    HTTP_CODE = 500
    @msg = 'Server error'
  end

  class ServiceOperationError
    extend  ErrorClass
    include ErrorInstance
    HTTP_CODE = 500
    def initialize(msg:, sopname: nil)
      @msg = "Error in service operation #{sopname}: #{msg}"
    end
  end

  # for outputing Sequel exceptions that we could not prevent
  class SequelExceptionError < ServerError
    include ErrorInstance
    def initialize(seqle7n)
      @msg = seqle7n.message
    end
  end

  # for outputing Ruby StandardError exceptions that we could not prevent
  class RubyStandardErrorException < ServerError
    include ErrorInstance
    def initialize(rubye7n)
      @msg = rubye7n.message
    end
  end

  # http Unprocessable Entity for example when trying to
  # upload duplicated media ressource
  class UnprocessableEntityError
    extend  ErrorClass
    include ErrorInstance
    HTTP_CODE = 422
    @msg = 'Unprocessable Entity'
    def initialize(reason)
      @msg = reason
    end
  end

  class AlreadyExistsUnprocessableError < UnprocessableEntityError
    @msg = 'The ressource you are trying to create already exists'
  end

  # http Bad Req.
  class BadRequestError
    extend  ErrorClass
    include ErrorInstance
    HTTP_CODE = 400
    @msg = 'Bad Request'
    def initialize(reason)
      @msg = "Bad Request : #{reason}"
    end
  end

  # for upload empty media
  class BadRequestEmptyMediaUpload < BadRequestError
    include ErrorInstance
    def initialize(path)
      @msg = "Bad Request: empty media file #{path}"
    end
  end

  # Generic failed changeset
  class BadRequestFailedChangeSet < BadRequestError
    @msg = 'Bad Request: Failed changeset '
  end

  # $value request for a non-media entity
  class BadRequestNonMediaValue < BadRequestError
    @msg = 'Bad Request: $value request for a non-media entity'
  end

  class BadRequestSequelAdapterError < BadRequestError
    include ErrorInstance
    def initialize(err)
      @msg = err.inner.message
    end
  end

  # for Syntax error in Filtering
  class BadRequestFilterParseError < BadRequestError
    @msg = 'Bad Request: Syntax error in $filter'
  end

  # for invalid attribute in path
  class BadRequestInvalidAttribPath < BadRequestError
    include ErrorInstance
    def initialize(model, attr)
      @msg = "Bad Request: the attribute #{attr} is invalid for entityset #{model.entity_set_name}"
    end
  end

  # for invalid attribute in $expand param
  class BadRequestExpandInvalidPath < BadRequestError
    include ErrorInstance
    def initialize(model, path)
      @msg = "Bad Request: the $expand path #{path} is invalid for entityset #{model.entity_set_name}"
    end
  end

  # for invalid properti(es) in $select param
  class BadRequestSelectInvalidProps < BadRequestError
    include ErrorInstance
    def initialize(model, iprops)
      @msg = (iprops.size > 1 ? "Bad Request: the $select properties #{iprops.to_a.join(', ')} are invalid for entityset #{model.entity_set_name}" : "Bad Request: the $select property #{iprops.first} is invalid for entityset #{model.entity_set_name}")
    end
  end

  # for Syntax error in $orderby param
  class BadRequestOrderParseError < BadRequestError
    @msg = 'Bad Request: Syntax error in $orderby'
  end

  # for $inlinecount error
  class BadRequestInlineCountParamError < BadRequestError
    @msg = 'Bad Request: wrong $inlinecount parameter'
  end

  # http not found
  class ErrorNotFound
    extend ErrorClass
    HTTP_CODE = 404
    @msg = 'The requested ressource was not found'
  end

  # http not found segment
  class ErrorNotFoundSegment < ErrorNotFound
    include ErrorInstance

    def initialize(segment)
      @msg = "The requested ressource segment #{segment} was not found"
    end
  end

  # Transition error (Safrano specific)
  class ServerTransitionError < ServerError
    @msg = 'Server error: Segment could not be parsed'
  end

  # not implemented (Safrano specific)
  class NotImplementedError
    extend ErrorClass
    HTTP_CODE = 501
  end

  # version not implemented (Safrano specific)
  class VersionNotImplementedError
    extend ErrorClass
    HTTP_CODE = 501
    @msg = 'The requested OData version is not yet supported'
  end

  # batch not implemented (Safrano specific)
  class BatchNotImplementedError < RuntimeError
    @msg = 'Not implemented: OData batch'
  end

  # error in filter parsing (Safrano specific)
  class FilterParseError < BadRequestError
    extend  ErrorClass
  end

  class FilterFunctionNotImplementedError < BadRequestError
    extend  ErrorClass
    include ErrorInstance
    @msg = 'the requested $filter function is Not implemented'
    def initialize(xmsg)
      @msg = xmsg
    end
  end

  class FilterUnknownFunctionError < BadRequestError
    include ErrorInstance
    def initialize(badfuncname)
      @msg = "Bad request: unknown function #{badfuncname} in $filter"
    end
  end

  class FilterParseErrorWrongColumnName < BadRequestError
    extend ErrorClass
    @msg = 'Bad request: invalid property name in $filter'
  end

  class FilterParseWrappedError < BadRequestError
    include ErrorInstance
    def initialize(exception)
      @msg = exception.to_s
    end
  end

  class ServiceOperationParameterMissing < BadRequestError
    include ErrorInstance
    def initialize(missing:, sopname:)
      @msg = "Bad request: Parameter(s) missing for for service operation #{sopname} : #{missing.join(', ')}"
    end
  end

  class ServiceOperationParameterError < BadRequestError
    include ErrorInstance
    def initialize(type:, value:, param:, sopname:)
      @type = type
      @value = value
      @param = param
      @sopname = sopname
    end

    def msg
      "Bad request: Parameter #{@param} with value '#{@value}' cannot be converted to type #{@type} for service operation #{@sopname}"
    end
  end
end
