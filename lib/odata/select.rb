# frozen_string_literal: true

require 'odata/error'

# all dataset selecting related classes in our OData module
# ie  do eager loading
module Safrano
  # base class for selecting. We have to distinguish between
  # fields of the current entity, and the navigation properties
  # we can have one special case
  #  empty, ie no $select specified --> return all fields and all nav props
  #            ==> SelectAll

  class SelectBase
    ALL = new # re-useable selecting-all handler

    def self.factory(selectstr, model)
      case selectstr&.strip
      when nil, '', '*'
        ALL
      else
        Select.new(selectstr, model)
      end
    end

    def all_props?
      false
    end

    def ALL.all_props?
      true
    end

    def parse_error?
      Contract::OK
    end
  end

  # single select
  class Select < SelectBase
    COMASPLIT = /\s*,\s*/.freeze
    attr_reader :props

    def initialize(selstr, model)
      @model = model
      @selectp = selstr.strip
      @props = @selectp.split(COMASPLIT)
    end

    def parse_error?
      (invalids = @model.find_invalid_props(props.to_set)) ? BadRequestSelectInvalidProps.new(@model, invalids) : Contract::OK
    end
  end
end
