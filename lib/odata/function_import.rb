# frozen_string_literal: true

require_relative 'complex_type'
require_relative 'edm/primitive_types'
require_relative 'transition'

module Safrano
  def self.FunctionImport(name)
    FunctionImport::Function.new(name)
  end

  module FunctionImport
    # error classes
    class DefinitionMissing < StandardError
      def initialize(fnam)
        msg = "Function import #{fnam}: definition is missing. Provide definition either as a return code block or with .definition(lambda)"
        super(msg)
      end
    end

    class ProcRedefinition < StandardError
      def initialize(fnam)
        msg = "Function import #{fnam}: Block/lambda Redefinition . Provide definition either as a return code block or with .definition(lambda) but not both"
        super(msg)
      end
    end

    # Function import object
    class Function
      @allowed_transitions = [Safrano::TransitionEnd]
      attr_reader :name
      attr_reader :proc

      def initialize(name)
        @name = name
        @http_method = 'GET'
      end

      def allowed_transitions
        [Safrano::TransitionExecuteFunc]
      end

      include Safrano::Transitions::GetNextTrans::ForJustTransitionEnd

      def get_next_transresult(path_remain)
        Safrano::TransitionExecuteFunc.result(path_remain)
      end

      def input(**parmtypes)
        @input = {}
        parmtypes.each do |k, t|
          @input[k] = case t.name
                      when 'Integer'
                        Safrano::Edm::Edm::Int32
                      when 'String'
                        Safrano::Edm::Edm::String
                      when 'Float'
                        Safrano::Edm::Edm::Double
                      when 'DateTime'
                        Safrano::Edm::Edm::DateTime
                      # UUID with uuidtools
                      when 'UUIDTools::UUID'
                        Safrano::Edm::Edm::Guid
                      else
                        t
                      end
        end
        self
      end

      def auto_query_parameters
        @auto_query_params = true
        self # chaining
      end
      alias auto_query_params auto_query_parameters

      def return(klassmod, &proc)
        @returning = if klassmod.respond_to? :return_as_instance_descriptor
                       klassmod.return_as_instance_descriptor
                     else
                       # if it's neither a ComplexType nor a Model-Entity
                       # --> assume it is a Primitive
                       ResultDefinition.asPrimitiveType(klassmod)
                     end
        # block is optional  since 0.6.7
        # the function definition can now also be made with .definition(lambda)
        # consistency check that there is a single definition either as a
        # return-block    or a definition lambda is made on publish finalise

        if block_given?
          # proc already defined...
          raise Redefinition.new(@name) if @proc

          @proc = proc
        end

        self
      end

      def definition(lambda)
        raise('Please provide a lambda') unless lambda
        # proc already defined...
        raise ProcRedefinition.new(@name) if @proc

        @proc = lambda
      end

      # this is called from service.finalize_publishing
      def check_definition
        raise DefinitionMissing.new(@name) unless @proc
      end

      def return_collection(klassmod, lambda: nil, &proc)
        @returning = if klassmod.respond_to? :return_as_collection_descriptor
                       klassmod.return_as_collection_descriptor
                     else
                       # if it's neither a ComplexType nor a Modle-Entity
                       # --> assume it is a Primitive
                       #                       ResultAsPrimitiveTypeColl.new(klassmod)
                       ResultDefinition.asPrimitiveTypeColl(klassmod)
                     end
        # block is optional  since 0.6.7
        if block_given?
          # proc already defined...
          raise ProcRedefinition.new(@name) if @proc

          @proc = proc
        end

        self
      end
      #      def initialize_params
      #        @uparms = UrlParameters4Func.new(@model, @params)
      #      end

      def check_missing_params
        # do we have all parameters provided ?  use Set difference to check
        pkeys = @params.keys.map(&:to_sym).to_set
        if (idiff = @input.keys.to_set - pkeys).empty?
          Contract::OK
        else

          Safrano::ServiceOperationParameterMissing.new(
            missing: idiff.to_a,
            sopname: @name
          )
        end
      end

      def check_url_func_params
        @funcparams = {}
        return nil unless @input # anything to check ?

        # do we have all parameters provided ?
        check_missing_params.tap_error { |error| return error }
        # ==> all params were provided

        # now we shall check the content and type of the parameters
        @input.each do |ksym, typ|
          typ.convert_from_urlparam(v = @params[ksym.to_s])
             .tap_valid do |retval|
            @funcparams[ksym] = retval
          end
             .tap_error do
            # return is really needed here, or we end up returning nil below
            return parameter_convertion_error(ksym, typ, v)
          end
        end
        nil
      end

      def parameter_convertion_error(param, type, val)
        Safrano::ServiceOperationParameterError.new(type: type,
                                                    value: val,
                                                    param: param,
                                                    sopname: @name)
      end

      def add_metadata_rexml(ec)
        ## https://services.odata.org/V2/OData/Safrano.svc/$metadata
        # <FunctionImport Name="GetProductsByRating" EntitySet="Products" ReturnType="Collection(ODataDemo.Product)" m:HttpMethod="GET">
        # <Parameter Name="rating" Type="Edm.Int32" Mode="In"/>
        # </FunctionImport>
        funky = ec.add_element('FunctionImport',
                               'Name' => @name.to_s,
                               #                       EntitySet= @entity_set ,
                               'ReturnType' => @returning.type_metadata,
                               'm:HttpMethod' => @http_method)
        if @input
          @input.each do |iname, type|
            funky.add_element('Parameter',
                              'Name' => iname.to_s,
                              'Type' => type.type_name,
                              'Mode' => 'In')
          end
        end
        funky
      end

      def with_transition_validated(req)
        #        initialize_params
        @params = req.params
        unless (@error = check_url_func_params)
          begin
            return yield
          rescue LocalJumpError
            @error = Safrano::ServiceOperationReturnError.new
          end
        end
        [nil, :error, @error] if @error
      end

      def do_execute_func(req)
        with_transition_validated(req) do
          result = @proc.call(**@funcparams)

          # (application-)error handling
          # the result is an Error object or class

          # Note: Sequel::Error exceptions are already
          # handled on rack app level (cf. the call methode )
          Safrano.with_error(result) do |error|
            @error = error
            return [nil, :error, @error] # this is return from do_execute_func !
          end

          # non error case
          [@returning.do_execute_func_result(result, req,
                                             apply_query_params: @auto_query_params),
           :run]
        end
      end

      def transition_execute_func(_match_result)
        [self, :run_with_execute_func]
      end
    end
  end
end
