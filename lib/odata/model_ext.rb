# frozen_string_literal: true

# Design: Collections are nothing more as Sequel based model classes that have
# somehow the character of an array (Enumerable)
# Thus Below we have called that "EntityClass". It's meant as "Collection"

require 'json'
require 'base64'
require 'rexml/document'
require 'bigdecimal'
require_relative '../safrano/core'
require_relative 'error'
require_relative 'collection_filter'
require_relative 'collection_order'
require_relative 'expand'
require_relative 'select'
require_relative 'url_parameters'
require_relative 'collection_media'
require_relative 'function_import'

module Safrano
  # class methods. They Make heavy use of Sequel::Model functionality
  # we will add this to our Model classes with "extend" --> self is the Class
  module EntityClassBase
    ONLY_INTEGER_RGX = /\A[+-]?\d+\z/.freeze

    attr_reader :nav_collection_url_regexp
    attr_reader :nav_entity_url_regexp
    attr_reader :entity_id_url_regexp
    attr_reader :nav_collection_attribs
    attr_reader :nav_entity_attribs
    attr_reader :data_fields
    attr_reader :default_template
    attr_reader :uri
    attr_reader :odata_upk_parts
    attr_reader :casted_cols
    attr_reader :namespace
    # store cols metata here in the model (sub)-class. Initially we stored this infos (eg. edm_types etc)
    # directly into sequels db_schema hash. But this hash is on the upper Sequel::Model(table) class
    # and is shared by all subclasses.
    # By storing it separately here we are less dependant from Sequel, and have less issues with
    # testing with multiples models class derived from same Sequel::Model(table)
    attr_reader :cols_metadata

    # initialising block of code to be executed at end of
    # ServerApp.publish_service after all model classes have been registered
    # (without the associations/relationships)
    # typically the block should contain the publication of the associations
    attr_accessor :deferred_iblock

    # allows to override standard types
    attr_accessor :type_mappings

    attr_accessor :pk_castfunc

    # convention: entityType is the namepsaced Ruby Model class --> name is just to_s
    # Warning: for handling Navigation relations, we use anonymous collection classes
    # dynamically subtyped from a Model class, and in such an anonymous class
    # the class-name is not the OData Type. In these subclass we redefine "type_name"
    # thus when we need the Odata type name, we shall use this method instead
    # of just the collection class name
    def type_name
      @type_name
    end

    def default_entity_set_name
      @default_entity_set_name
    end

    def build_type_name
      @type_name = @namespace.to_s.empty? ? to_s : "#{@namespace}.#{self}"
      @default_entity_set_name = to_s
    end

    # default for entity_set_name is @default_entity_set_name
    def entity_set_name
      @entity_set_name = (@entity_set_name || @default_entity_set_name)
    end

    def reset
      @deferred_iblock = nil
      @entity_set_name = nil
      @uri = nil
      @odata_upk_parts = nil
      @uparms = nil
      @params = nil
      @cx = nil
      @cols_metadata = {}
      @type_mappings = {}
      @pk_castfunc = nil
    end

    def build_uri(uribase)
      @uri = "#{uribase}/#{entity_set_name}"
    end

    def return_as_collection_descriptor
      Safrano::FunctionImport::ResultDefinition.asEntityColl(self)
    end

    def return_as_instance_descriptor
      Safrano::FunctionImport::ResultDefinition.asEntity(self)
    end

    def execute_deferred_iblock
      instance_eval { @deferred_iblock.call } if @deferred_iblock
    end

    # Factory json-> Model Object instance
    def new_from_hson_h(hash)
      # enty = new
      # enty.set_fields(hash, data_fields, missing: :skip)
      create(hash)
      # enty.set(hash)
    end

    def attrib_path_valid?(path)
      @attribute_path_list.include? path
    end

    def expand_path_valid?(path)
      @expand_path_list.include? path
    end

    def find_invalid_props(propsset)
      (propsset - @all_props) unless propsset.subset?(@all_props)
    end

    def build_expand_path_list
      @expand_path_list = expand_path_list
    end

    # list of table columns + all nav attribs --> all props
    def build_all_props_list
      @all_props = @columns_str.dup
      (@all_props +=  @nav_entity_attribs_keys.map(&:to_s)) if @nav_entity_attribs
      (@all_props +=  @nav_collection_attribs_keys.map(&:to_s)) if @nav_collection_attribs
      @all_props = @all_props.to_set
    end

    def build_attribute_path_list
      @attribute_path_list = attribute_path_list
    end

    MAX_DEPTH = 6
    MAX_TYPE_REPETITION = 2
    def attribute_path_list(depth_path = [])
      ret = @columns_str.dup
      # break circles
      return ret if depth_path.size > MAX_DEPTH
      return ret if depth_path.count(self) > MAX_TYPE_REPETITION

      @nav_entity_attribs&.each do |a, k|
        ret.concat(k.attribute_path_list(depth_path + [self]).map { |kc| "#{a}/#{kc}" })
      end

      @nav_collection_attribs&.each do |a, k|
        ret.concat(k.attribute_path_list(depth_path + [self]).map { |kc| "#{a}/#{kc}" })
      end
      ret
    end

    def expand_path_list(depth_path = [])
      ret = []
      ret.concat(@nav_entity_attribs_keys) if @nav_entity_attribs
      ret.concat(@nav_collection_attribs_keys) if @nav_collection_attribs

      # break circles
      return ret if depth_path.size > MAX_DEPTH
      return ret if depth_path.count(self) > MAX_TYPE_REPETITION

      @nav_entity_attribs&.each do |a, k|
        ret.concat(k.expand_path_list(depth_path + [self]).map { |kc| "#{a}/#{kc}" })
      end

      @nav_collection_attribs&.each do |a, k|
        ret.concat(k.expand_path_list(depth_path + [self]).map { |kc| "#{a}/#{kc}" })
      end
      ret
    end

    # add metadata xml to the passed REXML schema object
    def add_metadata_rexml(schema)
      enty = if @media_handler
               schema.add_element('EntityType', 'Name' => to_s, 'HasStream' => 'true')
             else
               schema.add_element('EntityType', 'Name' => to_s)
             end
      # with their properties
      db_schema.each do |pnam, prop|
        metadata = @cols_metadata[pnam]
        if prop[:primary_key] == true
          enty.add_element('Key').add_element('PropertyRef',
                                              'Name' => pnam.to_s)
        end
        attrs = { 'Name' => pnam.to_s,
                  #                  'Type' => Safrano.get_edm_type(db_type: prop[:db_type]) }
                  'Type' => metadata[:edm_type] }
        attrs['Nullable'] = 'false' if prop[:allow_null] == false
        attrs['Precision'] = '0' if metadata[:edm_type] == 'Edm.DateTime'
        enty.add_element('Property', attrs)
      end
      enty
    end

    # metadata REXML data  for a single Nav attribute
    def metadata_nav_rexml_attribs(assoc, to_klass, relman)
      from = to_s
      to = to_klass.to_s
      relman.get_metadata_xml_attribs(from,
                                      to,
                                      association_reflection(assoc.to_sym)[:type],
                                      @namespace,
                                      assoc)
    end

    # and their Nav attributes == Sequel Model association
    def add_metadata_navs_rexml(schema_enty, relman)
      @nav_entity_attribs&.each do |ne, klass|
        nattr = metadata_nav_rexml_attribs(ne,
                                           klass,
                                           relman)
        schema_enty.add_element('NavigationProperty', nattr)
      end

      @nav_collection_attribs&.each do |nc, klass|
        nattr = metadata_nav_rexml_attribs(nc,
                                           klass,
                                           relman)
        schema_enty.add_element('NavigationProperty', nattr)
      end
    end

    # Recursive
    # this method is performance critical. Called at least once for every request
    def output_template(expand_list:,
                        select: Safrano::SelectBase::ALL)

      return @default_template.dup if expand_list.empty? && select.all_props?

      template = { meta: EMPTYH }
      expand_e = {}
      expand_c = {}
      deferr = []

      # 1. handle non-navigation properties, only consider $select
      # 2. handle navigations properties, need to check $select and $expand
      if select.all_props?

        template[:all_values] = EMPTYH

        # include all nav attributes -->
        @nav_entity_attribs&.each do |attr, klass|
          if expand_list.key?(attr)
            expand_e[attr] = klass.output_template(expand_list: expand_list[attr])
          else
            deferr << attr
          end
        end

        @nav_collection_attribs&.each do |attr, klass|
          if expand_list.key?(attr)
            expand_c[attr] = klass.output_template(expand_list: expand_list[attr])
          else
            deferr << attr
          end
        end

      else
        template[:selected_vals] = @columns_str & select.props

        # include only selected nav attribs-->need additional intersection step
        if @nav_entity_attribs
          selected_nav_e = @nav_entity_attribs_keys & select.props

          selected_nav_e&.each do |attr|
            if expand_list.key?(attr)
              klass = @nav_entity_attribs[attr]
              expand_e[attr] = klass.output_template(expand_list: expand_list[attr])
            else
              deferr << attr
            end
          end
        end
        if @nav_collection_attribs
          selected_nav_c = @nav_collection_attribs_keys & select.props
          selected_nav_c&.each do |attr|
            if expand_list.key?(attr)
              klass = @nav_collection_attribs[attr]
              expand_c[attr] = klass.output_template(expand_list: expand_list[attr])
            else
              deferr << attr
            end
          end
        end
      end
      template[:expand_e] = expand_e
      template[:expand_c] = expand_c
      template[:deferr] = deferr
      template
    end

    # this functionally similar to the Sequel Rels (many_to_one etc)
    # We need to base this on the Sequel rels, or extend them
    def add_nav_prop_collection(assoc_symb, attr_name_str = nil)
      @nav_collection_attribs = (@nav_collection_attribs || {})
      @nav_collection_attribs_keys = (@nav_collection_attribs_keys || [])
      # DONE: Error handling. This requires that associations
      # have been properly defined with Sequel before
      assoc = all_association_reflections.find do |a|
        a[:name] == assoc_symb && a[:model] == self
      end

      raise Safrano::API::ModelAssociationNameError.new(self, assoc_symb) unless assoc

      attr_class = assoc[:class_name].constantize
      lattr_name_str = (attr_name_str || assoc_symb.to_s)

      # check duplicate attributes names
      raise Safrano::API::ModelDuplicateAttributeError.new(self, lattr_name_str) if @columns.include? lattr_name_str.to_sym

      if @nav_entity_attribs_keys
        raise Safrano::API::ModelDuplicateAttributeError.new(self, lattr_name_str) if @nav_entity_attribs_keys.include? lattr_name_str
      end

      @nav_collection_attribs[lattr_name_str] = attr_class
      @nav_collection_attribs_keys << lattr_name_str
      @nav_collection_url_regexp = @nav_collection_attribs_keys.join('|')
    end

    def add_nav_prop_single(assoc_symb, attr_name_str = nil)
      @nav_entity_attribs = (@nav_entity_attribs || {})
      @nav_entity_attribs_keys = (@nav_entity_attribs_keys || [])
      # DONE: Error handling. This requires that associations
      # have been properly defined with Sequel before
      assoc = all_association_reflections.find do |a|
        a[:name] == assoc_symb && a[:model] == self
      end

      raise Safrano::API::ModelAssociationNameError.new(self, assoc_symb) unless assoc

      attr_class = assoc[:class_name].constantize
      lattr_name_str = (attr_name_str || assoc_symb.to_s)

      # check duplicate attributes names
      raise Safrano::API::ModelDuplicateAttributeError.new(self, lattr_name_str) if @columns.include? lattr_name_str.to_sym

      if @nav_collection_attribs_keys
        raise Safrano::API::ModelDuplicateAttributeError.new(self, lattr_name_str) if @nav_collection_attribs_keys.include? lattr_name_str
      end

      @nav_entity_attribs[lattr_name_str] = attr_class
      @nav_entity_attribs_keys << lattr_name_str
      @nav_entity_url_regexp = @nav_entity_attribs_keys.join('|')
    end

    # allow to override default type settings on attribute level
    # for example use Edm.Guid instead of Binary(Blob) or Edm.DateTimeOffset instead of Edm.DateTime
    def with_attribute(asymb, &proc)
      am = AttributeTypeMapping.builder(asymb, &proc).type_mapping
      @type_mappings[asymb] = am
    end

    EMPTYH = {}.freeze

    def build_default_template
      @default_template = { meta:  EMPTYH, all_values: EMPTYH }
      @default_template[:deferr] = (@nav_entity_attribs&.keys || []) + (@nav_collection_attribs&.keys || EMPTY_ARRAY) if @nav_entity_attribs || @nav_collection_attribs
    end

    def build_casted_cols(service)
      # cols needed catsting before final json output
      @casted_cols = {}
      db_schema.each do |col, props|
        # first check if we have user-defined regexp based global type mapping
        usermap = nil
        dbtyp = props[:db_type]
        metadata = @cols_metadata[col]
        if service.type_mappings.values.find { |map| usermap = map.match(dbtyp) }

          metadata[:edm_type] = usermap.edm_type
          if usermap.castfunc
            @casted_cols[col] = usermap.castfunc
            next # this will override our rules below !
          end
        end

        # attribute specific type mapping
        if (colmap = @type_mappings[col])
          metadata[:edm_type] = colmap.edm_type
          if colmap.castfunc
            @casted_cols[col] = colmap.castfunc
            next # this will override our rules below !
          end
        end

        if metadata[:edm_precision] && (metadata[:edm_type] =~ /\AEdm.Decimal\(/i)
          # we save the precision and/or scale in the lambda (binding!)

          @casted_cols[col] = if metadata[:edm_scale]
                                lambda { |x|
                                  # not sure if these copies are really needed, but feels better that way
                                  # output  decimal with precision and scale
                                  x&.toDecimalPrecisionScaleString(metadata[:edm_precision], metadata[:edm_scale])
                                }
                              else
                                lambda { |x|
                                  # not sure if these copies are really needed, but feels better that way
                                  # output  decimal with precision only
                                  x&.toDecimalPrecisionString(metadata[:edm_precision])
                                }
                              end

          next
        end
        if metadata[:edm_type] == 'Edm.Decimal'
          @casted_cols[col] = ->(x) { x&.toDecimalString }
          next
        end
        # Odata V2 Spec:
        # Edm.Binary	Base64 encoded value of an EDM.Binary value represented as a JSON string
        # See for example https://services.odata.org/V2/Northwind/Northwind.svc/Categories(1)?$format=json
        if metadata[:edm_type] == 'Edm.Binary'
          @casted_cols[col] = ->(x) { Base64.encode64(x) unless x.nil? } # Base64
          next
        end
        # Odata V2 Spec:
        # 	Literal form of Edm.Guid as used in URIs formatted as a JSON string
        if metadata[:edm_type] == 'Edm.Guid'

          if props[:type] == :blob # Edm.Guid but as 16 byte binary Blob on DB level, eg in Sqlite
            @casted_cols[col] = lambda { |x|
              UUIDTools::UUID.parse_raw(x).to_s unless x.nil?
            } # Base64
            next
          end
        end
        # TODO: check this more in details
        # NOTE: here we use :type which is the sequel defined ruby-type
        if props[:type] == :datetime || props[:type] == :date
          #          @casted_cols[col] = ->(x) { x&.iso8601 }
          @casted_cols[col] = ->(x) { x&.to_edm_json }
        end
      end # db_schema.each do |col, props|

      # check if key needs casting. Important for later entity-uri generation !
      return unless primary_key.is_a? Symbol # single key field

      # guid key as guid'xxx-yyy-zzz'
      metadata = @cols_metadata[primary_key]

      if metadata[:edm_type] == 'Edm.Guid'
        props = db_schema[primary_key]
        @pk_castfunc = if props[:type] == :blob # Edm.Guid but as 16 byte binary Blob on DB level, eg in Sqlite
                         lambda { |x| "guid'#{UUIDTools::UUID.parse_raw(x)}'" unless x.nil? }
                       else
                         lambda { |x| "guid'#{x}'" unless x.nil? }
                       end
      else
        @pk_castfunc = @casted_cols[primary_key]
      end
    end # build_casted_cols(service)

    def finalize_publishing(service)
      build_type_name

      # build default output template structure
      build_default_template

      # add edm_types into metadata store

      db_schema.each do |col, props|
        metadata = @cols_metadata.key?(col) ? @cols_metadata[col] : (@cols_metadata[col] = {})
        Safrano.add_edm_types(metadata, props)
      end

      build_casted_cols(service)

      # build pk regexps
      prepare_pk

      # and finally build the path lists and allowed tr's
      build_attribute_path_list
      build_expand_path_list
      build_all_props_list

      build_allowed_transitions
      build_entity_allowed_transitions

      # for media
      finalize_media if respond_to? :finalize_media
    end

    KEYPRED_URL_REGEXP = /\A\(\s*((?:guid)?'?[\w=,\-'\s]+'?)\s*\)(.*)/.freeze
    GUIDRGX = /guid'([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})'/i.freeze
    def prepare_pk
      if primary_key.is_a? Array
        @pk_names = []
        @pk_cast_from_string = {}
        odata_upk_build = []
        primary_key.each do |pk|
          @pk_names << pk.to_s
          kvpredicate = case db_schema[pk][:type]
                        when :integer
                          @pk_cast_from_string[pk] = ->(str) { Integer(str) }
                          '?'
                        else
                          "'?'"
                        end
          odata_upk_build << "#{pk}=#{kvpredicate}"
        end
        @odata_upk_parts = odata_upk_build.join(',').split('?')

        # regex parts for unordered matching
        @iuk_rgx_parts = primary_key.map do |pk|
          kvpredicate = case db_schema[pk][:type]
                        when :integer
                          '(\\d+)'
                        else
                          "'(\\w+)'"
                        end
          [pk, "#{pk}=#{kvpredicate}"]
        end.to_h

        # single regex assuming the key fields are ordered !
        @iuk_rgx = /\A#{@iuk_rgx_parts.values.join(',\s*')}\z/

        @iuk_rgx_parts.transform_values! { |v| /\A#{v}\z/ }

      else
        @pk_names = [primary_key.to_s]
        @pk_cast_from_string = nil

        kvpredicate = case db_schema[primary_key][:type]
                      when :integer
                        # TODO: Harmonize this with primitive_types.rb  convert_from_url
                        @pk_cast_from_string = ->(str) { Integer(str) }
                        /(\d+)/.freeze
                      else
                        metadata = @cols_metadata[primary_key]
                        case metadata[:edm_type]
                        when 'Edm.Guid'
                          if db_schema[primary_key][:type] == :blob # Edm.Guid but as 16byte binary Blob on DB
                            @pk_cast_from_string = lambda { |str|
                              Sequel::SQL::Blob.new(UUIDTools::UUID.parse(str).raw)
                            }
                          end
                          GUIDRGX
                        else
                          /'(\w+)'/.freeze
                        end
                      end
        @iuk_rgx = /\A\s*#{kvpredicate}\s*\z/
      end
      # @entity_id_url_regexp =   /\A\(\s*#{kvpredicate}\s*\)(.*)/.freeze
      @entity_id_url_regexp = KEYPRED_URL_REGEXP
    end

    def prepare_fields
      # columns as strings
      @columns_str = @columns.map(&:to_s)

      @data_fields = db_schema.map do |col, cattr|
        cattr[:primary_key] ? nil : col
      end.select { |col| col }
    end

    def invalid_hash_data?(data)
      data.keys.map(&:to_sym).find { |ksym| !(@columns.include? ksym) }
    end

    ## A regexp matching all allowed attributes of the Entity
    ## (eg ID|name|size etc... ) at start position and returning the rest
    def transition_attribute_regexp
      #      db_schema.map { |sch| sch[0] }.join('|')
      # @columns is from Sequel Model
      %r{\A/(#{@columns.join('|')})(.*)\z}
    end

    # super-minimal type check, but better as nothing
    def cast_odata_val(val, pk_cast)
      pk_cast ? Contract.valid(pk_cast.call(val)) : Contract.valid(val) # no cast needed, eg for string
    rescue StandardError => e
      RubyStandardErrorException.new(e)
    end

    CREATE_AND_SAVE_ENTY_AND_REL = lambda do |new_entity, assoc, parent|
      # in-changeset requests get their own transaction
      case assoc[:type]
      when :one_to_many, :one_to_one
        Safrano.create_nav_relation(new_entity, assoc, parent)
        new_entity.save(transaction: false)
      when :many_to_one
        new_entity.save(transaction: false)
        Safrano.create_nav_relation(new_entity, assoc, parent)
        parent.save(transaction: false)
        # else # not supported
      end
    end
    def odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
      if req.in_changeset
        # in-changeset requests get their own transaction
        CREATE_AND_SAVE_ENTY_AND_REL.call(new_entity, assoc, parent)
      else
        db.transaction do
          CREATE_AND_SAVE_ENTY_AND_REL.call(new_entity, assoc, parent)
        end
      end
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def allowed_transitions
        @allowed_transitions
      end

      include Safrano::Transitions::GetNextTrans::BySimpleDetect

      def entity_allowed_transitions
        @entity_allowed_transitions
      end

      def build_allowed_transitions
        @allowed_transitions = [Safrano::TransitionEnd,
                                Safrano::TransitionCount,
                                Safrano::Transition.new(entity_id_url_regexp,
                                                        trans: :transition_id)].freeze
      end

      def build_entity_allowed_transitions
        @entity_allowed_transitions = [
          Safrano::TransitionEnd,
          Safrano::TransitionCount,
          Safrano::TransitionLinks,
          Safrano::TransitionValue,
          Safrano::Transition.new(transition_attribute_regexp, trans: :transition_attribute)
        ]
        if (ncurgx = @nav_collection_url_regexp)
          @entity_allowed_transitions <<
            Safrano::Transition.new(%r{\A/(#{ncurgx})(.*)\z}, trans: :transition_nav_collection)
        end
        if (neurgx = @nav_entity_url_regexp)
          @entity_allowed_transitions <<
            Safrano::Transition.new(%r{\A/(#{neurgx})(.*)\z}, trans: :transition_nav_entity)
        end
        @entity_allowed_transitions << Safrano::Transition.new(%r{\A/(\w+)(.*)\z}, trans: :transition_invalid_attribute)
        @entity_allowed_transitions.freeze
        @entity_allowed_transitions
      end
    end
    include Transitions
  end

  # special handling for composite key
  module EntityClassMultiPK
    include EntityClassBase
    def pk_lookup_expr(ids)
      primary_key.zip(ids)
    end

    # input  fx='aas',fy_w='0001'
    # output true, ['aas', '0001'] ... or false when typ-error
    def parse_odata_key(mid)
      # @iuk_rgx is (needs to be) built on start with
      # collklass.prepare_pk

      # first try to match single regex assuming orderd key fields
      if (md = @iuk_rgx.match(mid))
        md = md.captures
        mdc = []
        primary_key.each_with_index do |pk, i|
          mdc << if (pk_cast = @pk_cast_from_string[pk])
                   pk_cast.call(md[i])
                 else
                   md[i] # no cast needed, eg for string
                 end
        end

      else

        # order key fields didnt match--> try and collect/check each parts unordered
        scan_rgx_parts = @iuk_rgx_parts.dup
        mdch = {}

        mid.split(/\s*,\s*/).each do |midpart|
          mval = nil
          mpk, _mrgx = scan_rgx_parts.find do |_pk, rgx|
            if (md = rgx.match(midpart))
              mval = md[1]
            end
          end
          return Contract::NOK unless mpk && mval

          mdch[mpk] = if (pk_cast = @pk_cast_from_string[mpk])
                        pk_cast.call(mval)
                      else
                        mval # no cast needed, eg for string
                      end
          scan_rgx_parts.delete(mpk)
        end
        # normally arriving here we have mdch filled with key values pairs,
        # but not in the model key ordering. lets just re-order the values
        mdc = @iuk_rgx_parts.keys.map { |pk| mdch[pk] }

      end
      Contract.valid(mdc)
      # catch remaining convertion errors that we failed to prevent
    rescue StandardError => e
      RubyStandardErrorException.new(e)
    end
  end

  # special handling for single key
  module EntityClassSinglePK
    include EntityClassBase

    def parse_odata_key(rawid)
      if (md = @iuk_rgx.match(rawid))
        if @pk_cast_from_string
          Contract.valid(@pk_cast_from_string.call(md[1]))
        else
          Contract.valid(md[1]) # no cast needed, eg for string
        end
      else
        Contract::NOK
      end
    rescue StandardError => e
      RubyStandardErrorException.new(e)
    end

    def pk_lookup_expr(id)
      { primary_key => id }
    end
  end

  # normal handling for non-media entity
  module EntityClassNonMedia
    # POST for non-media entity collection -->
    # 1. Create and add entity from payload
    # 2. Create relationship if needed
    def odata_create_entity_and_relation(req, assoc = nil, parent = nil)
      # TODO: this is for v2 only...
      req.with_parsed_data(self) do |data|
        data.delete('__metadata')

        # validate payload column names
        if (invalid = invalid_hash_data?(data))
          ::Safrano::Request::ON_CGST_ERROR.call(req)
          return [422, EMPTY_HASH, ['Invalid attribute name: ', invalid.to_s]]
        end

        if req.accept?(APPJSON)
          new_entity = new_from_hson_h(data)
          if parent
            odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
          else
            # in-changeset requests get their own transaction
            new_entity.save(transaction: !req.in_changeset)
          end
          req.register_content_id_ref(new_entity)
          new_entity.copy_request_infos(req)
          # json is default content type so we dont need to specify it here again
          # TODO quirks array mode !
          # [201, EMPTY_HASH, new_entity.to_odata_post_json(service: req.service)]
          [201, { Safrano::LOCATION => new_entity.uri },
           new_entity.to_odata_create_json(request: req)]
        else # TODO: other formats
          415
        end
      end
    end
  end
end
