# frozen_string_literal: true

require 'json'
require 'rexml/document'
require 'safrano'

module Safrano
  # handle navigation in the Datamodel tree of entities/attributes
  # input is the url path. Url parameters ($filter etc...) are NOT handled here
  # This uses a state transition algorithm
  class Walker
    attr_accessor :contexts
    attr_accessor :context
    attr_accessor :end_context
    attr_reader   :path_start
    attr_accessor :path_remain
    attr_accessor :path_done
    attr_accessor :status
    attr_accessor :error

    # is $count requested?
    attr_accessor :do_count

    # is $value (of attribute) requested?
    attr_reader :raw_value

    # is $value (of media entity) requested?
    attr_reader :media_value

    # are $links requested ?
    attr_reader :do_links

    attr_reader :request

    NIL_SERVICE_FATAL = 'Walker is called with a nil service'
    EMPTYSTR = ''
    SLASH = '/'

    def initialize(service, path, request, content_id_refs = nil)
      raise NIL_SERVICE_FATAL unless service

      path = URI.decode_www_form_component(path)
      @context = service
      @content_id_refs = content_id_refs

      # needed because for function import we need access to the url parameters (req.params)
      # who contains the functions params
      @request = request

      @contexts = [@context]

      @path_start = @path_remain = if service
                                     unprefixed(service.xpath_prefix, path)
                                   else # This is for batch function
                                     path
                                   end
      @path_done = String.new
      @status = :start
      @end_context = nil
      @do_count = nil
      eo
    end

    def unprefixed(prefix, path)
      if (prefix == EMPTYSTR) || (prefix == SLASH)
        path
      else
        path.sub(/\A#{prefix}/, EMPTYSTR)
      end
    end

    # perform a content-id ($batch changeset ref) transition
    def do_run_with_content_id
      if @content_id_refs.is_a? Hash
        if (@context = @content_id_refs[@context.to_s])
          @status = :run
        else
          @context = nil
          @status = :error
          # TODO: more appropriate error handling
          @error = Safrano::ErrorNotFound
        end
      else
        @context = nil
        @status = :error
        # TODO: more appropriate error handling
        @error = Safrano::ErrorNotFound
      end
    end

    # execute function import with request parameters
    # input: @context containt the function to exectute,
    #        @request.params should normally contain the params
    # result: validate the params for the given function, execute the function and
    #          return it's result back into @context,
    #          and finaly set status :end   (or error if anyting went wrong )
    def do_run_with_execute_func
      @context, @status, @error = @context.do_execute_func(@request)
    end

    # little hacks... depending on returned state, set some attributes
    def state_mappings
      case @status
      when :end_with_count
        @do_count = true
        @status = :end
      when :end_with_value
        @raw_value = true
        @status = :end
      when :end_with_media_value
        @media_value = true
        @status = :end
      when :run_with_links
        @do_links = true
        @status = :run
      end
    end

    def do_next_transition
      @context, @status, @error = @tres_next.do_transition(@context)
      # little hack's
      case @status
        # we dont have the content-id references data on service level
        # but we have it here, so in case of a $content-id transition
        # the returned context is just the id, and we get the final result
        # entity reference here and place it in @context
      when :run_with_content_id
        do_run_with_content_id
      when :run_with_execute_func
        do_run_with_execute_func
      end

      @contexts << @context
      @path_remain = @tres_next.path_remain
      @path_done << @tres_next.path_done

      # little hack's
      state_mappings
    end

    def eo
      while @context
        @tres_next = @context.get_next_transresult(@path_remain)
        if @tres_next
          do_next_transition
        else
          @context = nil
          @status = :error
          @error = Safrano::ErrorNotFoundSegment.new(@path_remain)
        end
      end
      # TODO: shouldnt we raise an error here if @status != :end ?
      return false unless @status == :end

      @end_context = @contexts.size >= 2 ? @contexts[-2] : @contexts[1]
    end

    def finalize
      @status == :end ? Contract.valid(@end_context) : @error
    end
  end
end
