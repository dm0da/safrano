# frozen_string_literal: true

require_relative './tree'
require_relative './sequel'

module Safrano
  module Filter
    # sqlite adapter specific function handler
    module FuncTreeSqlite
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # sqlite uses instr()
        Contract.collect_result!(args[1].leuqes(jh),
                                 args[0].leuqes(jh)) do |l1, l0|
          substr_func = Sequel.function(:instr, l1, l0)
          Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
        end
      end
      # %d		day of month: 00
      # %f		fractional seconds: SS.SSS
      # %H		hour: 00-24
      # %j		day of year: 001-366
      # %J		Julian day number
      # %m		month: 01-12
      # %M		minute: 00-59
      # %s		seconds since 1970-01-01
      # %S		seconds: 00-59
      # %w		day of week 0-6 with Sunday==0
      # %W		week of year: 00-53
      # %Y		year: 0000-9999
      # %%		%

      # sqlite does not have extract but recommends to use strftime
      def year(lq)
        Sequel.function(:strftime, '%Y', lq).cast(:integer)
      end

      def month(lq)
        Sequel.function(:strftime, '%m', lq).cast(:integer)
      end

      def second(lq)
        Sequel.function(:strftime, '%S', lq).cast(:integer)
      end

      def minute(lq)
        Sequel.function(:strftime, '%M', lq).cast(:integer)
      end

      def hour(lq)
        Sequel.function(:strftime, '%H', lq).cast(:integer)
      end

      def day(lq)
        Sequel.function(:strftime, '%d', lq).cast(:integer)
      end

      def floor(_lq)
        Safrano::FilterFunctionNotImplementedError.new("$filter function 'floor' is not implemented in sqlite adapter")
      end

      def ceiling(_lq)
        Safrano::FilterFunctionNotImplementedError.new("$filter function 'ceiling' is not implemented in sqlite adapter")
      end
    end

    # re-useable module with math floor/ceil functions for those adapters having these SQL funcs
    module MathFloorCeilFuncTree
      def floor(lq)
        success Sequel.function(:floor, lq)
      end

      def ceiling(lq)
        success Sequel.function(:ceil, lq)
      end
    end

    # re-useable module with Datetime functions with extract()
    module DateTimeFuncTreeExtract
      def year(lq)
        lq.extract(:year)
      end

      def month(lq)
        lq.extract(:month)
      end

      def second(lq)
        lq.extract(:second)
      end

      def minute(lq)
        lq.extract(:minute)
      end

      def hour(lq)
        lq.extract(:hour)
      end

      def day(lq)
        lq.extract(:day)
      end
    end

    # postgresql adapter specific function handler
    module FuncTreePostgres
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # postgres does not know instr() but has strpos
        Contract.collect_result!(args[1].leuqes(jh),
                                 args[0].leuqes(jh)) do |l1, l0|
          substr_func = Sequel.function(:strpos, l1, l0)
          Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
        end
      end

      # postgres uses extract()
      include DateTimeFuncTreeExtract

      # postgres has floor/ceil funcs
      include MathFloorCeilFuncTree
    end

    # default adapter  function handler for all others... try to use the most common version
    # :substring --> instr because here is seems Postgres is special
    # datetime funcs --> exctract, here sqlite is special(uses format)
    # note: we dont test this, provided as an example/template, might work eg for mysql
    module FuncTreeDefault
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # instr() seems to be the most common substring func
        Contract.collect_result!(args[1].leuqes(jh),
                                 args[0].leuqes(jh)) do |l1, l0|
          substr_func = Sequel.function(:instr, l1, l0)
          Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
        end
      end

      # XYZ uses extract() ?
      include DateTimeFuncTreeExtract

      # ... assume SQL
      include MathFloorCeilFuncTree
    end
  end
end
