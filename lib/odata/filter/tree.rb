# frozen_string_literal: true

require_relative './base'
require_relative './error'

module Safrano
  module Filter
    # Base class for Leaves, Trees, RootTrees etc
    class Node
      attr_reader :value

      def initialize(val, &block)
        @value = val
        instance_eval(&block) if block_given?
      end

      def ==(other)
        @value == other.value
      end
    end

    # Leaves are Nodes with a parent but no children
    class Leave
      attr_accessor :parent

      # nil is considered as accepted, otherwise non-nil=the error
      def accept?(tok, typ)
        Parser::ErrorInvalidToken(tok, typ)
      end

      def check_types; end

      def attach(_child)
        Safrano::Filter::Parser::ErrorLeaveChild
      end
    end

    # RootTrees have childrens but no parent
    class RootTree
      attr_reader :children
      attr_accessor :state

      def initialize(val: :root, &block)
        @children = []
        super(val, &block)
      end

      # shortcut used for testing
      def first_child_value
        @children.first.value
      end

      def attach(child)
        child.parent = self
        @children << child
        Contract::OK
      end

      def detach(child)
        child.parent = nil
        @children.delete(child)
      end

      def ==(other)
        super(other) && (@children == other.children)
      end

      def update_state(tok, typ) end

      # nil is considered as accepted, otherwise non-nil=the error
      def accept?(tok, typ)
        case typ
        when :Literal, :NullLiteral, :Qualit, :QString, :FuncTree, :ArgTree,
             :UnopTree, :FPNumber, :DecimalLit, :DateTimeLit, :DateTimeOffsetLit, :GuidLit
          nil
        when :Delimiter
          if tok == '('
            nil
          else
            Parser::ErrorInvalidToken.new(tok, typ, self)
          end
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def check_types
        err = nil
        @children.find { |c| (err = c.check_types) }
        err
      end
    end

    # Tree's have Parent and children
    class Tree
      attr_accessor :parent

      def initialize(val)
        super(val: val)
      end
    end

    # For functions... should have a single child---> the argument list
    class FuncTree
      def initialize(val)
        super(val.downcase.to_sym)
      end

      def args
        @children.first.children
      end

      def arity_full?(cursize)
        cursize >= max_arity
      end

      def max_arity
        case @value
        when :replace
          3
        when :concat, :substringof, :substring, :endswith, :startswith
          2
        else
          1
        end
      end

      def edm_type
        case @value
        when :concat, :substring
          :string
        when :length
          :int
        when :substringof, :endswith, :startswith
          :bool
        else
          :any
        end
      end

      # nil is considered as accepted, otherwise non-nil=the error
      def accept?(tok, typ)
        case typ
        when :BinopBool, :BinopArithm
          nil
        else
          super(tok, typ)
        end
      end

      def check_types
        case @value
        when :length
          argtyp = args.first.edm_type
          if (argtyp != :any) && (argtyp != :string)
            return Parser::ErrorInvalidArgumentType.new(self,
                                                        expected: :string,
                                                        actual: argtyp)
          end
        end
        super
      end
    end

    # Indentity Func to use as "parent" func of parenthesis expressions
    # --> allow to handle generically parenthesis always as argument of
    # some function
    class IdentityFuncTree
      def initialize
        super(:__indentity)
      end

      # we can have parenthesis with one expression inside everywhere
      # only in FuncTree this is redefined for the function's arity
      # Note: if you change this method, please also update arity_full_monkey?
      #       see below
      def arity_full?(cursize)
        cursize >= 1
      end

      # this is for testing only.
      # see 99_threadsafe_tc.rb
      # there we will monkey patch  arity_full? by adding some sleeping
      # to easily slow down a given test-thread (while the other one runs normaly)
      #
      # The rule is to keep this method here exactly same as the original
      #   "productive" one
      #
      # With this trick we can test  threadsafeness without touching
      # "productive" code
      def arity_full_monkey?(cursize)
        cursize >= 1
      end

      def edm_type
        @children.first.edm_type
      end

      def ==(other)
        @children == other.children
      end
    end

    # unary op eg. NOT
    class UnopTree
      def initialize(val)
        super(val.downcase.to_sym)
      end

      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :not
          7
        else
          999
        end
      end

      def edm_type
        case @value
        when :not
          :bool
        else
          :any
        end
      end
    end

    # Bin ops
    class BinopTree
      def initialize(val)
        @state = :open
        super(val.downcase.to_sym)
      end

      def update_state(_tok, typ)
        case typ
        when :Literal, :NullLiteral, :Qualit, :QString, :FuncTree, :BinopBool, :BinopArithm,
            :UnopTree, :FPNumber, :DecimalLit, :DateTimeLit, :DateTimeOffsetLit, :GuidLit
          @state = :closed
        end
      end
    end

    class BinopBool
      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :or
          1
        when :and
          2
        when :eq, :ne
          3
        when :gt, :ge, :lt, :le, :isof
          4
        else
          999
        end
      end

      def edm_type
        :bool
      end
    end

    class BinopArithm
      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :add, :sub
          5
        when :mul, :div, :mod
          6
        else
          999
        end
      end

      # TODO: different num types?
      def edm_type
        :any
      end
    end

    # Arguments or lists
    class ArgTree
      attr_reader :type

      def initialize(val)
        @type = :expression
        @state = :open
        super
      end

      def update_state(_tok, typ)
        case typ
        when :Delimiter
          @state = :closed
        when :Separator
          @state =  :sep
        when :Literal, :NullLiteral, :Qualit, :QString, :FuncTree, :FPNumber, :DecimalLit,
             :DateTimeLit, :DateTimeOffsetLit, :GuidLit
          @state =  :val
        end
      end

      # nil is considered as accepted, otherwise non-nil=the error
      def accept?(tok, typ)
        case typ
        when :Delimiter
          if @value == '(' && tok == ')' && @state != :closed
            if (@parent.class == IdentityFuncTree) ||
               @parent.arity_full?(@children.size)

              nil
            else
              Parser::ErrorInvalidArity.new(tok, typ, self)
            end
          else
            if @value == '(' && tok == '(' && @state == :open
              nil
            else
              Parser::ErrorUnmatchedClose.new(tok, typ, self)
            end
          end
        when :Separator
          if @value == '(' && tok == ',' && @state == :val
            nil
          elsif @state == :sep
            Parser::ErrorInvalidToken.new(tok, typ, self)
          end
        when :Literal, :NullLiteral, :Qualit, :QString, :FuncTree, :FPNumber, :DecimalLit,
             :DateTimeLit, :DateTimeOffsetLit, :GuidLit
          if (@state == :open) || (@state == :sep)
            Parser::ErrorInvalidArity.new(tok, typ, self) if @parent.arity_full?(@children.size)
          else
            Parser::ErrorInvalidToken.new(tok, typ, self)
          end
        when :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def ==(other)
        super(other) && @type == other.type && @state == other.state
      end
    end

    # Numbers (floating point, ints, dec)
    class FPNumber
      # def initialize(val)
      # 1.53f --> value 1.53
      # 1.53d --> value 1.53
      # 1.53 --> value 1.53
      # Note: the tokenizer has already dropped the not usefull string parts
      # Note : we dont differentiate between Float and Double here
      # super(val)
      # end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      # TODO
      def edm_type
        :number
      end
    end

    # Edm guid
    class Guid16
      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :guid
      end
    end

    class DecimalLit
      # def initialize(val)
      # 1.53m --> value 1.53
      # Warning, this assumes that the m|M part in the input is really not optional
      # Note: the tokenizer has already dropped the not usefull string parts
      # cf. DECIMALRGX in token.rb
      # super(val)
      # end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :decimal
      end
    end

    # Literals are unquoted words without /
    class Literal
      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :any
      end

      # error, Literal are leaves
      # when the child is a IdentityFuncTree then this looks like
      # an attempt to use a unknown function, eg. ceil(Total)
      # instead of ceiling(Total)
      def attach(child)
        if child.is_a? Safrano::Filter::IdentityFuncTree
          Safrano::FilterUnknownFunctionError.new(value)
        else
          super
        end
      end
    end

    # Qualit (qualified lits) are words separated by /
    # path/path/path/attrib
    class Qualit
      REGEXP = /((?:\w+\/)+)(\w+)/.freeze
      attr_reader :path
      attr_reader :attrib

      def initialize(val)
        super(val)
        # split into path + attrib
        raise Parser::Error.new(self, Qualit) unless (md = REGEXP.match(val))

        @path = md[1].chomp('/')
        @attrib = md[2]
      end
    end

    # DateTimeLit
    class DateTimeLit
      # def initialize(val)
      # datetime'2000-12-12T12:00:53' --> value 2000-12-12T12:00:53
      # Note: the tokenizer has already dropped the not usefull string parts
      # super(val)
      # end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :datetime
      end
    end

    # DateTimeOffsetLit
    class DateTimeOffsetLit
      # def initialize(val)
      # datetimeoffset'2000-12-12T12:00:53+02:00' --> value 2000-12-12T12:00:53+02:00
      # Note: the tokenizer has already dropped the not usefull string parts
      # super(val)
      # end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :datetimeoffset
      end
    end

    # Quoted Strings
    class QString
      DBL_QO = "''"
      SI_QO = "'"
      def initialize(val)
        # unescape double quotes
        super(val.gsub(DBL_QO, SI_QO))
      end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          nil
        else
          Parser::ErrorInvalidToken.new(tok, typ, self)
        end
      end

      def edm_type
        :string
      end
    end
  end
end
