# frozen_string_literal: true

require_relative './base'
require_relative './sequel_function_adapter'
require_relative './sequel_datetime_adapter'
module Safrano
  module Filter
    # Base class for Leaves, Trees, RootTrees etc
    #    class Node
    #    end

    # Leaves are Nodes with a parent but no children
    #    class Leave < Node
    #    end

    # RootTrees have childrens but no parent
    class RootTree
      def apply_to_dataset(dtcx, jh)
        @children.first.leuqes(jh).if_valid do |filtexpr|
          jh.dataset(dtcx).where(filtexpr).select_all(jh.start_model.table_name)
        end
      end

      def sequel_expr(jh)
        @children.first.leuqes(jh)
      end
    end

    # Tree's have Parent and children
    class Tree < RootTree
    end

    # For functions... should have a single child---> the argument list
    # note: Adapter specific function helpers like year() or substringof_sig2()
    # need to be mixed in on startup (eg. on publish finalize)
    class FuncTree < Tree
      def leuqes(jh)
        case @value
        when :startswith
          Contract.collect_result!(args[0].leuqes(jh),
                                   args[1].leuqes_starts_like(jh)) do |l0, l1|
            Sequel.like(l0, l1)
          end

        when :endswith
          Contract.collect_result!(args[0].leuqes(jh),
                                   args[1].leuqes_ends_like(jh)) do |l0, l1|
            Sequel.like(l0, l1)
          end

        when :substringof

          # there are multiple possible argument types (but all should return edm.string)

          if args[0].is_a?(QString) ||
             # substringof('Rhum', name)  -->
             # name contains substr 'Rhum'

             # special non standard (ui5 client) case ?
             (args[0].is_a?(Literal) && args[1].is_a?(Literal))
            Contract.collect_result!(args[1].leuqes(jh),
                                     args[0].leuqes_substringof_sig1(jh)) do |l1, l0|
              Sequel.like(l1, l0)
            end
          elsif args[1].is_a?(QString)
            substringof_sig2(jh) # adapter specific
          else
            # TODO... actually not supported?
            raise Safrano::Filter::Parser::ErrorFunctionArgumentType
          end
        when :concat
          Contract.collect_result!(args[0].leuqes(jh),
                                   args[1].leuqes(jh)) do |l0, l1|
            Sequel.join([l0, l1])
          end

        when :length
          args.first.leuqes(jh)
              .map_result! { |l| Sequel.char_length(l) }

        when :trim
          args.first.leuqes(jh)
              .map_result! { |l| Sequel.trim(l) }

        when :toupper
          args.first.leuqes(jh)
              .map_result! { |l| Sequel.function(:upper, l)  }

        when :tolower
          args.first.leuqes(jh)
              .map_result! { |l| Sequel.function(:lower, l)  }

        # all datetime funcs are adapter specific (because sqlite does not have extract)
        when :year
          args.first.leuqes(jh)
              .map_result! { |l| year(l) }

        when :month
          args.first.leuqes(jh)
              .map_result! { |l| month(l) }

        when :second
          args.first.leuqes(jh)
              .map_result! { |l| second(l) }

        when :minute
          args.first.leuqes(jh)
              .map_result! { |l| minute(l) }

        when :hour
          args.first.leuqes(jh)
              .map_result! { |l| hour(l) }

        when :day
          args.first.leuqes(jh)
              .map_result! { |l| day(l) }

        # math functions
        when :round
          args.first.leuqes(jh)
              .map_result! { |l| Sequel.function(:round, l) }

        when :floor
          args.first.leuqes(jh)
              .if_valid { |l| floor(l) }

        when :ceiling
          args.first.leuqes(jh)
              .if_valid { |l| ceiling(l) }
        else
          Safrano::FilterParseError
        end
      end
    end

    # Indentity Func to use as "parent" func of parenthesis expressions
    # --> allow to handle generically parenthesis always as argument of
    # some function
    class IdentityFuncTree < FuncTree
      def leuqes(jh)
        args.first.leuqes(jh)
      end
    end

    # unary op eg. NOT
    class UnopTree < Tree
      def leuqes(jh)
        case @value
        when :not
          @children.first.leuqes(jh).map_result! { |l| Sequel.~(l) }
        else
          Safrano::FilterParseError
        end
      end
    end

    # logical Bin ops
    class BinopBool
      def leuqes(jh)
        leuqes_op = case @value
                    when :eq
                      :'='
                    when :ne
                      :'!='
                    when :le
                      :<=
                    when :ge
                      :'>='
                    when :lt
                      :<
                    when :gt
                      :>
                    when :or
                      :OR
                    when :and
                      :AND
                    else
                      return Safrano::FilterParseError
                    end
        Contract.collect_result!(@children[0].leuqes(jh),
                                 @children[1].leuqes(jh)) do |c0, c1|
          if c1 == NullLiteral::LEUQES
            case @value
            when :eq
              leuqes_op = :IS
            when :ne
              leuqes_op = :'IS NOT'
            end
          end
          Sequel::SQL::BooleanExpression.new(leuqes_op, c0, c1)
        end
      end
    end

    # Arithmetic Bin ops
    class BinopArithm
      def leuqes(jh)
        leuqes_op = case @value
                    when :add
                      :+
                    when :sub
                      :-
                    when :mul
                      :*
                    when :div
                      :/
                    when :mod
                      :%
                    else
                      return Safrano::FilterParseError
                    end
        Contract.collect_result!(@children[0].leuqes(jh),
                                 @children[1].leuqes(jh)) do |c0, c1|
          Sequel::SQL::NumericExpression.new(leuqes_op, c0, c1)
        end
      end
    end

    # Arguments or lists
    class ArgTree
    end

    # Numbers (floating point, ints, dec)
    class FPNumber
      def leuqes(_jh)
        success Sequel.lit(@value)
      end

      def leuqes_starts_like(_jh)
        success  "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        success  "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        success  "%#{@value}%"
      end
    end

    class DecimalLit
      def leuqes(_jh)
        success Sequel.lit(@value)
      end
    end

    # Literals are unquoted words
    class Literal
      def leuqes(jh)
        return Safrano::FilterParseErrorWrongColumnName unless jh.start_model.db_schema.key?(@value.to_sym)

        success Sequel[jh.start_model.table_name][@value.to_sym]
      end

      # non stantard extensions to support things like
      # substringof(Rhum, name)  ????
      def leuqes_starts_like(_jh)
        success "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        success "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        success "%#{@value}%"
      end

      def as_string
        @value
      end
    end

    # Null
    class NullLiteral
      def leuqes(_jh)
        # Sequel's representation of NULL
        success LEUQES
      end
    end

    # Qualit (qualified lits) are words separated by /
    class Qualit
      def leuqes(jh)
        jh.add(@path)
        talias = jh.start_model.get_alias_sym(@path)
        success Sequel[talias][@attrib.to_sym]
      end
    end

    # Quoted Strings
    class QString
      def leuqes(_jh)
        success @value
      end

      def leuqes_starts_like(_jh)
        success "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        success "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        success "%#{@value}%"
      end
    end

    # DateTime literals  datetime'2017-04-15T00:00:00'
    class DateTimeLit
      # datetime method is defined dynamically by adapter-specific include on startup
      # --> sequel_datetime_adapter.rb
      def leuqes(_jh)
        # success Sequel.function(:datetime, @value)
        success datetime(@value)
      end
    end

    # DateTimeOffset literals  datetimeoffset'2017-04-15T00:00:00+02:00'
    class DateTimeOffsetLit
      # datetime method is defined dynamically by adapter-specific include on startup
      # --> sequel_datetime_adapter.rb
      def leuqes(_jh)
        # success Sequel.function(:datetime, @value)
        success datetime(@value)
      end
    end
  end
end
