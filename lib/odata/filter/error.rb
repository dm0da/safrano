# frozen_string_literal: true

require_relative '../error'

module Safrano
  class SequelAdapterError < StandardError
    attr_reader :inner

    def initialize(err)
      @inner = err
    end
  end

  module Filter
    class Parser
      # Parser errors

      class Error
        def self.http_code
          const_get(:HTTP_CODE)
        end
        HTTP_CODE = 400

        attr_reader :tok
        attr_reader :typ
        attr_reader :cur_val
        attr_reader :cur_typ

        def initialize(tok, typ, cur)
          @tok = tok
          @typ = typ
          return unless cur

          @cur_val = cur.value
          @cur_typ = cur.class
        end
      end

      # Invalid Tokens
      class ErrorInvalidToken < Error
        include ::Safrano::ErrorInstance
        def initialize(tok, typ, cur)
          super
          @msg = "Bad Request: invalid token #{tok} in $filter"
        end
      end

      # Unmached closed
      class ErrorUnmatchedClose < Error
        include ::Safrano::ErrorInstance
        def initialize(tok, typ, cur)
          super
          @msg = "Bad Request: unmatched #{tok} in $filter"
        end
      end

      class ErrorFunctionArgumentType
        include ::Safrano::ErrorInstance
      end

      class ErrorWrongColumnName
        include ::Safrano::ErrorInstance
      end

      # attempt to add a child to a Leave
      class ErrorLeaveChild
        include ::Safrano::ErrorInstance
      end

      # Invalid function arity
      class ErrorInvalidArity < Error
        include ::Safrano::ErrorInstance
        def initialize(tok, typ, cur)
          super
          @msg = "Bad Request: wrong number of parameters for function #{cur.parent.value} in $filter"
        end
      end

      # Invalid separator in this context (missing parenthesis?)
      class ErrorInvalidSeparator < Error
        include ::Safrano::ErrorInstance
      end

      # unmatched quot3
      class UnmatchedQuoteError < Error
        include ::Safrano::ErrorInstance
        def initialize(tok, typ, cur)
          super
          @msg = "Bad Request: unbalanced quotes #{tok} in $filter"
        end
      end

      # wrong type of function argument
      class ErrorInvalidArgumentType < Error
        include ::Safrano::ErrorInstance

        def initialize(tree, expected:, actual:)
          @tree = tree
          @expected = expected
          @actual = actual
        end
      end
    end
  end
end
