# frozen_string_literal: true

require_relative './tree'
require_relative './sequel'

module Safrano
  module Filter
    # sqlite adapter specific DateTime handler
    module DateTimeSqlite
      def datetime(_val)
        Sequel.function(:datetime, @value)
      end
    end

    # non-sqlite adapter specific DateTime handler
    module DateTimeDefault
      def datetime(_val)
        Sequel.lit("'#{@value}'")
      end
    end
  end
end
