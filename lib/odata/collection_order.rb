# frozen_string_literal: true

require 'odata/error'

# all ordering related classes in our OData module
module Safrano
  # base class for ordering
  class OrderBase
    # re-useable empty ordering (idempotent)
    EmptyOrder = new.freeze

    # input : the OData order string
    # returns a Order object that should have a apply_to(cx) method
    def self.factory(orderstr, jh)
      orderstr.nil? ? EmptyOrder : MultiOrder.new(orderstr, jh)
    end

    def empty?
      true
    end

    def parse_error?
      false
    end

    def apply_to_dataset(dtcx)
      dtcx
    end
  end

  class Order < OrderBase
    attr_reader :oarg

    def initialize(ostr, jh)
      @orderp = ostr.strip
      @jh = jh
      build_oarg if @orderp
    end

    def empty?
      false
    end

    def apply_to_dataset(dtcx)
      # Warning, we need order_append, simply order(oarg) overwrites
      # previous one !
      dtcx.order_append(@oarg)
    end

    def build_oarg
      field, ord = @orderp.split(' ')
      oargu = if field.include?('/')
                @assoc, field = field.split('/')
                @jh.add @assoc

                Sequel[@jh.start_model.get_alias_sym(@assoc)][field.strip.to_sym]
              else
                Sequel[field.strip.to_sym]
              end

      @oarg = if ord == 'desc'
                Sequel.desc(oargu)
              else
                Sequel.asc(oargu)
              end
    end
  end

  # complex ordering logic
  class MultiOrder < Order
    def initialize(orderstr, jh)
      super
      @olist = []
      @jh = jh
      @orderstr = orderstr.dup
      @olist = orderstr.split(',').map { |ostr| Order.new(ostr, @jh) }
    end

    def apply_to_dataset(dtcx)
      @olist.each { |osingl| dtcx = osingl.apply_to_dataset(dtcx) }
      dtcx
    end

    def parse_error?
      @orderstr.split(',').each do |pord|
        pord.strip!
        qualfn, dir = pord.split(/\s/)
        qualfn.strip!
        dir.strip! if dir
        return true unless @jh.start_model.attrib_path_valid? qualfn
        return true unless [nil, 'asc', 'desc'].include? dir
      end

      false
    end
  end
end
