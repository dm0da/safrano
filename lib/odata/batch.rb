# frozen_string_literal: true

require_relative '../safrano/rack_app'
require_relative '../safrano/core'
require 'rack/body_proxy'
require_relative './common_logger'

module Safrano
  # Support for OData multipart $batch Requests
  class Request
    def parse_multipart
      @mimep = MIME::Media::Parser.new
      @boundary = media_type_params['boundary']
      @mimep.hook_multipart(media_type, @boundary)
      @mimep.parse_str(body)
    end

    # The top-level (full_req) Request is used like
    # a Rack App
    # With this method we get the response of part-requests
    # app.call(env)  --> full_req.bach_call(part_req)

    def batch_call(part_req)
      Safrano::Batch::PartRequest.new(part_req, self).process
    end

    # needed for changeset transaction
    def db
      @service.collections.first.db
    end
  end

  module Batch
    # Part-Request part of a Batch-Request
    class PartRequest < Safrano::Request
      def initialize(part_req, full_req)
        batch_env(part_req, full_req)
        @env['HTTP_HOST'] = full_req.env['HTTP_HOST']
        super(@env, full_req.service_base)
        @full_req = full_req
        @part_req = part_req
      end

      # redefined for $batch
      def before
        headers 'Cache-Control' => 'no-cache'
        @service = @full_req.service
        headers 'DataServiceVersion' => @service.data_service_version
      end

      def process
        began_at = Rack::Utils.clock_time

        @response = Safrano::Response.new

        if @part_req.level == 2
          @in_changeset = true
          @content_id = @part_req.content_id
          @content_id_references = @part_req.content_id_references
        end

        before

        dispatch

        status, header, body = @response.finish

        # Logging of sub-requests with ODataCommonLogger.
        # A bit hacky but working
        # TODO: test ?
        if (logga = @full_req.env['safrano.logger_mw'])
          logga.batch_log(env, status, header, began_at)
          # TODO: check why/if we need Rack::Utils::HeaderHash.new(header)
          # and Rack::BodyProxy.new(body) ?
        end
        [status, header, body]
      end

      # shamelessely copied from Rack::TEST:Session
      def headers_for_env(headers)
        converted_headers = {}

        headers.each do |name, value|
          env_key = name.upcase.tr('-', '_')
          env_key = "HTTP_#{env_key}" unless env_key == 'CONTENT_TYPE'
          converted_headers[env_key] = value
        end

        converted_headers
      end

      def batch_env(mime_req, full_req)
        @env = ::Rack::MockRequest.env_for(mime_req.uri,
                                           method: mime_req.http_method,
                                           input: mime_req.content)
        # Logging of sub-requests
        @env[Rack::RACK_ERRORS] = full_req.env[Rack::RACK_ERRORS]
        @env.merge! headers_for_env(mime_req.hd)

        @env
      end
    end

    # $batch Handler
    class HandlerBase
      TREND = Safrano::Transition.new('', trans: :transition_end)
      def allowed_transitions
        @allowed_transitions = [TREND]
      end

      def transition_end(_match_result)
        Safrano::Transition::RESULT_END
      end

      include Safrano::Transitions::GetNextTrans::ForJustTransitionEnd
    end

    # $batch disabled Handler
    class DisabledHandler < HandlerBase
      def odata_post(_req)
        [404, EMPTY_HASH, '$batch is not enabled ']
      end

      def odata_get(_req)
        [404, EMPTY_HASH, '$batch is not enabled ']
      end
    end

    # $batch enabled Handler
    class EnabledHandler < HandlerBase
      attr_accessor :boundary
      attr_accessor :mmboundary
      attr_accessor :body_str
      attr_accessor :parts
      attr_accessor :request

      # here we are in the Batch handler object, and this POST should
      # normally handle a $batch request
      def odata_post(req)
        @request = req

        if @request.media_type == Safrano::MP_MIXED

          @mult_request = @request.parse_multipart

          @mult_request.prepare_content_id_refs

          @mult_request.get_mult_resp(@request)

        else
          [415, EMPTY_HASH, 'Unsupported Media Type']
        end
      end

      def odata_get(_req)
        [405, EMPTY_HASH, 'You cant GET $batch, POST it ']
      end
    end
  end
end
