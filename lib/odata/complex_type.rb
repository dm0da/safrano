# frozen_string_literal: true

module Safrano
  module FunctionImport
    EMPTY_HASH = {}.freeze
    class ResultDefinition
      D = 'd'
      DJ_OPEN = '{"d":'
      DJ_CLOSE = '}'
      METAK = '__metadata'
      TYPEK = 'type'
      VALUEK = 'value'
      RESULTSK = 'results'
      COLLECTION = 'Collection'

      def allowed_transitions
        [Safrano::TransitionEnd]
      end

      def transition_end(_match_result)
        Safrano::Transition::RESULT_END
      end

      include Safrano::Transitions::GetNextTrans::ForJustTransitionEnd

      # we will have this on class and instance level for making things simpler first
      class << self
        attr_reader :klassmod
      end

      # return a subclass of ResultAsComplexType
      def self.asComplexType(klassmod)
        Class.new(ResultAsComplexType) do
          @klassmod = klassmod
        end
      end

      # return a subclass of ResultAsComplexType
      def self.asComplexTypeColl(klassmod)
        Class.new(ResultAsComplexTypeColl) do
          @klassmod = klassmod
        end
      end

      def self.asPrimitiveType(klassmod)
        Class.new(ResultAsPrimitiveType) do
          @klassmod = klassmod
        end
      end

      def self.asPrimitiveTypeColl(klassmod)
        Class.new(ResultAsPrimitiveTypeColl) do
          @klassmod = klassmod
        end
      end

      def self.asEntity(klassmod)
        Class.new(ResultAsEntity) do
          @klassmod = klassmod
        end
      end

      def self.asEntityColl(klassmod)
        Class.new(ResultAsEntityColl) do
          @klassmod = klassmod
        end
      end

      def initialize(value)
        @value = value
      end

      def odata_get(req)
        [200, EMPTY_HASH, [to_odata_json(req)]]
      end

      def self.type_metadata
        @klassmod.type_name
      end

      def type_metadata
        self.class.type_metadata
      end

      # needed for ComplexType result
      def to_odata_json(req)
        t = self.class.klassmod.output_template
        innerh = req.service.get_entity_odata_h(entity: @value, template: t)
        innerj = innerh.to_json

        "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
      end

      # wrapper
      # for OData Entity and Collections, return them directly
      # for others, ie ComplexType, Prims etc, return the ResultDefinition-subclass wrapped result
      def self.do_execute_func_result(result, _req, apply_query_params: false)
        new(result)
      end
    end

    class ResultAsComplexType < ResultDefinition
      def self.type_metadata
        @klassmod.type_name
      end
    end

    class ResultAsComplexTypeColl < ResultDefinition
      def self.type_metadata
        "Collection(#{@klassmod.type_name})"
      end

      def to_odata_json(req)
        template = self.class.klassmod.output_template
        # TODO: Error handling if database contains binary BLOB data that cant be
        # interpreted as UTF-8 then JSON will fail here

        innerh = req.service.get_coll_odata_h(array: @value,
                                              template: template)

        innerj = innerh.to_json

        "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
      end
    end

    class ResultAsEntity < ResultDefinition
      def self.type_metadata
        @klassmod.type_name
      end

      # wrapper
      # for OData Entity  return them directly
      def self.do_execute_func_result(result, _req, apply_query_params: false)
        # note: Sequel entities instances seem to be thread safe, so we can
        # safely add request-dependant data (eg. req.params) there
        apply_query_params ? result : result.inactive_query_params
      end
    end

    class ResultAsEntityColl < ResultDefinition
      def self.type_metadata
        "Collection(#{@klassmod.type_name})"
      end

      # wrapper
      # for OData Entity Collection return them directly
      def self.do_execute_func_result(result, req, apply_query_params: false)
        coll = Safrano::OData::Collection.new(@klassmod)
        # instance_exec has other instance variables; @values would be nil in the block below
        # need to pass a local copy
        dtset = result
        coll.instance_exec do
          @params = apply_query_params ? req.params : EMPTY_HASH
          initialize_dataset(dtset)
          initialize_uparms
        end
        coll
      end
    end

    class ResultAsPrimitiveType < ResultDefinition
      def self.type_metadata
        @klassmod.type_name
      end

      def to_odata_json(_req)
        { D => { METAK => { TYPEK => type_metadata },
                 VALUEK => self.class.klassmod.odata_value(@value) } }.to_json
      end
    end

    class ResultAsPrimitiveTypeColl < ResultDefinition
      def self.type_metadata
        "Collection(#{@klassmod.type_name})"
      end

      def to_odata_json(_req)
        { D => { METAK => { TYPEK => self.class.type_metadata },
                 RESULTSK => self.class.klassmod.odata_collection(@value) } }.to_json
      end
    end
  end

  # a generic Struct  like ruby's standard Struct, but implemented with a
  # @values Hash, similar to Sequel models and
  # with added OData functionality
  class ComplexType
    attr_reader :values

    EMPTYH = {}.freeze

    @namespace = nil
    class << self
      attr_reader :namespace
    end

    class << self
      attr_reader :props
    end

    def type_name
      self.class.type_name
    end

    def metadata_h
      { type: type_name }
    end

    def casted_values
      # MVP... TODO: handle time mappings like in Entity models
      values
    end

    # needed for nested json output
    # this is a simpler version of model_ext#output_template
    def self.default_template
      template = { meta: EMPTYH, all_values: EMPTYH }
      expand_e = {}

      @props.each do |prop, kl|
        expand_e[prop] = kl.default_template if kl.respond_to? :default_template
      end
      template[:expand_e] = expand_e
      template
    end

    def self.output_template
      default_template
    end

    def self.type_name
      namespace ? "#{namespace}.#{self}" : to_s
    end

    def initialize
      @values = {}
    end
    METAK = '__metadata'
    TYPEK = 'type'

    def odata_h
      ret = { METAK => { TYPEK => self.class.type_name } }

      @values.each do |key, val|
        ret[key] = val.respond_to?(:odata_h) ? val.odata_h : val
      end
      ret
    end

    def self.return_as_collection_descriptor
      FunctionImport::ResultDefinition.asComplexTypeColl(self)
    end

    def self.return_as_instance_descriptor
      FunctionImport::ResultDefinition.asComplexType(self)
    end

    # add metadata xml to the passed REXML schema object
    def self.add_metadata_rexml(schema)
      ctty = schema.add_element('ComplexType', 'Name' => to_s)

      # with their properties
      @props.each do |prop, rbtype|
        attrs = { 'Name' => prop.to_s,
                  'Type' => rbtype.type_name }
        ctty.add_element('Property', attrs)
      end
      ctty
    end
  end

  def self.ComplexType(**props)
    Class.new(Safrano::ComplexType) do
      @props = props
      props.each do |a, _klassmod|
        asym = a.to_sym
        define_method(asym) { @values[asym] }
        define_method("#{a}=") { |val| @values[asym] = val }
      end
      define_method :initialize do |*p, **kwvals|
        super()
        p.zip(props.keys).each { |val, a| @values[a] = val } if p
        kwvals.each { |a, val| @values[a] = val if props.key?(a) } if kwvals
      end
    end
  end
end
