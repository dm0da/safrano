# frozen_string_literal: true

require 'json'
require 'rexml/document'
require 'safrano'
require 'odata/model_ext' # required for  self.class.entity_type_name ??
require_relative 'navigation_attribute'

module Safrano
  # this will be mixed in the  Model classes (subclasses of Sequel Model)
  module EntityBase
    attr_reader :params

    include Safrano::NavigationInfo

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def allowed_transitions
        self.class.entity_allowed_transitions
      end

      include Safrano::Transitions::GetNextTrans::ByLongestMatchDyn

      def transition_end(_match_result)
        Safrano::Transition::RESULT_END
      end

      def transition_count(_match_result)
        [self, :end]
      end

      def transition_value(_match_result)
        # $value is only allowd for media entities (or attributes)
        [self, :end_with_media_value]
      end

      def transition_links(_match_result)
        [self, :run_with_links]
      end

      def transition_attribute(match_result)
        attrib = match_result[1]
        [Safrano::Attribute.new(self, attrib), :run]
      end

      def transition_nav_collection(match_result)
        attrib = match_result[1]
        [get_related(attrib), :run]
      end

      def transition_nav_entity(match_result)
        attrib = match_result[1]
        [get_related_entity(attrib), :run]
      end

      def transition_invalid_attribute(match_result)
        invalid_attrib = match_result[1]
        [nil, :error, Safrano::ErrorNotFoundSegment.new(invalid_attrib)]
      end
    end

    include Transitions

    # for testing only?
    def ==(other)
      ((self.class.type_name == other.class.type_name) and (@values == other.values))
    end

    def nav_values
      @nav_values = {}

      self.class.nav_entity_attribs&.each_key do |na_str|
        @nav_values[na_str.to_sym] = send(na_str)
      end
      @nav_values
    end

    def nav_coll
      @nav_coll = {}
      self.class.nav_collection_attribs&.each_key do |nc_str|
        @nav_coll[nc_str.to_sym] = send(nc_str)
      end
      @nav_coll
    end

    def uri
      @odata_pk ||= "(#{pk_uri})"
      "#{self.class.uri}#{@odata_pk}"
    end

    D = 'd'
    DJ_OPEN = '{"d":'
    DJ_CLOSE = '}'

    # Json formatter for a single entity (probably OData V1/V2 like)
    def to_odata_json(request:)
      template = self.class.output_template(expand_list: @uparms.expand.template,
                                            select: @uparms.select)
      innerj = request.service.get_entity_odata_h(entity: self,
                                                  template: template).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    # Json formatter for a single entity reached by navigation $links
    def to_odata_onelink_json(service:)
      innerj = service.get_entity_odata_link_h(entity: self).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    def selected_values_for_odata(cols)
      allvals = values_for_odata
      selvals = {}
      cols.map(&:to_sym).each { |k| selvals[k] = allvals[k] if allvals.key?(k) }
      selvals
    end

    # some clients wrongly expect post payload with the new entity in an array
    # TODO quirks array mode !
    def to_odata_array_json(request:)
      innerj = request.service.get_coll_odata_h(array: [self],
                                                template: self.class.default_template).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    def type_name
      self.class.type_name
    end

    def copy_request_infos(req)
      @params = @inactive_query_params ? EMPTY_HASH : req.params
      @do_links = req.walker.do_links
      @uparms = UrlParameters4Single.new(self, @params)
    end

    def odata_get_output(req)
      if req.walker.media_value
        odata_media_value_get(req)
      elsif req.accept?(APPJSON)
        # json is default content type so we dont need to specify it here again
        if req.walker.do_links
          [200, EMPTY_HASH, [to_odata_onelink_json(service: req.service)]]
        else
          [200, EMPTY_HASH, [to_odata_json(request: req)]]
        end
      else # TODO: other formats
        415
      end
    end

    # Finally Process REST verbs...
    def odata_get(req)
      copy_request_infos(req)
      @uparms.check_all.tap_valid { return odata_get_output(req) }
             .tap_error { |e| return e.odata_get(req) }
    end

    def inactive_query_params
      @inactive_query_params = true
      self # chaining
    end

    DELETE_REL_AND_ENTY = lambda do |entity, assoc, parent|
      Safrano.remove_nav_relation(assoc, parent)
      entity.destroy(transaction: false)
    end

    def odata_delete_relation_and_entity(req, assoc, parent)
      if parent
        if req.in_changeset
          # in-changeset requests get their own transaction
          DELETE_REL_AND_ENTY.call(self, assoc, parent)
        else
          db.transaction do
            DELETE_REL_AND_ENTY.call(self, assoc, parent)
          end
        end
      else
        destroy(transaction: false)
      end
    rescue StandardError => e
      raise SequelAdapterError, e
    end

    # TODO: differentiate between POST/PUT/PATCH/MERGE
    def odata_post(req)
      if req.walker.media_value
        odata_media_value_put(req)
      elsif req.accept?(APPJSON)

        AlreadyExistsUnprocessableError.odata_get(req)

      else # TODO: other formats
        415
      end
    end

    def odata_put(req)
      if req.walker.media_value
        odata_media_value_put(req)
      elsif req.accept?(APPJSON)
        data = Safrano::OData::JSON.parse_one(req.body.read, self.class)
        data.delete('__metadata')

        if req.in_changeset
          set_fields(data, self.class.data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(data, self.class.data_fields, missing: :skip)
        end

        ARY_204_EMPTY_HASH_ARY
      else # TODO: other formats
        415
      end
    end

    def odata_patch(req)
      req.with_parsed_data(self.class) do |data|
        data.delete('__metadata')
        # validate payload column names
        if (invalid = self.class.invalid_hash_data?(data))
          ::Safrano::Request::ON_CGST_ERROR.call(req)
          return [422, EMPTY_HASH, ['Invalid attribute name: ', invalid.to_s]]
        end
        # TODO: check values/types

        my_data_fields = self.class.data_fields

        if req.in_changeset
          set_fields(data, my_data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(data, my_data_fields, missing: :skip)
        end
        # patch should return 204 + no content
        ARY_204_EMPTY_HASH_ARY
      end
    end

    # GetRelated that returns a collection object representing
    # wrapping  the related object Class  ( childklass )
    # (...to_many relationship )
    def get_related(childattrib)
      Safrano::OData::NavigatedCollection.new(childattrib, self)
    end

    # GetRelatedEntity that returns an single related Entity
    # (...to_one relationship )
    def get_related_entity(childattrib)
      # this makes use of Sequel's Model relationships; eg this is
      # 'Race[12].RaceType'
      # where Race[12] would be our self and 'RaceType' is the single
      # childattrib entity

      # when the child attribute is nil (because the FK is NULL on DB)
      # then we return a Nil... wrapper object. This object then
      # allows to receive a POST operation that would actually create the nav attribute entity

      ret = method(childattrib.to_sym).call || Safrano::NilNavigationAttribute.new

      ret.set_relation_info(self, childattrib)

      ret
    end
  end

  # end of module SafranoEntity
  module Entity
    include EntityBase
  end

  module NonMediaEntity
    # non media entity metadata for json h
    def metadata_h
      {   uri: uri,
          type: type_name }
    end

    def values_for_odata
      values
    end

    def odata_delete(req)
      if req.accept?(APPJSON)
        #        delete
        begin
          odata_delete_relation_and_entity(req, @navattr_reflection, @nav_parent)
          [200, EMPTY_HASH, [{ 'd' => req.service.get_emptycoll_odata_h }.to_json]]
        rescue SequelAdapterError => e
          BadRequestSequelAdapterError.new(e).odata_get(req)
        end
      else # TODO: other formats
        415
      end
    end

    # in case of a non media entity, we have to return an error on $value request
    def odata_media_value_get(_req)
      BadRequestNonMediaValue.odata_get
    end

    # in case of a non media entity, we have to return an error on $value PUT
    def odata_media_value_put(_req)
      BadRequestNonMediaValue.odata_get
    end
  end

  module MediaEntity
    # media entity metadata for json h
    def metadata_h
      {   uri: uri,
          type: type_name,
          media_src: media_src,
          edit_media: edit_media,
          content_type: @values[:content_type] }
    end

    def media_src
      version = self.class.media_handler.ressource_version(self)
      "#{uri}/$value?version=#{version}"
    end

    def edit_media
      "#{uri}/$value"
    end

    def values_for_odata
      ret = values.dup
      ret.delete(:content_type)
      ret
    end

    def odata_delete(req)
      if req.accept?(APPJSON)
        # delete the MR
        # delegate to the media handler on collection(ie class) level
        # TODO error handling

        self.class.media_handler.odata_delete(entity: self)
        # delete the relation(s)  to parent(s) (if any) and then entity
        odata_delete_relation_and_entity(req, @navattr_reflection, @nav_parent)
        # result
        [200, EMPTY_HASH, [{ 'd' => req.service.get_emptycoll_odata_h }.to_json]]
      else # TODO: other formats
        415
      end
    end

    # real implementation for returning $value for a media entity
    def odata_media_value_get(req)
      # delegate to the media handler on collection(ie class) level
      self.class.media_handler.odata_get(request: req, entity: self)
    end

    # real implementation for replacing $value for a media entity
    def odata_media_value_put(req)
      model = self.class
      req.with_media_data do |data, mimetype, _filename|
        emdata = { content_type: mimetype }
        if req.in_changeset
          set_fields(emdata, model.data_fields, missing: :skip)
          save(transaction: false)
        else

          update_fields(emdata, model.data_fields, missing: :skip)

        end
        model.media_handler.replace_file(data: data,
                                         entity: self)

        ARY_204_EMPTY_HASH_ARY
      end
    end
  end

  module PKUriWithFunc
    def pk_uri
      self.class.pk_castfunc.call(pk)
    end

    def media_path_id
      pk_uri.to_s
    end

    def media_path_ids
      [pk_uri]
    end
  end

  module PKUriWithoutFunc
    def pk_uri
      pk
    end

    def media_path_id
      pk.to_s
    end

    def media_path_ids
      [pk]
    end
  end

  # for a single public key
  module EntitySinglePK
    include Entity
    # PKUriWithoutFunc or PKUriWithFunc will be included on startup
  end

  # for multiple key
  module EntityMultiPK
    include Entity
    def pk_uri
      pku = +''
      self.class.odata_upk_parts.each_with_index do |upart, i|
        pku = "#{pku}#{upart}#{pk[i]}"
      end
      pku
    end

    def media_path_id
      pk_hash.values.join(SPACE)
    end

    def media_path_ids
      pk_hash.values
    end
  end

  module EntityCreateStandardOutput
    # Json formatter for a create  entity POST call / Standard version; return as json object
    def to_odata_create_json(request:)
      # we added this redirection for readability and flexibility
      to_odata_json(request: request)
    end
  end

  module EntityCreateArrayOutput
    # Json formatter for a create  entity POST call Array version
    def to_odata_create_json(request:)
      # we added this redirection for readability and flexibility
      to_odata_array_json(request: request)
    end
  end
end # end of Module OData
