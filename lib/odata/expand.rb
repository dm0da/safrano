# frozen_string_literal: true

require 'odata/error'

# all dataset expanding related classes in our OData module
# ie  do eager loading
module Safrano
  # base class for expanding
  class ExpandBase
    EmptyExpand = new # re-useable empty expanding (idempotent)
    EMPTYH = {}.freeze

    def self.factory(expandstr, model)
      expandstr.nil? ? EmptyExpand : MultiExpand.new(expandstr, model)
    end

    def apply_to_dataset(dtcx)
      Contract.valid(dtcx)
    end

    def empty?
      true
    end

    def parse_error?
      Contract::OK
    end

    def template
      EMPTYH
    end
  end

  # single expand
  class Expand < ExpandBase
    # sequel eager arg.
    attr_reader :arg
    attr_reader :template

    # used for Sequel eager argument
    # Recursive array to deep hash
    # [1,2,3,4]  -->  {1=>{2=>{3=>4}}}
    # [1] --> 1
    DEEPH_0 = ->(inp) { inp.size > 1 ? { inp[0] => DEEPH_0.call(inp[1..-1]) } : inp[0] }

    # used for building output template
    # Recursive array to deep hash
    # [1,2,3,4]  -->  {1=>{2=>{3=>4}}}
    # [1] --> { 1 => {} }
    DEEPH_1 = ->(inp) { inp.size > 1 ? { inp[0] => DEEPH_1.call(inp[1..-1]) } : { inp[0] => {} } }

    NODESEP = '/'

    def initialize(exstr)
      exstr.strip!
      @expandp = exstr
      @nodes = @expandp.split(NODESEP)
      build_arg
    end

    def apply_to_dataset(dtcx)
      Contract.valid(dtcx)
    end

    def build_arg
      #   'a/b/c/d'  ==> {a: {b:{c: :d}}}
      #   'xy'  ==> :xy
      @arg = DEEPH_0.call(@nodes.map(&:to_sym))
      @template = DEEPH_1.call(@nodes)
    end

    def empty?
      false
    end
  end

  # Multi expanding logic
  class MultiExpand < ExpandBase
    COMASPLIT = /\s*,\s*/.freeze
    attr_reader :template

    # Note: if you change this method, please also update arity_full_monkey?
    #       see below
    def initialize(expandstr, model)
      @model = model
      @expandp = expandstr.strip

      @exstrlist = expandstr.split(COMASPLIT)
      @exlist = @exstrlist.map { |exstr| Expand.new(exstr) }
      build_template
    end

    def apply_to_dataset(dtcx)
      # use eager loading for each used association
      @exlist.each { |exp| dtcx = dtcx.eager(exp.arg) }
      Contract.valid(dtcx)
    end

    def build_template
      #   'a/b/c/d,xy'  ==> [ {'a' =>{ 'b' => {'c' => {'d' => {} } }}},
      #                       { 'xy' => {} }]
      #
      @template = @exlist.map(&:template)

      # { 'a'  => { 'b' => {'c' => 'd' }},
      #   'xy' => {} }
      @template = @template.inject({}) { |mrg, elmt| mrg.merge elmt }
    end

    def parse_error?
      @exstrlist.each do |expstr|
        return BadRequestExpandInvalidPath.new(@model, expstr) unless @model.expand_path_valid? expstr
      end

      Contract::OK
    end

    def empty?
      false
    end
  end
end
