# frozen_string_literal: true

require_relative 'model_ext'

module Safrano
  module OData
    class Collection
      attr_accessor :cx

      # url params
      attr_reader :params

      # url parameters processing object (mostly convert to sequel exprs).
      # exposed for testing only
      attr_reader :uparms

      attr_reader :inlinecount

      attr_reader :modelk

      def initialize(modelk)
        @modelk = modelk
        @allowed_transitions = @modelk.allowed_transitions
      end

      def allowed_transitions
        @modelk.allowed_transitions
      end

      def transition_end(_match_result)
        Safrano::Transition::RESULT_END
      end

      def transition_count(_match_result)
        [self, :end_with_count]
      end

      ###########################################################
      # this is redefined in NavigatedCollection
      def dataset
        @modelk.dataset
      end

      def transition_id(match_result)
        if (rawid = match_result[1])
          @modelk.parse_odata_key(rawid).tap_error do
            return Safrano::Transition::RESULT_BAD_REQ_ERR
          end.if_valid do |casted_id|
            (y = find_by_odata_key(casted_id)) ? [y, :run] : Safrano::Transition::RESULT_NOT_FOUND_ERR
          end
        else
          Safrano::Transition::RESULT_SERVER_TR_ERR
        end
      end

      include Safrano::Transitions::GetNextTrans::ByLongestMatch

      # pkid can be a single value for single-pk models,  or an array.
      # type checking/convertion is done in check_odata_key_type
      def find_by_odata_key(pkid)
        lkup = @modelk.pk_lookup_expr(pkid)
        dataset[lkup]
      end

      def initialize_dataset(dtset = nil)
        @cx = @cx || dtset || @modelk
      end

      def initialize_uparms
        @uparms = UrlParameters4Coll.new(@cx, @params)
      end

      def odata_get_apply_params
        @uparms.apply_to_dataset(@cx).map_result! do |dataset|
          @cx = dataset
          odata_get_inlinecount_w_sequel
          if (skipp = @params['$skip'])
            @cx = @cx.offset(skipp) if skipp != '0'
          end
          @cx = @cx.limit(@params['$top']) if @params['$top']

          @cx
        end
      end

      D = 'd'
      DJ_OPEN = '{"d":'
      DJ_CLOSE = '}'
      EMPTYH = {}.freeze

      def to_odata_json(request:)
        template = @modelk.output_template(expand_list: @uparms.expand.template,
                                           select: @uparms.select)
        # TODO: Error handling if database contains binary BLOB data that cant be
        # interpreted as UTF-8 then JSON will fail here
        innerj = request.service.get_coll_odata_h(array: @cx.all,
                                                  template: template,
                                                  icount: @inlinecount).to_json

        "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
      end

      def to_odata_links_json(service:)
        innerj = service.get_coll_odata_links_h(array: @cx.all,
                                                icount: @inlinecount).to_json
        "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
      end

      def odata_get_inlinecount_w_sequel
        return unless (icp = @params['$inlinecount'])

        @inlinecount = if icp == 'allpages'
                         if @modelk.is_a? Sequel::Model::ClassMethods
                           @cx.count
                         else
                           @cx.dataset.count
                         end
                       end
      end

      # finally return the requested output according to format, options etc
      def odata_get_output(req)
        output = if req.walker.do_count
                   [200, CT_TEXT, @cx.count.to_s]
                 elsif req.accept?(APPJSON)
                   # json is default content type so we dont need to specify it here again
                   if req.walker.do_links
                     [200, EMPTYH, [to_odata_links_json(service: req.service)]]
                   else
                     [200, EMPTYH, [to_odata_json(request: req)]]
                   end
                 else # TODO: other formats
                   406
                 end
        Contract.valid(output)
      end

      # on model class level we return the collection
      def odata_get(req)
        @params ||= req.params
        initialize_dataset
        initialize_uparms
        @uparms.check_all.if_valid do |_ret|
          odata_get_apply_params.if_valid do |_ret|
            odata_get_output(req)
          end
        end.tap_error { |e| return e.odata_get(req) }.result
      end

      def odata_post(req)
        @modelk.odata_create_entity_and_relation(req)
      end
    end

    class NavigatedCollection < Collection
      include Safrano::NavigationInfo

      def initialize(childattrib, parent)
        childklass = parent.class.nav_collection_attribs[childattrib]

        super(childklass)
        @parent = parent

        set_relation_info(@parent, childattrib)

        @child_method = parent.method(childattrib.to_sym)
        @child_dataset_method = parent.method("#{childattrib}_dataset".to_sym)

        @cx = navigated_dataset
      end

      def odata_post(req)
        @modelk.odata_create_entity_and_relation(req,
                                                 @navattr_reflection,
                                                 @nav_parent)
      end

      def initialize_dataset(dtset = nil)
        @cx = @cx || dtset || navigated_dataset
      end
      # redefinitions of the main methods for a navigated collection
      # (eg. all Books of Author[2]  is Author[2].Books.all )

      def all
        @child_method.call
      end

      def count
        @child_method.call.count
      end

      def dataset
        @child_dataset_method.call
      end

      def navigated_dataset
        @child_dataset_method.call
      end

      def each(&block)
        @child_method.call.each(&block)
      end

      def to_a
        y = @child_method.call
        y.to_a
      end
    end
  end
end
