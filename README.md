## Safrano
Safrano is an OData server library .

## Installation

Safrano is available as a [Rubygem](https://rubygems.org/gems/safrano)

```bash
gem install safrano
```

## Usage

To create an OData endpoint follow these steps:

1. [Setup  Models and their associations](docs/latest/01_Sequel_Model.md#safrano-documentation) by using the Sequel functionality.
2. [Publish models and their associations](docs/latest/02_publish.md#safrano-documentation) as an OData app and optionally 
   + [Publish media entity models and associations](docs/latest/02_publish_media.md#safrano-documentation) 
   + [Define and publish service operations](docs/latest/02_service_opps.md#safrano-documentation) 
   + [Override default type mappings](docs/latest/02_type_mapping.md#safrano-documentation) 
3. [Configure OData Rack app](docs/latest/03_rack_config.md#safrano-documentation) (port, additional middlewares, logging etc) 

Only the second step is Safrano/OData specific.

After that you are ready to start your server app 
with `rackup config.ru`

## Demo apps and examples

* Demo apps
  + [Fiveapples](https://gitlab.com/dm0da/fiveapples) is a fully opensource and standalone (fullstack) Safrano based OData + openui5 demo app
* Examples in the Safrano's repository testsuite 
  + [Chinook database](https://gitlab.com/dm0da/safrano/-/tree/master/test/chinookn3) featuring:
      - one_to_many, many_to_one, many_to_many
  + [A Sailing races and rankings database](https://gitlab.com/dm0da/safrano/-/tree/master/test/sail) featuring:
      - tables with multiple key fields
      - one_to_many, many_to_one, many_to_many
  + [Botanics database](https://gitlab.com/dm0da/safrano/-/tree/master/test/botanics) featuring:
      - one_to_many, many_to_one, many_to_many
      - media entities
* [OData server Examples](https://gitlab.com/artyomb/odata_server_examples) from @artyomb
  + [OData UUID](https://gitlab.com/artyomb/odata_server_examples#odata-uuid) featuring:
      - use of uuid/guid key field 

## Supported OData versions and features

OData v1 and v2 are provided with the following functionality

* format: only json, no xml/atom yet
* Supported types : most OData primitive types are supported. See [Safrano's default type system amd how to override it](docs/latest/02_type_mapping.md#safrano-documentation) for more details
* reading entities and collections with
  + $expand, $select, $count, $value
  + $links
  + $filter
      - logical filter expressions with functions length, trim, tolower, toupper, concat, substringof, startswith / endswith
      - math and arithmetic functions add,sub,mul,div,mod,round,floor,ceiling
      - datetime functions year, month, day, hour, minute, second 
  + $orderby
  + $top and $skip
  + $inlinecount
* all CRUD operations
* fully automated  support for navigation properties based on model associations, 
themselves based on database relationships 
  + many_to_one, one_to_many, one_to_one
  + Added in version 0.7, full support for many_to_many and one_through_one 
* $batch requests supporting
  + changesets
  + content-ID referencing in changesets
* media entities and media ressources: upload/update, download, delete, associate
* service operations / function imports
* complex types - currently only used in service operations 


## Supported database backends and ruby versions

* Any database supported by Sequel should work in theory. Currently the testsuite  runs agains Sqlite and Postgresql test-databases.
* Ruby versions: 
  + From version 0.8.0, safrano  is tested on ruby 2.7 to latest stable ruby
  + From version 0.6.0 to 0.7.1, safrano  is tested on ruby 2.4 to 3.2
  + From version 0.5.0 to 0.5.5, safrano  is tested on ruby 2.4 to 3.0
  + Up to version 0.4.6, safrano is tested on ruby 2.4 to 2.7 
  
  


## Supported Rack servers

Safrano should work well with these web servers:
  + puma
  + thin
  + falcon, see details below
  + webrick

Running Safrano with Falcon is  possible with _some tweaks_ :
  + with postgresql; use the async-postgres gem 
  + and/or run Sequel in single threaded   mode

## Note concerning unsupported types

DB-Columns with unsupported types, ie DB columns that dont map to one of the supported types above
are actually not fully unsupported,  they are present in OData responses. But they
dont have the "Type" attribute set in the $metadata . Thus clients relying on the presence of
this type information in $metadata might fail.

Starting from Safrano v0.5.5 you can override the default type mapping and output rules and define your own logic. See [Safrano's default type system amd how to override it](docs/latest/02_type_mapping.md#safrano-documentation) for more details.


## Backwards incompatible changes

In order to fix bad decisions from the early days a few backwards incompatible changes are needed. We do our best to keep these changes as minimal as possible.

* Namespace change: Starting from safrano 0.4.4, we only use a single top level `Safrano` module namespace. Before that we had a mixture of `OData`  and `Safrano` namespaces. We will keep the `OData` namespace and API with a deprecation warning and planned removal is in version 0.6 . This affects the following API parts:
  + Media handlers are now: `Safrano::Media::Static` and `Safrano::Media::StaticTree`  instead of `OData::Media::Static` and `ODdata::Media::StaticTree`
  + The Safrano Rack app base class is now `Safrano::ServerApp` instead of `OData::ServerApp` 
  + The Safrano Rack Builder class is now `Rack::Safrano::Builder` instead of `Rack::OData::Builder`

* OData functionality change: More strict multiple primary-key predicates. Before safrano 0.4.4 we allowed to have ressource uris for multiple integer primary-key predicates with or  without quotes, ie. considering pk1, pk2, pk3 the integer keys of `myentity` then `myentity(pk1='55',pk2='66',pk3='12')` was accepted and treated same as `myentity(pk1=55,pk2=66,pk3=12)` . In safrano 0.4.4, now quoted primary key predicates are invalid if the keys are integers.

* Bugs: 
  + Up to Safrano 0.4.5, the response of POST requests to create new entities was wronlgy a JSON array with the created entity. This will still be the default behavior in all 0.4.x releases, but starting with 0.4.6, we provide the DSL statement `bugfix_create_response` to have the correct POST response with just the newly created entity *not* wrapped in an array. Starting from safrano 0.6 the bugfix will be active per default . 
  + Up to Safrano 0.5.5 datetime field were wrongly outputed in the iso-8601 format instead of /Date(ticks)/ format as it is required by OData V2. This has been fixed in  Safrano 0.6.0. See [OData V2 DateTime and Timezone considerations](docs/latest/02_odatav2_datetime_tz.md#safrano-documentation) for details.
 
  
## Contributing

Any contribution and feedback is welcome. 

## Testsuite(s)

Safrano's code is intensively tested. We run multiple testsuites totalling more as 500 testcases :
* a database independant suite, run with `rake test_wo_db`
* a database dependant suite with three different test-databases: a sail racing DB, a botanical db, and the chinook test-db. 
Currently we run these real-database tests under sqlite and postgresql:
  + `rake test_with_db[sqlite]`
  + `rake test_with_db[postgres]`
  
To run all three testsuites, just use `rake test`

Note: 
* To be able to run the sqlite testsuite, you only need to have sqlite installed. No additional setup is required.
* In order to be able to run the postgresql suite, some minimal setup tasks are required.
  + ensure that you have postgresql up and running
  + create a database user `safrano`, password `password`. This db-user should have authorisation for DB creation. To do so, just run `createuser --pwprompt safrano --createdb` (as postgres admin user)
That's all ! Database creation, connect, test-data loading is handled automatically by the test-suites.


## Thanks

* @artyomb for valuable feedback (issues) and merge request contributions
* Reeven Govaert for providing a great logo for Safrano
* https://opensourcedesign.net for making it easy to get a great logo :-)


